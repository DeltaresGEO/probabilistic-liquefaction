import numpy as np
import pandas as pd
import os
import time as tm

from Liquefaction_Probabilistic_Model import model as liq_model



path = '..\\Railway analysis\\Groningen - AllRail'
gef_folder = 'CPT gefs'
input_filename ='Groningen_AllRail_input'
output_filename ='Groningen_AllRail_output'
dummy_filename ='Groningen_AllRail_output_dummy'

# path = '..\\Road analysis\\Groningen - AllRoad'
# gef_folder = 'CPT gefs'
# input_filename ='Groningen_AllRoad_input'
# output_filename ='Groningen_AllRoad_output'
# dummy_filename ='Groningen_AllRoad_output_dummy'

tm_start = tm.time()

PGA_grid = [0.10, 0.15, 0.20, 0.25, 0.30]

for PGA in PGA_grid:

    df = pd.read_csv(path + '\\' + input_filename + '.csv')
    df['a_max'] = PGA
    df.to_csv(path+'\\'+dummy_filename + '.csv')

    model = liq_model(path=path, gef_folder=gef_folder, input_filename=dummy_filename, LPI_vals=[0, 5, 10, 15],
                      S_vals=np.arange(10), n_samples=2_000, per_PGA=True)

    model.run_model()

    model.write_model_pickle(name='Rail_analysis_'+str(round(PGA, 2)))

    tm_end = tm.time()

    print('Runtime = ' + str(round((tm_end - tm_start) / 60, 0)))

    model.export_df(filename = path + '\\' +''+ output_filename+'_PGA_'+str(round(PGA, 2)))

    model.plot_det_LPI_map(path=path+'\\'+'Output maps')

    model.plot_det_settlement_map(path=path+'\\'+'Output maps')

    if not os.path.exists(path + '\\' + 'Output maps\\PGA_' + str(round(PGA, 2))):
        os.mkdir(path + '\\' + 'Output maps\\PGA_' + str(round(PGA, 2)))

    for LPIish in np.arange(5, 21, 5):
        model.plot_LPI_map_points(LPI_val=LPIish, show_cpt_id=False, path=path+'\\'+'Output maps\\PGA_' + str(round(PGA, 2)))

    for S in np.arange(2, 11, 2):
        model.plot_S_map_points(S_val=S, show_cpt_id=False, path=path+'\\' + 'Output maps\\PGA_' + str(round(PGA, 2)))

    for perc in [5, 50, 95]:
        model.plot_LPI_perc_map_points(perc=perc, show_cpt_id=False, path=path+'\\'+'Output maps\\PGA_' + str(round(PGA, 2)))

    for perc in [5, 50, 95]:
        model.plot_S_perc_map_points(perc=perc, show_cpt_id=False, path=path+'\\'+'Output maps\\PGA_' + str(round(PGA, 2)))
