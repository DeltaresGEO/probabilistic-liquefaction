import numpy as np
from scipy.stats import norm, lognorm
import pandas as pd
import emcee as mc
import seaborn as sns
import matplotlib.pyplot as plt
import Liquefaction_FoS as L_FoS
from Liquefaction_Probabilistic_Model import liq_sampler

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def FoS_model(df, rd, MSF, Cfc, Co):
    FoS, _, _, _ = L_FoS.calc_FoS_KF(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                                                  pwp=df['pwp'].to_numpy(), gwl=df['GW'].iloc[0],
                                                  a_max=df['amax_g'].iloc[0]*9.81,
                                                  rd=rd, MSF=MSF, Cfc=Cfc, Co=Co, fines_type='NPR_9998_2020')
    return FoS

def settlement_model(df, rd, MSF, Cfc, Co):
    FoS, Ic, FC, qc1ncs = L_FoS.calc_FoS_KF(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                                                  pwp=df['pwp'].to_numpy(), gwl=df['GW'].iloc[0],
                                                  a_max=df['amax_g'].iloc[0]*9.81,
                                                  rd=rd, MSF=MSF, Cfc=Cfc, Co=Co, fines_type='NPR_9998_2020')
    S = L_FoS.calc_settlement(FoS=FoS, qc1ncs=qc1ncs, z=df['Depth'].to_numpy(), Ic=Ic) * 100
    return S

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

filename = 'model_validation_data_ku2004'

df = pd.read_csv('D:\\Probabilistic_Liquefaction\\Model validation data\\'+filename+'.csv', index_col=False)



dummy_sampler = liq_sampler(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                            a_max_g=df['amax_g'].iloc[0], M=df['Mw'].iloc[0], Vs_12=140, pwp=df['pwp'].to_numpy(),
                            gwl=df['GW'].iloc[0], zone=0, n_samples=10_000)

dummy_sampler.initialize_variables()

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rd_samples = dummy_sampler.draw_rd_samples()
MSF_samples = dummy_sampler.draw_MSF_samples()
Co_samples = dummy_sampler.draw_Co_samples()
Cfc_samples = dummy_sampler.draw_Cfc_samples()
FoS = np.zeros((dummy_sampler.n_depths, dummy_sampler.n_samples))
LPI = np.zeros(dummy_sampler.n_samples)
FoS = np.zeros((len(df), dummy_sampler.n_samples))
S_Gron = np.zeros(dummy_sampler.n_samples)
for i, (rd, MSF, Co, Cfc) in enumerate(zip(rd_samples.T, MSF_samples.T, Co_samples.T, Cfc_samples.T)):
    FoS[:, i] = FoS_model(df=df, rd=rd, MSF=MSF, Co=Co, Cfc=Cfc)
    S_Gron[i] = settlement_model(df=df, rd=rd, MSF=MSF, Co=Co, Cfc=Cfc)

density, bin_edges = np.histogram(S_Gron, bins=30, density=True)
df_out = pd.DataFrame(data=np.c_[FoS.reshape(-1,1), np.tile(np.arange(1, FoS.shape[0]+1).reshape(-1,1),
                                                            (1, FoS.shape[1])).reshape(-1,1)], columns=['FoS', 'Point'])
df_out['Liquefaction?'] = np.where(df_out['FoS']>=1, 0, 1)


plt.figure()
sns.set(style='white', font_scale=1)
sns.histplot(S_Gron, label='Settlement prediction - Deltares', bins=100)
plt.axvline(density.dot((bin_edges[1:]+bin_edges[:-1])/2), label='Mean prediction - Deltares', linestyle='--')
plt.axvline(df['S_obs_mean'].iloc[0], label='Observation', color='red')
plt.axvline(df['S_pred_mean'].iloc[0], label='Model prediction - Juang et al', color='green')
plt.xlabel('Settlement [cm]')
plt.legend()


plt.figure()
sns.set(style='white', font_scale=1)
sns.stripplot(data=df_out, x='Point', y='FoS')
plt.axhline(1.0, color='black', linewidth=2, zorder=11111, label='FoS=1.0', linestyle='--')
plt.scatter(df.index[df['Liquefaction?']=='Yes'].tolist(),np.ones(len(df[df['Liquefaction?']=='Yes'])),
            color='green', marker='X', zorder=11111, label='Layer liquefied', s=50)
plt.scatter(df.index[df['Liquefaction?']=='No'].tolist(),np.ones(len(df[df['Liquefaction?']=='No'])),
            color='red', marker='X', zorder=11111, label='Layer not liquefied', s=50)
plt.title('Monte Carlo FoS samples per depth point (Deltares model)')
plt.xlabel('Point over depth')
plt.ylabel('FoS [-]')
plt.legend()




# for fos in FoS[:,100].T:
#     plt.plot(fos, df['Depth'], color='k', alpha=0.2)
# plt.gca().invert_yaxis()
