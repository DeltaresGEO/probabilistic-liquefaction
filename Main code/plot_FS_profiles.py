import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

#-----------------------------------------------------------------------------------------------------------------------

df_screen = pd.read_csv('..\\Groningen locations.csv')

# path_rail = '..\\Railway analysis\\Groningen - AllRail'
path_rail = '..\\Railway analysis\\Groningen - AllRail - Actual PGA'
# path_road = '..\\Road analysis\\Groningen - AllRoad'
path_road = '..\\Road analysis\\Groningen - AllRoad - Actual PGA'


dict = {}
for (type, path) in zip(['Rail', 'Road'], [path_rail, path_road]):
# for (type, path) in zip(['Rail'], [path_rail]):

    folder = path + '\\' + 'Deterministic FoS profiles'
    files = os.listdir(folder)

    for file in files:
        if file.split('.')[0] not in list(dict.keys()):
            df = pd.read_csv(folder+'\\'+file)
            dict[file.split('.')[0]] = {'z': df['Depth [m]'].values,
                                        'elev': df['Elevation [mNAP]'].values,
                                        'FS': df['FoS [-]'].values,
                                        'CRR': df['CRR [-]'].values,
                                        'type': type}

dict = {key: val for (key, val) in dict.items() if key in df_screen['CPT ID'].tolist()}


# type = 'Rail'
# fig, ax = plt.subplots(1, 2)
# for (key, val) in dict.items():
#     if val['type'] == type:
#         ax[0].plot(val['FS'], val['z'], alpha=0.5, label=key)
#         ax[1].plot(val['CRR'], val['z'], alpha=0.5, label=key)
# ax[0].set_xlim([0.5, 2])
# ax[0].set_xlabel('FoS [-]', fontsize=16)
# ax[0].set_ylabel('Depth [m]', fontsize=16)
# ax[1].set_xlim([0., 2])
# ax[1].set_xlabel('CRR [-]', fontsize=16)
# ax[1].set_ylabel('Depth [m]', fontsize=16)
# ax[0].legend()
# fig.suptitle(type, fontsize=16)


# type = 'Road'
# fig, ax = plt.subplots(8, 2)
# cnt = 0
# for (key, val) in dict.items():
#     idx = cnt // 5
#     if val['type'] == type:
#         ax[idx, 0].plot(val['FS'], val['z'], alpha=0.5, label=key)
#         ax[idx, 1].plot(val['CRR'], val['z'], alpha=0.5, label=key)
#         cnt += 1
#     ax[idx, 0].set_ylabel('Depth [m]', fontsize=16)
#     ax[idx, 1].set_ylabel('Depth [m]', fontsize=16)
#     ax[idx, 0].legend()
#
# ax[-1, 0].set_xlim([0.5, 2])
# ax[-1, 0].set_xlabel('FoS [-]', fontsize=16)
# ax[-1, 1].set_xlim([0., 2])
# ax[-1, 1].set_xlabel('CRR [-]', fontsize=16)
# fig.suptitle(type, fontsize=16)



# type = 'Road'
# fig, ax = plt.subplots(3, 3)
# cnt = 0
# for (key, val) in dict.items():
#     idx = cnt // 8
#     i = idx // ax.shape[1]
#     j = idx % ax.shape[1]
#     print(idx, i, j)
#     if val['type'] == type:
#         ax[i, j].plot(val['FS'], val['z'], alpha=0.5, label=key)
#         ax[i, j].plot(val['CRR'], val['z'], alpha=0.5, label=key)
#         cnt += 1
#     ax[i, j].set_ylabel('Depth [m]', fontsize=16)
#     ax[i, j].set_ylabel('Depth [m]', fontsize=16)
#     ax[i, j].set_xlim([0.5, 2])
#     ax[i, j].legend()
# for i in range(ax.shape[1]):
#     ax[-1, i].set_xlabel('FoS [-]', fontsize=16)
# fig.suptitle(type, fontsize=16)


# type = 'Rail'
# cnt = 0
# for (key, val) in [(key, val) for (key, val) in dict.items() if val['type']==type]:
#     # if cnt % 5 == 0:
#     fig = plt.figure()
#     plt.plot(val['FS'], val['z'], alpha=0.5, label=key)
#     plt.axvline(1, color='k')
#     cnt += 1
#     plt.ylabel('Depth [m]', fontsize=16)
#     plt.ylabel('Depth [m]', fontsize=16)
#     plt.xlim([0.5, 2])
#     plt.legend()
#     plt.xlabel('FoS [-]', fontsize=16)
#     fig.suptitle(type, fontsize=16)
#     plt.savefig('..\\Depth FoS profile plots\\Rail_no_'+str(cnt) + '.png', bbox_inches='tight')
#
#
# type = 'Road'
# cnt = 0
# for (key, val) in [(key, val) for (key, val) in dict.items() if val['type']==type]:
#     # if cnt % 5 == 0:
#     fig = plt.figure()
#     plt.plot(val['FS'], val['z'], alpha=0.5, label=key)
#     plt.axvline(1, color='k')
#     cnt += 1
#     plt.ylabel('Depth [m]', fontsize=16)
#     plt.ylabel('Depth [m]', fontsize=16)
#     plt.xlim([0.5, 2])
#     plt.legend()
#     plt.xlabel('FoS [-]', fontsize=16)
#     fig.suptitle(type, fontsize=16)
#     plt.savefig('..\\Depth FoS profile plots\\Road_no_'+str(cnt) + '.png', bbox_inches='tight')


z_perc = {}
CRR_perc = {}

type = 'Rail'
cnt = 0
for (key, val) in [(key, val) for (key, val) in dict.items() if val['type']==type]:
    fig = plt.figure()
    cumulative_trigger = np.cumsum(np.where(val['FS']<=1, 1, 0))
    plt.plot(cumulative_trigger, val['z'], alpha=0.5, label=key)
    perc5 = cumulative_trigger.max() * 0.05
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc5))], color='r', label='5% depth')
    perc75 = cumulative_trigger.max() * 0.75
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc75))], color='r', label='75% depth')
    perc95 = cumulative_trigger.max() * 0.95
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc95))], color='r', label='95% depth')
    z_perc[key] = {'5': val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                   '75': val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                   '95': val['z'][np.argmin(np.abs(cumulative_trigger-perc95))]}
    CRR_perc[key] = {'5_75': [np.mean(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                                                              val['FS']<=1))[0]]),
                              np.var(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                                                              val['FS']<=1))[0]])],
                     '5_95': [np.mean(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc95))],
                                                              val['FS']<=1))[0]]),
                              np.var(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc95))],
                                                              val['FS']<=1))[0]])]
                     }
    cnt += 1
    plt.gca().invert_yaxis()
    plt.ylabel('Depth [m]', fontsize=16)
    plt.ylabel('Depth [m]', fontsize=16)
    plt.legend()
    plt.xlabel('Cumulative triggerring count [-]', fontsize=16)
    fig.suptitle(type, fontsize=16)
    plt.savefig('..\\Depth FoS profile plots\\Rail_' + key + '.png', bbox_inches='tight')


type = 'Road'
cnt = 0
for (key, val) in [(key, val) for (key, val) in dict.items() if val['type']==type]:
    fig = plt.figure()
    cumulative_trigger = np.cumsum(np.where(val['FS']<=1, 1, 0))
    plt.plot(cumulative_trigger, val['z'], alpha=0.5, label=key)
    perc5 = cumulative_trigger.max() * 0.05
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc5))], color='r', label='5% depth')
    perc75 = cumulative_trigger.max() * 0.75
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc75))], color='r', label='75% depth')
    perc95 = cumulative_trigger.max() * 0.95
    plt.axhline(val['z'][np.argmin(np.abs(cumulative_trigger-perc95))], color='r', label='95% depth')
    z_perc[key] = {'5': val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                   '75': val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                   '95': val['z'][np.argmin(np.abs(cumulative_trigger-perc95))]}
    CRR_perc[key] = {'5_75': [np.mean(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                                                              val['FS']<=1))[0]]),
                              np.var(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc75))],
                                                              val['FS']<=1))[0]])],
                     '5_95': [np.mean(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc95))],
                                                              val['FS']<=1))[0]]),
                              np.var(val['CRR'][np.where(np.logical_and(val['z']>=val['z'][np.argmin(np.abs(cumulative_trigger-perc5))],
                                                              val['z']<=val['z'][np.argmin(np.abs(cumulative_trigger-perc95))],
                                                              val['FS']<=1))[0]])]
                     }
    cnt += 1
    plt.gca().invert_yaxis()
    plt.ylabel('Depth [m]', fontsize=16)
    plt.ylabel('Depth [m]', fontsize=16)
    plt.legend()
    plt.xlabel('Cumulative triggerring count [-]', fontsize=16)
    fig.suptitle(type, fontsize=16)
    plt.savefig('..\\Depth FoS profile plots\\Road_' + key + '.png', bbox_inches='tight')

df = pd.DataFrame(data=list(zip(list(z_perc.keys()),
                                [val['5'] for val in z_perc.values()],
                                [val['75'] for val in z_perc.values()],
                                [val['95'] for val in z_perc.values()],
                                [val['5_75'][0] for val in CRR_perc.values()],
                                [val['5_75'][1] for val in CRR_perc.values()],
                                [val['5_95'][0] for val in CRR_perc.values()],
                                [val['5_95'][1] for val in CRR_perc.values()],
                                )),
                  columns=['CPT ID', '5% depth', '75% depth', '95% depth',
                           '5_75 CRR mean', '5_75 CRR var',
                           '5_95 CRR mean', '5_95 CRR var'])
df.to_csv('..\\Profiles_depths_5&75&95.csv', index=False)

key = 'CPT000000052437_IMBRO_A'
A = dict[key]
