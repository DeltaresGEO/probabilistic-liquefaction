import matplotlib.pylab as plt
import numpy as np
import os

def make_plots_NPR(gef_files, qc1n, IC, FC, qc1ncs, z_NAP, model, path_res, lito):
    # define limits
    lmt = [[0, 500], [0, 3], [0, 100], [0, 500]]
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[np.array(qc1n[0][idx]), np.array(qc1n[1][idx])],
                       [np.array(IC[0][idx]), np.array(IC[1][idx])],
                       [FC[0][idx], FC[1][idx]],
                       [np.array(qc1ncs[0][idx]), np.array(qc1ncs[1][idx])]],
                      [[z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]]],
                      [r'q$_{c1n}$ [-]',
                       r'IC [-]',
                       'FC [-]',
                       r'q$_{c1ncs}$ [-]'],
                      ['Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]'],
                      [[model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]]],
                      path_res, name, extension='png', limit=lmt, sand=lito[idx])

    return

def make_plots_comp(gef_files, qc1n, sigma_eff, IC, qc1ncs, z_NAP, model, path_res, lito):
    # define limits
    lmt = [[0, 500], [0, 250], [1, 4], [0, 500]]
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[np.array(qc1n[0][idx]), np.array(qc1n[1][idx])],
                       [np.array(sigma_eff[0][idx]), np.array(sigma_eff[1][idx])],
                       [np.array(IC[0][idx]), np.array(IC[1][idx])],
                       [np.array(qc1ncs[0][idx]), np.array(qc1ncs[1][idx])]],
                      [[z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]]],
                      [r'q$_{c1n}$ [-]',
                       r"$\sigma^{'}_{eff}$",
                       r'IC [-]',
                       r'q$_{c1ncs}$ [-]'],
                      ['Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]'],
                      [[model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]]],
                      path_res, name, extension='png', limit=lmt, sand=[False]*4)

    return


def make_plots_comp2(gef_files, CSR, CRR, FS, Ru, LPI, z_NAP, model, path_res, lito):
    # define limits
    lmt = [[0, 1], [0, 1], [0, 3], [0, 1]]
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[CSR[0][idx], CSR[1][idx]],
                       [CRR[0][idx], CRR[1][idx]],
                       [FS[0][idx], FS[1][idx]],
                       [Ru[0][idx], Ru[1][idx]],
                       # [phi[0][idx], phi[1][idx]],
                       ], #is geen psi maar Ru (omdat Psi is uitgezet)
                      [[z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       # [z_NAP[idx], z_NAP[idx]],
                       ],
                      [r'CSR [-]',
                       r'CRR [-]',
                       'FoS$_{liq}$ [-]',
                       'Ru [-]',
                       # r'$\psi$ [-]',
                       ],
                      ['Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       # 'Depth NAP [m]',
                       ],
                      [[model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]],
                       [model[0], model[1]],
                       # [model[0], model[1]],
                       ],
                      path_res, name, extension='png', limit=lmt, sand=[[lito[0][idx], lito[1][idx]]]*4, text_box=np.transpose(LPI)[idx])

    return

def make_plots_comp4(gef_files, tip, frc, frc_nb, water, z_NAP, model, path_res):
    # define limits
    # lmt = [[0, 500], [0, 250], [1, 4], [0, 500]]
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[np.array(tip[0][idx]) / 1000],
                       [np.array(frc[0][idx]) / 1000],
                       [np.array(frc_nb[0][idx])],
                       [np.array(water[0][idx])]],
                      [[z_NAP[idx]],
                       [z_NAP[idx]],
                       [z_NAP[idx]],
                       [z_NAP[idx]]],
                      [r'q$_{c}$ [MPa]',
                       r"Friction [MPa]",
                       r'Friction number [%]',
                       r'Pwp [kPa]'],
                      ['Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]'],
                      [[model[0]],
                       [model[0]],
                       [model[0]],
                       [model[0]]],
                      path_res, name, extension='png', sand=[False]*4)

    return

def color_map(dataX, dataY, dataZ, levels, y_label, data_label, file_path, name, lito, extension='eps',
              color_s=plt.cm.Spectral):
    # checks if file_path exits. If not creates file_path
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    # divide the data in hinterland "30" kruin "50" and outside "90"
    # loc = [int(dataX[i].split('.GEF')[0].split('DKP')[-1][-2:]) for i in range(len(dataX))]
    # km = [int(dataX[i].split('.GEF')[0].split('DKP')[-1][:-2]) for i in range(len(dataX))]
    loc = [int(dataX[i].split('.GEF')[0].split('DKP')[-1][-2:]) for i in range(len(dataX))]
    km = [int(dataX[i].split('.GEF')[0].split('DKP')[-1][:-2]) for i in range(len(dataX))]

    location = []
    for i in set(km):
        idx = [j for j, k in enumerate(km) if k == i]
        idx_hin = [30 for j in idx if loc[j] == 30]
        idx_kr = [50 for j in idx if loc[j] == 50]
        idx_out = [90 for j in idx if loc[j] == 90]

        location.append([i, idx_hin, idx_kr, idx_out])

    # sort location
    idx_sorted = [i[0] for i in sorted(enumerate(location), key=lambda x: x[1])]

    plt.subplots(3, len(location), figsize=(2 * len(location), 6))

    # define y limits
    y_max = [max(i) for i in dataY]
    y_min = [min(i) for i in dataY]

    for idx, val in enumerate(idx_sorted):
        for i in range(3):
            # cpt number
            x_data = [location[val][0] - 1, location[val][0] + 1]
            ax1 = plt.subplot(3, len(location), idx + len(location) * i + 1)
            if i == 0 and idx == 0:
                ax1.set_title('Berm')
            if i == 1 and idx == 0:
                ax1.set_title('Kruin')
            if i == 2 and idx == 0:
                ax1.set_title('Voorland')

            if idx == 0:
                plt.setp(ax1.get_yticklabels())
                plt.ylabel(y_label)
            else:
                plt.setp(ax1.get_yticklabels(), visible=False)

            if i == 2:
                plt.setp(ax1.get_xticklabels())
                # plt.xlabel(str(location[val][0]))
                plt.xticks([np.mean(x_data)], [str(location[val][0])])
            else:
                plt.xticks([np.mean(x_data)], [''])
                plt.setp(ax1.get_xticklabels(), visible=False)

            plt.ylim([min(y_min), max(y_max)])

            # if doesnt exist it skips
            if not location[val][i + 1]:
                # if it is the last one creates the legend before skipping
                if idx == len(idx_sorted) - 1:
                    cbar = plt.colorbar(CS)
                    cbar.ax.set_ylabel(data_label)
                    X, Y = np.meshgrid(x_data, dataY[ind])
                    Z = np.zeros((len(Y), 2))
                    CS = plt.contourf(X, Y, Z, levels,
                                      alpha=.6,
                                      cmap=plt.cm.Greys,
                                      origin='lower')
                continue

            # find location
            file = dataX[i].split('.GEF')[0].rstrip('1234567890') + str(location[val][0]) + str(
                location[val][i + 1][0]) + '.GEF'
            ind = dataX.index(file)

            X, Y = np.meshgrid(x_data, dataY[ind])

            # remove non sand
            ind_sand = [j for j, s in enumerate(lito[ind]) if s == "sand"]
            for j in ind_sand:
                dataZ[ind][j] = np.nan

            Z = [dataZ[ind], dataZ[ind]]
            CS = plt.contourf(X, Y, np.transpose(Z), levels,
                              alpha=.6,
                              cmap=color_s,
                              origin='lower')

            if idx == len(idx_sorted) - 1:
                cbar = plt.colorbar(CS)
                cbar.ax.set_ylabel(data_label)

    plt.tight_layout()
    plt.savefig(os.path.join(file_path, name) + '.' + extension, format=extension, dpi=900)
    plt.close()

    # plt.show()
    return


def make_plots_comp3(gef_files, CSR, CRR, FS, Ru, phi, z_NAP, model, path_res, lito):
    # define limits
    lmt = [[0, 500], [0, 3], [0, 1], [0, 10], [0, 3]]
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[CSR[idx]],
                       [CRR[idx]],
                       [FS[idx]],
                       [Ru[idx]],
                       [phi[idx]]],
                      [[z_NAP[idx]],
                       [z_NAP[idx]],
                       [z_NAP[idx]],
                       [z_NAP[idx]],
                       [z_NAP[idx]]],
                      [r'q$_{c1ncs}$',
                       r'IC',
                       r'CSR [-]',
                       r'CRR [-]',
                       'FoS$_{liq}$ [-]'],
                      ['Depth [m]',
                       'Depth [m]',
                       'Depth [m]',
                       'Depth [m]',
                       'Depth [m]'],
                      [[model],
                       [model],
                       [model],
                       [model],
                       [model]],
                      path_res, name, extension='png', limit=lmt, sand=lito[idx])
    return


def xy_plot_multi(x_values, y_values, x_label, y_label, label_name, file_path, name, extension='eps',
                  leg=0, limit=False, sand=False, text_box=False):
    # check extension. If different from png or eps it assumes eps (it is also the default).
    if extension != 'png' and extension != 'eps':
        extension = 'eps'

    # checks if file_path exits. If not creates file_path
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    # set the color list
    colormap = plt.cm.gist_ncar
    plt.gca().set_prop_cycle(color=[colormap(i) for i in np.linspace(0, 0.9, len(y_values))])

    plt.subplots(1, len(y_values), figsize=(20, 6))
    # plt.figure(num=1, figsize=(20, 6), dpi=80)
    # plt.axes().set_position([0.15, 0.1, 0.8, 0.8])

    # plot for each y_value
    for i, y_data in enumerate(y_values):
        plt.subplot(1, len(y_values), i + 1)
        for j in range(len(y_data)):

            if isinstance(text_box, np.ndarray) and i == 2:
                # l_name = label_name[i][j] + ' LPI=' + str(round(text_box[0][j], 1))
                l_name = label_name[i][j] + ' LPI=' + str(round(text_box[j], 1)) #ToDo dont know if this is the solution?
            else:
                l_name = label_name[i][j]

            if not isinstance(sand[i], bool):
                idx = [id for id, s in enumerate(sand[i][j]) if s == "sand"]
                plt.plot(x_values[i][j][idx], np.array(y_data[j])[idx], label=l_name, linestyle='',
                         marker='x', markersize=4)
            else:
                plt.plot(x_values[i][j], y_data[j], label=l_name)

        # plt.title(title)
        plt.xlabel(x_label[i], fontsize=12)
        plt.ylabel(y_label[i], fontsize=12)
        plt.grid('on')
        plt.legend(loc=leg, prop={'size': 12})

        if limit:
            plt.xlim(limit[i])

    plt.tight_layout()
    # legend location. default is loc=0 (best fit).
    # plt.legend(loc=leg, prop={'size': 14})
    # save the figure
    plt.savefig(os.path.join(file_path, name) + '.' + extension, format=extension, dpi=300)
    plt.close()

    return


def xy_plot(x_values, y_values, title, label_name, x_axis_label, y_axis_label, file_path, name, extension='eps',
            leg=0, xlim=None, ylim=None):

    # check extension. If different from png or eps it assumes eps (it is also the default).
    if extension != 'png' and extension != 'eps':
        extension = 'eps'

    # checks if file_path exits. If not creates file_path
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    # set the color list
    colormap = plt.cm.gist_ncar
    plt.gca().set_prop_cycle(color=[colormap(i) for i in np.linspace(0, 0.9, len(y_values))])

    plt.figure(num=1, figsize=(9, 6), dpi=80)
    plt.axes().set_position([0.15, 0.1, 0.8, 0.8])

    # plot for each y_value
    for i, y_data in enumerate(y_values):
        plt.plot(x_values[i], y_data, label=label_name[i])

    plt.title(title)
    plt.xlabel(x_axis_label, fontsize=14)
    plt.ylabel(y_axis_label, fontsize=14)
    plt.grid('on')

    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)

    # legend location. default is loc=0 (best fit).
    plt.legend(loc=leg, prop={'size': 14})
    # save the figure
    plt.savefig(os.path.join(file_path, name) + '.' + extension, format=extension, dpi=900)
    plt.close()

    return


def make_plots_base(gef_files, z_NAP, tip, friction_nb, gamma, gamma_lito, sigma_tot, sigma_eff, sigma_tot_l,
                    sigma_eff_l, path_res):
    # plot
    for idx, name in enumerate(gef_files):
        xy_plot_multi([[np.array(tip[idx]) / 1000, friction_nb[idx]],
                       [sigma_tot[idx], sigma_tot_l[idx]],
                       [sigma_eff[idx], sigma_eff_l[idx]],
                       [gamma[idx], gamma_lito[idx]]],
                      [[z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]],
                       [z_NAP[idx], z_NAP[idx]]],
                      ['Tip [MPa] / Friction number [-]',
                       'Total Stress [kPa]',
                       'Effective Stress [kPa]',
                       r'$\gamma$ [kN/m$^{3}$]'],
                      ['Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]',
                       'Depth NAP [m]'],
                      [['tip', 'friction nb'],
                       [r'Robertson & Cabal', r'Douglas & Olson'],
                       [r'Robertson & Cabal', r'Douglas & Olson'],
                       [r'Robertson & Cabal', r'Douglas & Olson']],
                      path_res, name, extension='png')

    return
