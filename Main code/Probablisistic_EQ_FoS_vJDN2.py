#kolk_bd
#script made for Mario Martinelli
#made to run in ProbToolkit
# qc1nCs and sigma_eff are input
# FC (Fine Content) is input
# formulas according to N:\Projects\11204000\11204496\B. Measurements and calculations\4-WP4\2-Sluice\Fragility_curve_sluice.pptx
## ---------
# bug 01 found in MSF : standard deviation should not be added always.
# bug 02 in MSF : MSF should be < 2.04

import numpy as np



coeffs = {602: {'alpha': [-0.2732, -0.3001, -0.8100, 0.1996, 0.0038, 0.4373],
               'beta': [2.0845, 1.1474, 0.9709, -0.2080, 0.0744, 0.1148, -0.0882, 0.2707, -0.0020, 0.1520]},
          603: {'alpha': [-0.0097, -0.2721, -0.4491, 0.1351, 0.0037, 0.4708],
                'beta': [2.1200, 1.0467, 0.6685, -0.2039, 0.0800, 0.1374, -0.0328, 0.5033, -0.0024, 0.1575]},
          604: {'alpha': [-0.1018, -0.2966, -0.0192, 0.1612, 0.0038, 0.4662],
                'beta': [2.1687, 1.2050, 0.8968, -0.2205, 0.0816, 0.0629, -0.0738, 0.4258, -0.0021, 0.1443]},
          801: {'alpha': [0.3877, -0.3213, -0.0984, 0.1620, -0.0007, 0.4575],
                'beta': [2.5725, 1.1450, 0.4983, -0.2296, 0.0734, 0.1634, 0.0437, 0.7463, -0.0041, 0.1908]},
          821: {'alpha': [-0.3764, -0.3080, -0.2753, 0.1634, 0.0045, 0.4571],
                'beta': [2.7957, 1.5813, 0.9813, -0.2732, 0.1003, 0.0599, -0.0743, 0.4674, -0.0039, 0.1889]},
          1001: {'alpha': [-0.1958, -0.3246, -0.6973, 0.1583, 0.0033, 0.4314],
                'beta': [2.1226, 1.0583, 0.7360, -0.2024, 0.0722, 0.1305, -0.0561, 0.4349, -0.0032, 0.1570]},
          1032: {'alpha': [-0.2487, -0.3131, -0.4085, 0.1766, 0.0038, 0.4446],
                'beta': [1.7208, 0.8872, 0.4872, -0.1697, 0.0638, 0.1247, -0.0189, 0.4720, -0.0017, 0.1465]},
          2001: {'alpha': [-0.4359, -0.2936, -0.0154, 0.1934, 0.0052, 0.4498],
                'beta': [2.2369, 1.1288, 0.8699, -0.2019, 0.0772, 0.1322, -0.0285, 0.4831, -0.0030, 0.1629]},
          'zandeweer': {'alpha': [0.6419, -0.3025, -0.7874, 0.1829, -0.0017, 0.4453],
                        'beta': [2.4832, 1.1972, 0.8834, -0.2194, 0.0772, 0.1472, -0.0662, 0.4002, -0.0037, 0.1405]},
          0: {'alpha': [-0.2487, -0.3131, -0.4085, 0.1766, 0.0038, 0.4446],
               'beta': [2.5725, 1.1450, 0.4983, -0.2296, 0.0734, 0.1634, 0.0437, 0.7463, -0.0041, 0.1908]},
          }
def lookupZone(zone):
    return coeffs[zone]['alpha'], coeffs[zone]['beta']

def calculation_fs(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, depth_layer, Cfc,
                   alpha=None, beta=None, Rd_value=None, ln_NeqM_value=None):

    qc1ncs, FC, dq = fine_correction(qc1n, IC, Cfc)

    # CRR calculation
    crr = crr_calc(qc1ncs, C0)

    # kappa sigma
    k_sigma, C_sigma = kappa_sigma(qc1ncs, sigma_eff)

    # determine CSR
    if beta is not None:
        csr, rd_mean, rd_SD = csr_calc(depth_layer, a_max, sigma_tot, sigma_eff, Vs12,  M=5.0, beta=beta, rd=Rd_value)
    else:
        csr = csr_calc(depth_layer, a_max, sigma_tot, sigma_eff, Vs12, M=5.0, beta=beta, rd=Rd_value)

    # FoS calculation crest - with safety factor
    if alpha is not None:
        FS, Ru, ln_neqM_mean, ln_neqM_SD = FoS(k_sigma, csr, crr, a_max, Vs12, kdr=1, FS_norm=1,  k_alpha=1,  theta=0.7,
                                               M=5.0, ln_neqM=ln_NeqM_value, alpha=alpha)
    else:
        FS, Ru = FoS(k_sigma, csr, crr, a_max, Vs12, kdr=1, FS_norm=1, k_alpha=1, theta=0.7,
                                   M=5.0, ln_neqM=ln_NeqM_value, alpha=alpha)

    if alpha is not None and beta is not None:
        return FS, Ru, qc1ncs, FC, crr, csr, rd_mean, rd_SD, ln_neqM_mean, ln_neqM_SD
    else:
        return FS, Ru, qc1ncs, FC, crr, csr


def get_stats(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, depth_layer, Cfc, zone):
    alpha, beta = lookupZone(zone)
    FS, Ru, qc1ncs, FC, crr, csr, rd_mean, rd_SD, ln_neqM_mean, ln_neqM_SD = \
        calculation_fs(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, depth_layer, Cfc, alpha=alpha, beta=beta)
    return FS, Ru, qc1ncs, FC, crr, csr, rd_mean, rd_SD, ln_neqM_mean, ln_neqM_SD


def ptk_calc(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, depth_layer, Cfc, Rd_value, ln_NeqM_value, thickness):
    settlement = []
    FS, Ru, qc1ncs, FC, crr, csr = \
        calculation_fs(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, depth_layer, Cfc,
                       Rd_value=Rd_value, ln_NeqM_value=ln_NeqM_value)
    Dr, epsV, gamma_max, Fult = calculation_epsV(qc1ncs,FS)
    # compute settlement of the layer
    settlement = epsV / 100 * thickness
    return FS, Ru, qc1ncs, FC, crr, csr, Dr, epsV, settlement


def calculation_epsV(qc1ncs,FS):
    # calculation based on NPR - 9998 - 2020
    epsV = []
    
    # compute relative density - Boulanger (2008) [%]
    Dr = 0.478 * np.array(qc1ncs)**0.264 - 1.063
    Dr = Dr * 100 
    
    # compute Fult
    if Dr < 39.23:
        Fult = 0.9524
    else:
        Fult = -0.0006 * Dr**2 + 0.047 * Dr + 0.032
        
    # compute gamma_max [%]
    if FS >= 2:
        gamma_max = 0.0
    else:
        if Fult < FS:
            gamma_max = 3.5 * (2 - FS) * (1 - Fult) / (FS - Fult)
        else:
            gamma_max = 1.0e15
    
    # compute epsV [%]
    if gamma_max < 8:
        epsV = 1.5 * gamma_max * np.exp(-0.025 * Dr)
    else:
        epsV = 12 * np.exp(-0.025 * Dr)   
            
    
    return Dr, epsV, gamma_max, Fult

def aging_fct(name, NAP, locx, ag_fct=1.3):
    aging = []
    # aging correction
    for ind, nam in enumerate(name):
        depth = locx[nam.split('.GEF')[0]]['pleistocene']

        ag_vec = np.ones(len(NAP[ind]))

        for i, k in enumerate(ag_vec):
            if NAP[ind][i] < depth:
                ag_vec[i] = ag_fct

        aging.append(ag_vec)
    return aging

def fine_correction(qc, IC, Cfc):

    qc1ncs_aux = qc
    FC = 80 * (IC + Cfc) - 137
    if FC > 100:
        FC = 100
    elif FC < 0:
        FC = 0

    dq = (11.9 + np.array(qc) / 14.6) * np.exp(1.63 - 9.7 / (FC + 2.) - (15.7 / (FC + 2.)) ** 2)

    qc1ncs_aux += dq

    qc1ncs = qc1ncs_aux
    fine_c = FC

    return qc1ncs, fine_c, dq

#=================================================================================================

def sigma_rd(beta,z,M):
    return beta[9]/(1+np.exp(-(np.log(max(z, 5))-(beta[1]+beta[5]*M))/(beta[2]+beta[6]*M)))

def csr_calc(depth_layer, a_max, stress, eff_stress, Vs12,  M=5.0, beta=None, rd=None):
    csr = []

    # calculation of CSR
    z = abs(depth_layer)

    if beta is None and rd is None:
        raise ValueError("csr_calc error: both beta and rd_value can't be set to none")

    if beta is not None:
        if a_max <= 0.3:
            Ard = beta[0] + beta[3] * min(float(M), 6.5) + beta[4] * np.log(a_max) + beta[8] * Vs12
        else:
            Ard = beta[0] + beta[3] * min(float(M), 6.5) + beta[4] * np.log(a_max) + beta[7] * np.log(a_max / 0.3) + beta[8] * Vs12
            
        rd = 1 - Ard / (1 + np.exp(-(np.log(z)-(beta[1] + beta[5] * M)) / (beta[2] + beta[6] * M)))
        
        if rd > 1.0:
            rd = 1.0
            
    csr = 0.65 * stress / eff_stress * a_max * rd    # is gravity missing here
        
    if beta is not None:
        print("Mean_rd:", rd)
        print("Sigma_rd:", sigma_rd(beta, z, M))
        return csr, rd, sigma_rd(beta, z, M)
    return csr

#=================================================================================================

def crr_calc(qc1n, C0):
    # calculate CRR
    crr = []
    qc1n = np.array(qc1n)
    crr = (np.exp((qc1n / 113.) + (qc1n / 1000.) ** 2 - (qc1n / 140.) ** 3 + (qc1n / 137.) ** 4 - C0))

    if crr > 0.6:
       crr = 0.6

    return crr

#=================================================================================================

def kappa_sigma(qc1n, sigma_eff):
    # calculate kappa sigma, qc1n == qc1ncs

    # atmospheric pressure
    Pa = 100

    kappa = []
    # qc1n = np.array(qc1n)
    # check if qc1n is positive
    if qc1n < 0:
        qc1n = 0

    C_sigma = 1 / (37.3 - 8.27 * qc1n**0.264)
    if qc1n > 211:
       C_sigma = 0.3
    elif C_sigma > 0.3:
         C_sigma = 0.3

    # kappa_sigma
    k_sigma = 1 - C_sigma * np.log(sigma_eff / Pa)
    if k_sigma > 1.1:
       k_sigma = 1.1

    # kappa.append(k_sigma)

    return k_sigma, C_sigma


def FoS(k_sigma, csr, crr, a_max, Vs12, kdr=1,  FS_norm=1, k_alpha=1,  theta=0.7, M=5.0, alpha=None, ln_neqM=None):
    # calculate FoS and Ru
    FS = []
    Ru = []

    if (alpha is not None and ln_neqM is not None) or (alpha is None and ln_neqM is None):
        raise ValueError("FoS Error: alpha and neqM conflict")

    # MSF calculation
    if alpha is not None:
        if a_max <= 0.3 :
            ln_neqM = alpha[0] + alpha[1] * np.log(a_max) + alpha[3] * M + alpha[4] * Vs12
        else:
            ln_neqM = alpha[0] + alpha[1] * np.log(a_max) + alpha[2] * np.log(a_max/0.3) + \
                      alpha[3] * M + alpha[4] * Vs12

        print("Mean ln_neqM:", ln_neqM)
        print("Sigma ln_neqM:", alpha[-1])

    neqM = np.exp(ln_neqM)
    MSF = min((7.25 / neqM) ** 0.34, 2.04)

    b = 0.34 #0.2 / theta

    fos = crr / csr * MSF * k_sigma * k_alpha * kdr

    # apply safety factor
    fos = fos / FS_norm
    if fos < 0:
        fos = 0

    FS = fos

    Ru_aux = 2 / np.pi * np.arcsin(fos ** (-1 / (2 * b * theta)))
    if fos == float('Inf'):
        Ru_aux= 0
    if fos <= 1:
        Ru_aux = 1.0
    Ru=Ru_aux

    # # correction for zone above freatic level #todo check if this is correct
    # for j in range(len(FS[0])):
    #     if water[0][j] == 0:
    #     FS[0][j] = 0
    #     Ru[0][j] = 0

    if alpha is not None:
        return FS, Ru, ln_neqM, alpha[-1]
    return FS, Ru


if __name__ == '__main__':
    qc1n = 10
    IC = 2.0
    sigma_eff = 50
    sigma_tot = 100
    C0 = 2.63
    Cfc = 0.29
    Vs12 = 120
    dept_layer = 5
    a_max = 0.4
    zone = 0
    thickness = 3.0

    # This is an call gets the stats for Probabilistic Toolkit
    print("\nStats for Zone "+str(zone)+" and Data:\n======================================================")
    res = get_stats(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, dept_layer, Cfc, zone)

    # Once in probabilistic tools kit it calls
    # lets assume it does a calculation with the mean Neq_M and Rd values then the results should be the same
    Rd_value = res[-4]
    ln_NeqM_value = res[-2]

    res_pkt = ptk_calc(qc1n, IC, sigma_eff, sigma_tot, a_max, Vs12, C0, dept_layer, Cfc, Rd_value, ln_NeqM_value, thickness)

    print("\nSanity Check:\n======================================================")
    print("Deterministic: \t", res[:-4])
    print("Pbtk Det. Calc:\t", res_pkt)