import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import pickle
import Liquefaction_FoS as LFoS
from scipy.stats import t

#-----------------------------------------------------------------------------------------------------------------------

path = '..\\Railway analysis\\Groningen - AllRail - Actual PGA - Screened locations'
picklefile = open(path+'\\'+'Groningen_AllRail_output_model.pickle', 'rb')

# path = '..\\Road analysis\\Groningen - AllRoad - Actual PGA - Screened locations'
# picklefile = open(path+'\\'+'Groningen_AllRoad_output_model.pickle', 'rb')

model_dict = pickle.load(picklefile)

df_selected = pd.read_csv('..\\Groningen locations.csv')
cpt_lst = list(df_selected['CPT ID'])

for i, (key, val) in enumerate(model_dict.items()):
    z = val['MC'].z.flatten()
    CRR = val['MC'].CRR.flatten()
    qc = val['MC'].qc.flatten()
    fs = val['MC'].fs.flatten()
    qc1ncs = val['MC'].qc1ncs.flatten()
    Ic = val['MC'].Ic.flatten()
    FoS = val['MC'].FoS.flatten()

    if key in cpt_lst:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(qc, z, color='k', label='${q}_{c}$')
        ax.set_xlabel('${q}_{c}$ [MPa]')
        ax.set_ylabel('Depth [-]')
        ax.legend(loc=0)
        ax.invert_yaxis()
        ax2 = plt.twiny(ax)
        ax2.plot(fs/(qc*1000)*100, z, color='r', label='Ratio')
        ax2.set_xlabel('Friction ratio [%]')
        ax2.legend(loc=5)
        plt.suptitle(key, fontsize=16)
        plt.savefig('..\\Depth qc plots\\' + key + '.png', bbox_inches='tight')
