import numpy as np
import scipy as sp
import pandas as pd
import Liquefaction_FoS as L_FoS
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl
from tqdm import tqdm
from sklearn.linear_model import LinearRegression
from scipy import interpolate
import math
import pickle

from pathlib import Path
from geolib_plus.gef_cpt import GefCpt
from geolib_plus.robertson_cpt_interpretation import RobertsonCptInterpretation
from geolib_plus.robertson_cpt_interpretation import UnitWeightMethod
from geolib_plus.robertson_cpt_interpretation import OCRMethod
from geolib_plus.robertson_cpt_interpretation import ShearWaveVelocityMethod

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class liq_sampler:

    def __init__(self, z, qc, fs, a_max_g, M, Vs_12, pwp, gwl, zone, n_samples, FC_bnd=100, Co=2.6318706, Co_sd=0.467):
        self.z = z
        self.qc = qc
        self.fs = fs
        self.Rf = fs / (qc * 10**3) *100
        self.a_max_g = a_max_g
        self.a_max = self.a_max_g * 9.81
        self.M = M
        self.Vs_12 = Vs_12
        self.pwp = pwp
        self.gwl = gwl
        self.n_samples = n_samples
        self.n_depths = z.shape[0]
        self.FC_bnd = FC_bnd
        self.Co_mu = Co
        self.Co_sd = Co_sd
        # Co_char = 2.8118706

        Gron_coeffs_dict = {602: {'alpha': [-0.2732, -0.3001, -0.8100, 0.1996, 0.0038, 0.4373],
                                  'beta': [2.0845, 1.1474, 0.9709, -0.2080, 0.0744, 0.1148, -0.0882, 0.2707, -0.0020,
                                           0.1520]},
                            603: {'alpha': [-0.0097, -0.2721, -0.4491, 0.1351, 0.0037, 0.4708],
                                  'beta': [2.1200, 1.0467, 0.6685, -0.2039, 0.0800, 0.1374, -0.0328, 0.5033, -0.0024,
                                           0.1575]},
                            604: {'alpha': [-0.1018, -0.2966, -0.0192, 0.1612, 0.0038, 0.4662],
                                  'beta': [2.1687, 1.2050, 0.8968, -0.2205, 0.0816, 0.0629, -0.0738, 0.4258, -0.0021,
                                           0.1443]},
                            801: {'alpha': [0.3877, -0.3213, -0.0984, 0.1620, -0.0007, 0.4575],
                                  'beta': [2.5725, 1.1450, 0.4983, -0.2296, 0.0734, 0.1634, 0.0437, 0.7463, -0.0041,
                                           0.1908]},
                            821: {'alpha': [-0.3764, -0.3080, -0.2753, 0.1634, 0.0045, 0.4571],
                                  'beta': [2.7957, 1.5813, 0.9813, -0.2732, 0.1003, 0.0599, -0.0743, 0.4674, -0.0039,
                                           0.1889]},
                            1001: {'alpha': [-0.1958, -0.3246, -0.6973, 0.1583, 0.0033, 0.4314],
                                   'beta': [2.1226, 1.0583, 0.7360, -0.2024, 0.0722, 0.1305, -0.0561, 0.4349, -0.0032,
                                            0.1570]},
                            1032: {'alpha': [-0.2487, -0.3131, -0.4085, 0.1766, 0.0038, 0.4446],
                                   'beta': [1.7208, 0.8872, 0.4872, -0.1697, 0.0638, 0.1247, -0.0189, 0.4720, -0.0017,
                                            0.1465]},
                            2001: {'alpha': [-0.4359, -0.2936, -0.0154, 0.1934, 0.0052, 0.4498],
                                   'beta': [2.2369, 1.1288, 0.8699, -0.2019, 0.0772, 0.1322, -0.0285, 0.4831, -0.0030,
                                            0.1629]},
                            'zandeweer': {'alpha': [0.6419, -0.3025, -0.7874, 0.1829, -0.0017, 0.4453],
                                          'beta': [2.4832, 1.1972, 0.8834, -0.2194, 0.0772, 0.1472, -0.0662, 0.4002,
                                                   -0.0037,
                                                   0.1405]},
                            0: {'alpha': [-0.2487, -0.3131, -0.4085, 0.1766, 0.0038, 0.4446],
                                'beta': [2.5725, 1.1450, 0.4983, -0.2296, 0.0734, 0.1634, 0.0437, 0.7463, -0.0041,
                                         0.1908]},
                            }

        self.Gron_coeffs = Gron_coeffs_dict[zone]

    def set_rd_dist(self):
        beta = self.Gron_coeffs['beta']

        if beta is not None:
            if self.a_max_g <= 0.3:
                Ard = beta[0] + beta[3] * min(float(self.M), 6.5) + beta[4] * np.log(self.a_max_g) + beta[8] * self.Vs_12
            else:
                Ard = beta[0] + beta[3] * min(float(self.M), 6.5) + beta[4] * np.log(self.a_max_g) + beta[7] * \
                      np.log(self.a_max_g / 0.3) + beta[8] * self.Vs_12

        rd_mu = 1 - Ard / (1 + np.exp(-(np.log(self.z) - (beta[1] + beta[5] * self.M)) / (beta[2] + beta[6] * self.M)))

        rd_mu = np.minimum(rd_mu, np.ones(rd_mu.shape[0]))

        rd_sd = beta[9] / (1 + np.exp(-(np.log(np.maximum(self.z, np.ones_like(self.z) * 5)) - (beta[1] + beta[5] * self.M)) / (beta[2] + beta[6] * self.M)))
        # rd_sd[np.where(self.z<=0)] = 0

        self.rd_mu = np.maximum(rd_mu,0)
        self.rd_sd = rd_sd

    def draw_rd_samples(self):
        random_normal_draw = np.random.randn(self.n_samples).reshape(1,-1)
        # rd_samples = self.rd_mu[:, None] + self.rd_sd[:, None] * np.tile(random_normal_draw,(self.n_depths,1))
        rd_samples = self.rd_mu[:, None] + self.rd_sd[:, None] * np.tile(np.zeros(self.n_samples),(self.n_depths,1))
        # rd_samples = np.maximum(rd_samples, 0)
        return np.maximum(rd_samples,0)

    def set_ln_neqM_dist(self):
        alpha = self.Gron_coeffs['alpha']
        if alpha is not None:
            if self.a_max_g <= 0.3:
                ln_neqM = alpha[0] + alpha[1] * np.log(self.a_max_g) + alpha[3] * self.M + alpha[4] * self.Vs_12
            else:
                ln_neqM = alpha[0] + alpha[1] * np.log(self.a_max_g) + alpha[2] * np.log(self.a_max_g / 0.3) + \
                          alpha[3] * self.M + alpha[4] * self.Vs_12


        MSF_mu = np.minimum( (7.25 / np.exp(ln_neqM))**0.34, 2.04)
        ln_MSF_sd = 0.34 * alpha[-1]
        MSF_sd = np.sqrt((np.exp(ln_MSF_sd**2) - 1) * MSF_mu**2)
        ln_MSF_mu = np.log(MSF_mu**2 / np.sqrt(MSF_sd**2 + MSF_mu**2))

        self.MSF_mu = MSF_mu
        self.ln_MSF_mu = ln_MSF_mu
        self.ln_MSF_sd = ln_MSF_sd

    def draw_MSF_samples(self):
        random_normal_draw = np.random.randn(self.n_samples).reshape(1,-1)
        MSF_samples = np.exp(self.ln_MSF_mu + self.ln_MSF_sd * random_normal_draw)
        return MSF_samples

    def draw_Cfc_samples(self, Cfc_sd=0.29):
       return np.random.randn(self.n_depths, self.n_samples) * Cfc_sd

    def draw_Co_samples(self):
       return self.Co_mu + np.random.randn(self.n_samples) * self.Co_sd

    def initialize_variables(self):
        self.set_rd_dist()
        self.set_ln_neqM_dist()

    def run_MC(self):
        self.rd_samples = self.draw_rd_samples()
        self.MSF_samples = self.draw_MSF_samples()
        self.Co_samples = self.draw_Co_samples()
        self.Cfc_samples = self.draw_Cfc_samples()
        self.FoS = np.zeros((self.n_depths, self.n_samples))
        self.Ic = np.zeros((self.n_depths, self.n_samples))
        self.qc1ncs = np.zeros((self.n_depths, self.n_samples))
        self.CRR = np.zeros((self.n_depths, self.n_samples))
        self.LPI = np.zeros(self.n_samples)
        self.S = np.zeros(self.n_samples)
        for i, (rd, MSF, Co, Cfc) in enumerate(zip(self.rd_samples.T, self.MSF_samples.T,self.Co_samples.T, self.Cfc_samples.T)):

            # self.FoS[:, i],_,_ = L_FoS.calc_FoS(qc=self.qc, fs=self.fs, a_max=self.a_max, pwp=self.pwp,
            #                                     gwl=self.gwl, z=self.z, rd=rd, MSF=MSF, Co=Co,
            #                                     Cfc=Cfc, FC_bnd=self.FC_bnd)
            # self.LPI[i] = L_FoS.calc_LPI(self.FoS[:, i], self.z)

            self.FoS[:, i], self.Ic[:, i], FC, self.qc1ncs[:, i], self.CRR[:, i] = L_FoS.calc_FoS_KF(z=self.z, qc=self.qc, fs=self.fs, gwl=self.gwl,
                                                               pwp=self.pwp, a_max=self.a_max, rd=rd, MSF=MSF, Cfc=Cfc,
                                                               Co=Co, Pa=101.3, K_alpha=1, FC_bnd=self.FC_bnd,
                                                               pleistocene_corr=1.3, fines_type='NPR_9998_2020')

            # _, self.LPI[i] = L_FoS.calc_LPI_KF(z=self.z, FoS=self.FoS[:, i], Ic=Ic)
            self.LPI[i], _ = L_FoS.calc_LPI_KF(z=self.z, FoS=self.FoS[:, i], Ic=self.Ic[:, i])

            self.S[i] = L_FoS.calc_settlement(FoS=self.FoS[:, i], qc1ncs=self.qc1ncs[:, i], z=self.z, Ic=self.Ic[:, i])

    def calc_LPI_non_exceed_pob(self, exceed_val):
        LPI_non_exceed_prob = list(map(lambda x: np.count_nonzero(self.LPI <= x) / self.n_samples * 100, exceed_val))
        return LPI_non_exceed_prob

    def calc_S_non_exceed_pob(self, exceed_val):
        S_non_exceed_prob = list(map(lambda x: np.count_nonzero(self.S*100 <= x) / self.n_samples * 100, exceed_val))
        return S_non_exceed_prob

    def run_deterministic(self, Co_det=2.812):
        rd = self.rd_mu
        MSF = self.MSF_mu
        Co = Co_det
        Cfc = np.zeros(self.n_depths)

        # self.FoS_det, self.Ic, self.FC, qc1ncs = L_FoS.calc_FoS(qc=self.qc, fs=self.fs, a_max=self.a_max,pwp=self.pwp,
        #                                                 gwl=self.gwl, z=self.z, rd=rd, MSF=MSF, Co=Co,
        #                                                 Cfc=Cfc, FC_bnd=self.FC_bnd)
        # self.LPI_det = L_FoS.calc_LPI(self.FoS_det, self.z)

        self.FoS_det, self.Ic, self.FC, qc1ncs, CRR = L_FoS.calc_FoS_KF(z=self.z, qc=self.qc, fs=self.fs,  gwl=self.gwl,
                                                                   pwp=self.pwp, a_max=self.a_max, rd=rd, MSF=MSF,
                                                                   Cfc=Cfc, Co=Co, Pa=101.3 , K_alpha=1, FC_bnd=self.FC_bnd,
                                                                   pleistocene_corr=1.3, fines_type='NPR_9998_2020')

        # _, self.LPI_det = L_FoS.calc_LPI_KF(z=self.z, FoS=self.FoS_det, Ic=self.Ic)
        self.LPI_det, _ = L_FoS.calc_LPI_KF(z=self.z, FoS=self.FoS_det, Ic=self.Ic)

        self.S_det = L_FoS.calc_settlement(FoS=self.FoS_det, qc1ncs=qc1ncs, z=self.z, Ic=self.Ic)

    def plot_depth_results(self):

        fig, ax = plt.subplots(1,3, figsize=(8,5))

        ax[0].plot(self.qc, self.z, color='b')
        ax[0].invert_yaxis()
        ax[0].set_xlabel('qc [MPa]', fontsize=14)
        ax[0].xaxis.label.set_color('b')
        ax[0].set_xlim([0, 40])
        ax[0].set_ylabel('Depth [m]', fontsize=14)
        ax_dum = ax[0].twiny()
        ax_dum.plot(self.Rf, self.z, color='r')
        ax_dum.set_xlabel('Friction ratio [%]', fontsize=14)
        ax_dum.xaxis.label.set_color('r')
        ax_dum.set_xlim([0, 15])
        ax_dum.invert_xaxis()

        ax[1].plot(self.Ic, self.z, color='b')
        ax[1].invert_yaxis()
        ax[1].set_xlabel('Ic [-]', fontsize=14)
        ax[1].xaxis.label.set_color('b')
        ax[1].set_xlim([1, 4])
        ax[1].axvline(2.6, color='r', linestyle='--')
        ax[1].yaxis.set_visible(False)
        ax_dum = ax[1].twiny()
        ax_dum.plot(self.FC, self.z, color='r')
        ax_dum.set_xlabel('Fines percentage [%]', fontsize=14)
        ax_dum.xaxis.label.set_color('r')
        ax_dum.set_xlim([0, 40])
        ax_dum.invert_xaxis()

        for i, fos in enumerate(self.FoS.T):
            idx = np.where(fos<=9999)
            ax[2].scatter(fos[idx], self.z[idx], marker='+', color='k', alpha=0.2, s=1)
        idx = np.where(self.FoS_det <= 9999)
        ax[2].scatter(self.FoS_det[idx], self.z[idx], marker='+', color='r', zorder=1111, label='Deterministic FoS')
        ax[2].invert_yaxis()
        ax[2].set_xlabel('FoS [-]', fontsize=14)
        ax[2].set_xlim([0, 2])
        ax[2].axvline(1.0, color='r', linestyle='--')
        ax[2].yaxis.set_visible(False)
        ax[2].legend()


#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class model:

    def __init__(self, path, gef_folder, input_filename, LPI_vals=[1,5,10], S_vals=[2,4,6,8,10], n_samples=100, per_PGA=False):
        self.path = path
        self.gef_folder = gef_folder
        self.per_PGA = per_PGA
        self.cpt_files = [f for f in listdir(path + '\\' + gef_folder) if isfile(join(path + '\\' + gef_folder, f))]
        self.cpt_names = [cpt_file.split('.')[0] for cpt_file in self.cpt_files]

        # self.cpt_files = ['CPT000000097741_IMBRO_A.gef']
        # self.cpt_names = ['CPT000000097741_IMBRO_A']

        self.cpt_files =  self.cpt_files[:600]
        self.cpt_names =  self.cpt_names[:600]

        self.input_file = pd.read_csv(path + '\\' + input_filename + '.csv',).set_index('CPT_ID')
        self.n_samples = n_samples
        self.LPI_vals = LPI_vals
        self.S_vals = S_vals
        # self.run_model()

    def run_model(self):

        if not self.per_PGA:
            regression_func = self.set_amax_prediction_func()

        self.results = {}
        cpt_files_to_remove = []
        cpt_names_to_remove = []

        cnt = 1
        for (cpt_file, cpt_name) in tqdm(zip(self.cpt_files,self.cpt_names)):
            z, qc, fs, pwp, gwl, coord = self.read_cpt_gef(cpt_filename = self.path + '\\' + self.gef_folder + '\\' + cpt_file)
            if not isinstance(z, np.ndarray):# GEF files cant be processed - this leads to z=None != ndarray
                cpt_files_to_remove.append(cpt_file)
                cpt_names_to_remove.append(cpt_name)
            else:
                if self.per_PGA:
                    amax = self.input_file.loc[[cpt_name], ['a_max']].values[0].item()
                else:
                    amax = max(0.05, regression_func(coord[0], coord[1]))
                mc = liq_sampler(z=z, qc=qc, fs=fs, a_max_g= amax,
                                 M= self.input_file['M'][cpt_name], Vs_12= self.input_file['Vs_12'][cpt_name],
                                 pwp=pwp, gwl=gwl, zone= self.input_file['zone'][cpt_name],
                                 FC_bnd=self.input_file['FC'][cpt_name], n_samples= self.n_samples)
                mc.initialize_variables()
                mc.run_deterministic()
                mc.run_MC()
                LPI_non_exceed_prob = mc.calc_LPI_non_exceed_pob(self.LPI_vals)
                S_non_exceed_prob = mc.calc_S_non_exceed_pob(self.S_vals)
                LPI_exceed_prob = [100 - item for item in LPI_non_exceed_prob]
                LPI_dict = {key: val for (key, val) in zip(self.LPI_vals, LPI_exceed_prob)}
                S_exceed_prob = [100 - item for item in S_non_exceed_prob]
                S_dict = {key: val for (key, val) in zip(self.S_vals, S_exceed_prob)}
                cpt_mc_run = {'coord': coord, 'amax':amax, 'FoS Det':mc.FoS_det,'S Det':mc.S_det,
                              'LPI Det':mc.LPI_det, 'MC': mc, 'P(LPI_exceedance)': LPI_dict, 'P(S_exceedance)': S_dict}
                self.results[cpt_name] = cpt_mc_run
                print(str(cnt) + "/" + str(len(self.cpt_files)))
                cnt += 1
        [self.cpt_names.remove(name) for name in cpt_names_to_remove]
        [self.cpt_files.remove(file) for file in cpt_files_to_remove]
        self.generate_dataframe()

    def write_model_pickle(self, name, path=None):
        if path == None:
            path = self.path
        with open(path+'\\'+name+'.pickle', 'wb') as handle:
            pickle.dump(self.results, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def read_model_pickle(self, name, path=None):
        if path == None:
            path = self.path
        with open(path+'\\_'+name+'.pickle', 'wb', 'rb') as handle:
            model = pickle.load(handle)
        return model

    def read_cpt_gef(self, cpt_filename):
        cpt_file_gef = Path(cpt_filename)

        cpt_gef = GefCpt()
        cpt_gef.read(cpt_file_gef)
        # cpt_gef.pre_process_data()

        if not isinstance(cpt_gef.friction, np.ndarray): # GEF files cant be procesed - this leads to friction=None != ndarray
            return (None, None, None, None, None, None)
        # elif ((not isinstance(cpt_gef.pore_pressure_u1, np.ndarray)) and
        #       (not isinstance(cpt_gef.pore_pressure_u2, np.ndarray)) and
        #       (not isinstance(cpt_gef.pore_pressure_u3, np.ndarray))):
        # elif (not isinstance(cpt_gef.pore_pressure_u2, np.ndarray)):
            # return (None, None, None, None, None, None, None)
        else:

            # if isinstance(cpt_gef.friction, np.ndarray):
            #     idx_keep = np.where(np.all(np.c_[cpt_gef.tip!=0, cpt_gef.friction!=0,
            #                                  cpt_gef.friction!=cpt_gef.error_codes['friction'],
            #                                  cpt_gef.pore_pressure_u2!=cpt_gef.error_codes['pore_pressure_u2']], axis=1))[0]
            # else:
            idx_keep = np.where(np.all(np.c_[cpt_gef.tip != 0, cpt_gef.friction != 0,
                                             cpt_gef.friction != cpt_gef.error_codes['friction']],axis=1))[0]

            if idx_keep.size == 0:
                z = cpt_gef.penetration_length
                qc = cpt_gef.tip
                fs = cpt_gef.friction
                # pwp = cpt_gef.pore_pressure_u2
            else:
                z = cpt_gef.penetration_length[idx_keep]
                qc = cpt_gef.tip[idx_keep]
                fs = cpt_gef.friction[idx_keep]
                # pwp = cpt_gef.pore_pressure_u2[idx_keep] * 10**3

            if np.any(cpt_gef.depth == 0):
                idx_non_zero = self.clean_zero_depth(z)
            else:
                idx_non_zero = np.arange(z.shape[0])



            cpt_gef.pre_process_data()

            interpreter = RobertsonCptInterpretation()
            interpreter.unitweightmethod = UnitWeightMethod.LENGKEEK

            # cpt_surf_NAP = cpt_gef.depth_to_reference[0] + cpt_gef.depth[0]

            # interpreter.user_defined_water_level = True
            cpt_gef.interpret_cpt(interpreter)


            # interpreter.user_defined_water_level = True
            # cpt_gef.pwp = -1.4
            # cpt_gef.interpret_cpt(interpreter)

            # pwp = cpt_gef.pore_pressure_u2 - (cpt_gef.pwp - gwl_cpt) * 9.81 * 10**(-3)

            # if np.any(cpt_gef.depth == 0):
            #     idx_non_zero = self.clean_zero_depth(cpt_gef.depth)
            # else:
            #     idx_non_zero = np.arange(cpt_gef.depth.shape[0])
            #
            # return (cpt_gef.depth[idx_non_zero], cpt_gef.qt[idx_non_zero] * 10 ** (-3),
            #         cpt_gef.friction[idx_non_zero] * 10**3, cpt_gef.total_stress[idx_non_zero],
            #         # cpt_gef.total_stress[idx_non_zero] - cpt_gef.pore_pressure_u2[idx_non_zero] * 10**3,
            #         cpt_gef.total_stress[idx_non_zero] - pwp[idx_non_zero] * 10**3,
            #         gwl, cpt_gef.coordinates)

            gwl_cpt = np.abs(cpt_gef.pwp + (cpt_gef.depth[0]-cpt_gef.depth_to_reference[0]))

            # idx_pwp = np.where(np.all(np.c_[z>gwl_cpt, pwp<0],axis=1))[0]
            # pwp[idx_pwp] = (z[idx_pwp] - gwl_cpt) * 9.81
            # pwp[z<gwl_cpt] = 0


            # a = 1
            # qt = qc + (1 - a) * pwp

            pwp = np.where(z>gwl_cpt, 9.81 * (z - gwl_cpt), 0)

            return (z[idx_non_zero], qc[idx_non_zero], fs[idx_non_zero] * 10**3,
                    pwp[idx_non_zero], gwl_cpt, cpt_gef.coordinates)


    def set_amax_prediction_func(self):
        df_amax = pd.read_csv('D:\\Probabilistic_Liquefaction\\Data\\amax_data.csv', index_col=False)

        # reg = LinearRegression().fit(df_amax[['X', 'Y']].to_numpy().reshape(-1,2), df_amax['amax'].to_numpy().reshape(-1,1))
        # f = lambda x: reg.predict(x)

        f = interpolate.interp2d(df_amax['X'].to_numpy(),df_amax['Y'].to_numpy(),df_amax['amax'].to_numpy(), kind='linear')

        return f

    def clean_zero_depth(self, z):
        idx_zero = np.where(z == 0)[0]
        return np.delete(np.arange(z.shape[0]), idx_zero)

    def generate_dataframe(self, perc_lst=[5, 50, 95]):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord', 'LPIish Det', 'S Det [cm]'] +
                                           ['P(LPI>' + str(val) for val in self.LPI_vals] +
                                           ['P(S>' + str(val) + '[cm]' for val in self.S_vals])
        for cpt_name in self.cpt_names:
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1], res['LPI Det'], res['S Det']*100] +
                                         [round(res['P(LPI_exceedance)'][val],3) for val in self.LPI_vals] +
                                         [round(res['P(S_exceedance)'][val],3) for val in self.S_vals], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)

        for perc in perc_lst:
            df_LPI_perc = self.calc_LPI_percentiles(perc=perc)
            df['LPI_'+str(round(perc, 2))] = df_LPI_perc['LPI_perc']

        for perc in perc_lst:
            df_S_perc = self.calc_S_percentiles(perc=perc)
            df['S_'+str(round(perc, 2))] = df_S_perc['S_perc']

        self.df = df

    def export_df(self, filename):
        self.df.to_csv(filename+'.csv', index=False)

    def calc_LPI_exceed_cpts(self, LPI_val):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord','P_LPI'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          100 - res['MC'].calc_LPI_non_exceed_pob([LPI_val])[0]], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def calc_S_exceed_cpts(self, S_val):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord','P_S'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          100 - res['MC'].calc_S_non_exceed_pob([S_val])[0]], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def calc_LPI_percentiles(self, perc=95):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord', 'LPI_perc'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          np.percentile(res['MC'].LPI, q=perc)], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def calc_S_percentiles(self, perc=95):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord', 'S_perc'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          np.percentile(res['MC'].S *100, q=perc)], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def calc_LPI_det_cpts(self):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord','LPI'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          res['LPI Det']], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def calc_S_det_cpts(self):
        df = pd.DataFrame(data=[], columns=['CPT ID', 'X coord', 'Y coord','LPI', 'S'])
        for i, cpt_name in enumerate(self.cpt_names):
            res = self.results[cpt_name]
            cpt_results = pd.Series(data=[cpt_name, res['coord'][0], res['coord'][1],
                                          res['LPI Det'], res['S Det']*100], index=df.columns)
            df = df.append(cpt_results, ignore_index=True)
        return df

    def plot_over_depth(self, cpt_list):
        for cpt in cpt_list:
            self.results[cpt]['MC'].plot_depth_results()

    def plot_det_LPI_map(self, path, show_cpt_id=False, LPIish_max=20):

        df = self.calc_LPI_det_cpts()

        cmap  = plt.cm.get_cmap('jet')
        bounds = np.linspace(0, LPIish_max, 6)
        norm = mpl.colors.BoundaryNorm(bounds, cmap .N)

        fig = plt.figure()
        ax = plt.gca()
        for i in range(len(df)):
            if df['LPI'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['LPI'].loc[i], norm=norm, s=35, cmap=cmap , marker='^', zorder=1111)
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap ), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        cbar.ax.set_ylabel('LPIish [-]', rotation=270, fontsize=14, labelpad=+20)
        if show_cpt_id:
            for (id, x, y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Map of deterministic LPIish calculation', fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'Map of deterministic LPIish calculation' + '.png')

    def plot_det_settlement_map(self, path, show_cpt_id=False, S_max=10):
        df = self.calc_S_det_cpts()

        cmap = plt.cm.get_cmap('jet')
        bounds = np.linspace(0, S_max, 6)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        fig = plt.figure()
        ax = plt.gca()
        for i in range(len(df)):
            if df['S'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['S'].loc[i], norm=norm, s=35, cmap=cmap, marker='^', zorder=1111)
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap ), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        cbar.ax.set_ylabel('Settlement [cm]', rotation=270, fontsize=14, labelpad=+20)
        if show_cpt_id:
            for (id, x, y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Map of deterministic settlement calculation', fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'Map of deterministic settlement calculation' + '.png')

    def plot_LPI_map_points(self, LPI_val, path, show_cpt_id=False):

        df = self.calc_LPI_exceed_cpts(LPI_val)

        cmap = plt.cm.get_cmap('jet')
        bounds = np.arange(0,101,20)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        fig = plt.figure()
        ax = plt.gca()

        for i in range(len(df)):
            if df['P_LPI'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['P_LPI'].loc[i], vmin=0, vmax=100, s=35, cmap=cmap, marker= '^')
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap ), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        if show_cpt_id:
            for (id,x,y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Probability of exceedance map for LPIish='+str(round(LPI_val,2)), fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'Probability of exceedance map for LPIish='+str(round(LPI_val,2)) + '.png')

    def plot_S_map_points(self, S_val, path, show_cpt_id=False):

        df = self.calc_S_exceed_cpts(S_val=S_val)

        cmap = plt.cm.get_cmap('jet')
        bounds = np.arange(0,101,20)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        fig = plt.figure()
        ax = plt.gca()

        for i in range(len(df)):
            if df['P_S'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['P_S'].loc[i], vmin=0, vmax=100, s=35, cmap=cmap, marker= '^')
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap ), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        if show_cpt_id:
            for (id, x, y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Probability of exceedance map for S=' + str(round(S_val, 2)) + ' [cm]', fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'Probability of exceedance map for S='+str(round(S_val,2)) + '.png')

    def plot_LPI_perc_map_points(self, path, perc, show_cpt_id=False, interval=5):

        df = self.calc_LPI_percentiles(perc=perc)

        cmap = plt.cm.get_cmap('jet')
        bounds = np.arange(0, interval*max(math.ceil(df['LPI_perc'].max()/interval), 3)+1, interval)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        fig = plt.figure()
        ax = plt.gca()

        for i in range(len(df)):
            if df['LPI_perc'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['LPI_perc'].loc[i], vmin=0, vmax=bounds.max(), s=35, cmap=cmap, marker= '^')
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        if show_cpt_id:
            for (id,x,y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('LPIish '+str(round(perc,0))+'% percentile', fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'LPIish '+str(round(perc, 0))+'% percentile' + '.png')

    def plot_S_perc_map_points(self, path, perc, show_cpt_id=False, interval=5):

        df = self.calc_S_percentiles(perc=perc)

        cmap = plt.cm.get_cmap('jet')
        bounds = np.arange(0, interval*max(math.ceil(df['S_perc'].max()/interval), 3)+1, interval)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        fig = plt.figure()
        ax = plt.gca()

        for i in range(len(df)):
            if df['S_perc'].loc[i] == 0:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], facecolors='white', edgecolors='black', s=35, marker='^', linewidths=1)
            else:
                ax.scatter(df['X coord'].loc[i], df['Y coord'].loc[i], c=df['S_perc'].loc[i], vmin=0, vmax=bounds.max(), s=35, cmap=cmap, marker= '^')
        cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), spacing='proportional', ticks=bounds, boundaries=bounds, format='%1i')
        if show_cpt_id:
            for (id, x, y) in zip(df['CPT ID'], df['X coord'], df['Y coord']):
                plt.annotate(id, (x, y))
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Settlement '+str(round(perc, 0))+'% percentile', fontsize=16)
        plt.grid()
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        # plt.show()
        plt.savefig(path + '\\' + 'Settlement '+str(round(perc, 0))+'% percentile' + '.png')

    # def plot_map_contour(self, LPI_val):
    #     import matplotlib.tri as tri
    #     df = self.calc_exceed_cpts(LPI_val)
    #     cm = plt.cm.get_cmap('RdYlBu')
    #     fig =plt.figure()
    #     plt.scatter(df['X coord'], df['Y coord'], color='k', marker= '^')
    #     sc = plt.tricontourf(df['X coord'], df['Y coord'], df['P_LPI'], vmin=0, vmax=20, cmap=cm)
    #     cbar = plt.colorbar(sc)
    #     cbar.ax.set_ylabel('LPIish [-]', rotation=270, fontsize=14, labelpad=+20)
    #     plt.xlabel('X coordinate', fontsize=14)
    #     plt.ylabel('Y coordinate', fontsize=14)
    #     plt.title('Probability exceedance map for LPIish='+str(round(LPI_val,2)), fontsize=16)
    #     plt.grid()
    #     # plt.show()


    def plot_map_contour_det(self):
        import matplotlib.tri as tri
        df = self.calc_LPI_det_cpts()
        cm = plt.cm.get_cmap('RdYlBu')
        fig =plt.figure()
        plt.scatter(df['X coord'], df['Y coord'], color='k', marker= '^')
        sc = plt.tricontourf(df['X coord'], df['Y coord'], df['LPI'], vmin=0, vmax=20, cmap=cm)
        cbar = plt.colorbar(sc)
        cbar.ax.set_ylabel('LPIish [-]', rotation=270, fontsize=14, labelpad=+20)
        plt.xlabel('X coordinate', fontsize=14)
        plt.ylabel('Y coordinate', fontsize=14)
        plt.title('Map of deterministic LPIish calculation', fontsize=16)
        plt.grid()
        # plt.show()
