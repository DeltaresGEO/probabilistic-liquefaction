import numpy as np

from Liquefaction_Probabilistic_Model import model
import time as tm

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


# path = '..\\Railway analysis\\Groningen - AllRail - Actual PGA - Screened locations'
# gef_folder = 'CPT gefs'
# input_filename ='Groningen_AllRail_input'
# output_filename ='Groningen_AllRail_output'

path = '..\\Road analysis\\Groningen - AllRoad - Actual PGA - Screened locations'
gef_folder = 'CPT gefs'
input_filename ='Groningen_AllRoad_input'
output_filename ='Groningen_AllRoad_output'

tm_start = tm.time()

model = model(path=path, gef_folder=gef_folder, input_filename=input_filename, LPI_vals=[1, 5, 10, 15],
              S_vals=np.arange(10), n_samples=10_000, per_PGA=False)

model.run_model()

model.write_model_pickle(name=output_filename+'_model')

tm_end = tm.time()

print('Runtime = ' + str(round((tm_end - tm_start) / 60, 0)))

model.export_df(filename=path + '\\' +''+ output_filename)

# model.plot_over_depth(['CPT000000097741_IMBRO_A'])

model.plot_det_LPI_map(path=path+'\\'+'Output maps')

model.plot_det_settlement_map(path=path+'\\'+'Output maps')

model.plot_LPI_perc_map_points(path=path+'\\'+'Output maps', perc=95, show_cpt_id=False)

model.plot_S_perc_map_points(path=path+'\\'+'Output maps', perc=95, show_cpt_id=False)

# model.plot_map_contour_det()

# for LPIish in np.arange(5,21,5):
#     model.plot_LPI_map_points(LPI_val=LPIish, show_cpt_id=False, path=path+'\\'+'Output maps')
#
# for S in np.arange(2,11,2):
#     model.plot_S_map_points(S_val=S, show_cpt_id=False, path=path+'\\'+'Output maps')

# model.plot_map_contour(LPI_val=10)

