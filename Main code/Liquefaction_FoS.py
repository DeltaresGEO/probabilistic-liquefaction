import numpy as np


def Ic_gamma_recursive(qc, fs, pwp, gwl, z, tol=1e-4, Ic_bnd=2.6):

    gamma = np.ones_like(qc) * 19
    gamma_new = np.ones_like(gamma)
    sigma_total, sigma_eff = calc_vertical_stress(z=z, gamma=gamma, pwp=pwp, gwl=gwl)
    Ic = calc_IC(qc=qc, fs=fs, sigma_total=sigma_total, sigma_eff=sigma_eff)

    error = 1.0
    cnt = 0
    while error > tol and cnt <= 1_000:

        # qc *= calc_thin_layer_correction(z=z, Ic=Ic, dc=0.08, Ic_bnd=Ic_bnd)

        gamma_new[np.where(Ic<=Ic_bnd)] = unit_weight_Robertson(qc=qc[np.where(Ic<=Ic_bnd)], fs=fs[np.where(Ic<=Ic_bnd)])
        gamma_new[np.where(Ic>Ic_bnd)] = unit_weight_Lengkeek(qc=qc[np.where(Ic>Ic_bnd)], fs=fs[np.where(Ic>Ic_bnd)])

        sigma_total, sigma_eff = calc_vertical_stress(z=z, gamma=gamma_new, pwp=pwp, gwl=gwl)
        Ic_new = calc_IC(qc=qc, fs=fs, sigma_total=sigma_total, sigma_eff=sigma_eff)

        # error = np.linalg.norm(gamma_new - gamma) / len(gamma_new)
        # gamma=gamma_new.copy()
        error = np.linalg.norm(Ic_new - Ic) / len(Ic_new)
        Ic=Ic_new.copy()
        cnt += 1

    return Ic, sigma_total, sigma_eff

def calc_thin_layer_correction(z, Ic, dc=np.sqrt(0.0010/np.pi)*2, Ic_bnd=2.6, type='Greef'):
    dz = np.append(z[1] - z[0], z[1:] - z[:-1])
    soil_type_grid = np.where(Ic <= Ic_bnd, 'sand', 'not sand')
    if type == 'Youd':
        H = np.zeros_like(z)
        for i, soil_type in enumerate(soil_type_grid):
            for j in range(i, -1, -1):
                if soil_type_grid[j] == soil_type:
                    H[i] += dz[j]
            for j in range(i, -1, -1):
                if soil_type_grid[j] == soil_type:
                    H[j] = H[i]
                else:
                    break
        KH = np.where((Ic<=2.6) & (H<=1), 0.25 * ((H / dc) /17 - 1.77)**2 + 1, 1)

    elif type == 'Greef':
        d_from_interface = np.zeros_like(z)
        for i, (z_point, soil_type) in enumerate(zip(z,soil_type_grid)):
            idx_ceiling = 0
            idx_floor = len(z)-1
            for j in range(i):
                if soil_type_grid[j] != soil_type:
                    idx_ceiling = j
            for j in range(len(z)-1, i, -1):
                if soil_type_grid[j] != soil_type:
                    idx_floor = j
            if idx_ceiling == 0 and idx_floor != len(z)-1:
                d_from_interface[i] = z[idx_floor] - z_point
            elif idx_ceiling != 0 and idx_floor == len(z)-1:
                d_from_interface[i] = z_point - z[idx_ceiling]
            elif idx_ceiling == 0 and idx_floor == len(z)-1:
                d_from_interface[i] = 9999
            else:
                d_from_interface[i] = min(z_point - z[idx_ceiling], z[idx_floor] - z_point)
        KH = np.where((Ic<=2.6) & (2*d_from_interface<=1), 0.25 * ((2 * d_from_interface / dc) /17 - 1.77)**2 + 1, 1)

    # import matplotlib.pyplot as plt
    # plt.plot(Ic)
    # plt.plot(d_from_interface)
    # plt.axhline(2.6)

    return KH


def calc_IC(qc, fs, sigma_total, sigma_eff, Pa=101.3):

    qc_kPa = qc.copy() * 1_000

    Q050 = np.abs(((qc_kPa - sigma_total) / Pa)) * (Pa / sigma_eff)**0.50
    Q075 = np.abs(((qc_kPa - sigma_total) / Pa))* (Pa / sigma_eff)**0.75
    Q100 = np.abs(((qc_kPa - sigma_total) / Pa)) * (Pa / sigma_eff)**1.00

    F = np.abs((fs / (qc_kPa - sigma_total))) * 100

    Ic050 = ((3.47 - np.log10(Q050))**2 + (1.22+np.log10(F))**2)**0.50
    Ic075 = ((3.47 - np.log10(Q075))**2 + (1.22+np.log10(F))**2)**0.50
    Ic100 = ((3.47 - np.log10(Q100))**2 + (1.22+np.log10(F))**2)**0.50

    Ic = np.zeros_like(Ic100)
    # for i,_ in enumerate(Ic):
    #     if Ic100[i] >= 2.6:
    #         Ic[i] = Ic100[i]
    #     else:
    #         if Ic050[i] <= 2.6:
    #             Ic[i] = Ic050[i]
    #         else:
    #             Ic[i] = Ic075[i]

    Ic[np.where(Ic100>=2.6)] = Ic100[np.where(Ic100>=2.6)]
    Ic[np.where((Ic100<2.6) & (Ic050 <=2.6))] = Ic050[np.where((Ic100<2.6) & (Ic050 <=2.6))]
    Ic[np.where((Ic100<2.6) & (Ic050 > 2.6))] = Ic075[np.where((Ic100<2.6) & (Ic050 > 2.6))]

    # Ic[np.where(qc==0)] = 5

    # Ic = np.ones_like(qc)
    # for i, (qc_point, fs_point, sigma_total_point, sigma_eff_point) in enumerate(zip(qc, fs, sigma_total, sigma_eff)):
    #     error = 1.0
    #     sigma_eff_point = abs(sigma_eff_point)
    #     Fr = fs_point * 100 / (qc_point * 1000 - sigma_total_point)
    #     n = 1.0
    #     CN_point = min((Pa / sigma_eff_point) ** n, 1.7)
    #     if qc_point == 0:
    #         Ic[i] = 5
    #     else:
    #         while error > 1e-4:
    #             Qtn = ((qc_point * 1000) / Pa) * CN_point
    #             Ic_point = ((3.47 - np.log10(Qtn))**2 + (np.log10(Fr) + 1.22)**2)**0.5
    #             n = 0.381 * Ic_point + 0.05 * sigma_eff_point / Pa - 0.15
    #             CN_point_new = min((Pa / sigma_eff_point) ** n, 1.7)
    #             error = np.abs(CN_point_new - CN_point)
    #             CN_point = CN_point_new
    #         Ic[i] = Ic_point

    return Ic

def unit_weight_Lengkeek(qc, fs):
     gamma = 19.0 - 4.12 * np.log10(5000.0 / np.array(qc * 1000)) / np.log10(30.0 / fs)
     gamma[np.isnan(gamma)] = 19.0
     gamma[gamma<=0] = 19.0
     first_non_zero = gamma[np.where(gamma>0)[0][0]]
     gamma = np.where(gamma<=10.5, first_non_zero, gamma)
     gamma[np.abs(gamma) >= 22] = 22
     return gamma

def unit_weight_Robertson(qc, fs, Pa=101.3):
    Rf = fs / (qc * 1000) *100
    gamma_over_gammawater = 0.27 * np.log10(Rf) + 0.36 * np.log10(qc * 1000/Pa) + 1.236
    gamma = gamma_over_gammawater * 9.81
    return gamma

def calc_vertical_stress(z, gamma, pwp, gwl):
    dz = np.append(z[1] - z[0], z[1:] - z[:-1])
    if z[0]>gwl:
        total_stress = z[0] * gamma[0] + np.cumsum(gamma * dz) + (z[0]-gwl) * 9.81
    else:
        total_stress = z[0] * gamma[0] + np.cumsum(gamma * dz)
    eff_stress = np.maximum(total_stress - pwp, 0)
    return total_stress, eff_stress

def Idriss_Boul_calc_qc1n(qc, sigma_eff, Ic, Pa=100, tol=1e-4, FC_bnd=100):
    CN_array = np.ones_like(qc)
    for i, (qc_point, sigma_eff_point, IC_point) in enumerate(zip(qc, sigma_eff, Ic)):
        if qc_point == 0:
            CN_array[i] = 0
        else:
            m = 1.0
            error = 1.0
            CN_point = min((Pa / sigma_eff_point) ** m, 1.7)
            cnt = 0
            while error > tol and cnt<=1_000:
                qc1n_point = CN_point * qc_point * 1_000 / Pa
                qc1ncs_point,_ = fine_correction(qc1n=qc1n_point, Ic=IC_point, FC_bnd=FC_bnd)
                m = 1.338 - 0.249 * qc1ncs_point**0.264
                CN_point_new = min((Pa / sigma_eff_point)**m, 1.7)
                error = np.abs(CN_point_new - CN_point)
                CN_point = CN_point_new
                cnt += 1
            CN_array[i] = CN_point
    qc1n = CN_array * qc * 1_000 / Pa
    return qc1n # qc1n is dimensionless

def fine_correction(qc1n, Ic, Cfc=0, FC_bnd=100):
    qc1ncs_aux = qc1n.copy()
    FC = 80 * (Ic + Cfc) - 137
    FC = np.minimum(FC, FC_bnd)
    FC = np.maximum(FC, 0)

    dq = (11.9 + np.array(qc1n) / 14.6) * np.exp(1.63 - 9.7 / (FC + 2.) - (15.7 / (FC + 2.)) ** 2)

    qc1ncs_aux += dq
    qc1ncs = qc1ncs_aux
    qc1ncs = np.maximum(qc1ncs, 0)

    return qc1ncs, FC

def calc_CRR(qc1ncs, Co=2.8118706, CRR75_lim=0.6):
    CRR = np.exp((qc1ncs / 113) + (qc1ncs / 1_000)**2 - (qc1ncs / 140)**3 + (qc1ncs / 137)**4 - Co)
    return np.minimum(CRR, CRR75_lim)

def kappa_sigma(qc1n, sigma_eff, Pa=100):
    # qc1n = qc1ncs
    C_sigma =  np.minimum(1 / (37.3 - 8.27 * qc1n ** 0.264), np.ones_like(sigma_eff) * 0.3)
    C_sigma[np.where(qc1n>211)] = 0.3
    K_sigma = np.minimum(1 - C_sigma * np.log(sigma_eff / Pa), np.ones_like(sigma_eff) * 1.1)
    return K_sigma, C_sigma

def calc_CSR(a_max, sigma_total, sigma_eff, rd, g=9.81):
    return 0.65 * a_max / g * sigma_total / sigma_eff * rd

def calc_CSR_star(CSR, MSF, K_sigma, K_alpha = 1):
    return CSR / (MSF * K_sigma * K_alpha)

def calc_FoS(qc, fs, a_max, pwp, gwl, z, rd, MSF, Cfc, Co, K_alpha=1, FC_bnd=100):
    # Ic = calc_IC(qc=qc, fs=fs, sigma_total=sigma_total, sigma_eff=sigma_eff)
    Ic, sigma_total, sigma_eff = Ic_gamma_recursive(qc=qc, fs=fs, pwp=pwp, gwl=gwl, z=z)
    qc1n = Idriss_Boul_calc_qc1n(qc=qc, sigma_eff=sigma_eff, Ic=Ic, FC_bnd=FC_bnd)
    qc1ncs, FC = fine_correction(qc1n=qc1n, Ic=Ic, Cfc=Cfc, FC_bnd=FC_bnd)
    CRR = calc_CRR(qc1ncs=qc1ncs, Co=Co)
    CSR = calc_CSR(a_max=a_max, sigma_total=sigma_total, sigma_eff=sigma_eff, rd=rd)
    K_sigma, _ = kappa_sigma(qc1n=qc1ncs, sigma_eff=sigma_eff, )
    CSR_star = calc_CSR_star(CSR=CSR, MSF=MSF, K_sigma=K_sigma, K_alpha=K_alpha)
    FoS = np.where(np.all(np.c_[Ic<=2.6, z>gwl],axis=1), CRR / CSR_star, 9999)

    return FoS, Ic, FC, qc1ncs

def calc_LPI(FoS, z, z_max=20):
    if np.any(FoS < 1):
        sort_idx = np.argsort(z)
        z = z[sort_idx]
        z_max = min(z.max(),z_max)
        FoS = FoS[sort_idx]
        liq_idx = np.where(FoS < 1.0)[0]
        liq_idx = np.sort(liq_idx)
        H1 = z[liq_idx[0]]
        dz = np.append(z[1] - z[0], z[1:] - z[:-1])
        m_FoS = np.where(FoS <= 0.95, np.exp(5 / (25.56 * (1 - FoS))) - 1, 100)
        F_LPI_ish = np.where(np.all(np.c_[(FoS[:, None] <= 1.0), (H1 * m_FoS[:, None] <= 3)], axis=1), 1 - FoS, 0)
        LPI_ish = np.sum(np.where(np.all(np.c_[(z >= H1), (z <= z_max),(FoS<9999)], axis=1), F_LPI_ish * 25.56 / z * dz, 0))

        # sort_idx = np.argsort(z)
        # z = z[sort_idx]
        # FoS = FoS[sort_idx]
        # z_max = min(z.max(), 20)
        # dz = z[1:] - z[:-1]
        # dz = np.append(dz, dz[-1])
        # Fz = np.where(FoS<1.0, 1 - FoS, 0)
        # Wz = np.where(z < z_max, 10 - 0.5*z, 0)
        # LPI = np.sum(Fz * Wz * dz)

    else:
        LPI_ish = 0

    return LPI_ish




#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

def Robertson_stress(z, qc, fs, gwl, pwp):   # FK "Form1.cs" line 764
    gamma_dict = {'sand_wet' : 20,
                  'sand_dry' : 17,
                  'clay' : 16,
                  'peat' : 11,
                  'water' : 10,
                  'topsoil' : 15}

    Rf = fs / (qc * 1000) *100
    dz = np.append(z[1] - z[0], z[1:] - z[:-1])
    gamma = np.zeros_like(z)

    # Added June 2020 to calculate first sigma's using Topsoil_weight following Piet Meijers
    # idx = np.where(z<=gwl)[0]
    # total_stress[idx] = dz[idx] * gamma['topsoil']

    idx = np.where((z <= gwl) & (Rf < 1.5))[0]  # Begemann rule of thumb, sand
    gamma[idx] = gamma_dict['sand_dry']

    idx = np.where((z>gwl) & (Rf<1.5))[0]               # Begemann rule of thumb, sand
    gamma[idx] = gamma_dict['sand_wet']

    idx = np.where((Rf>=1.5) & (Rf<4))[0]     # Begemann rule of thumb, clay
    gamma[idx] = gamma_dict['clay']

    idx = np.where((Rf>=4))[0]                # Begemann rule of thumb, peat
    gamma[idx] = gamma_dict['peat']

    if z[0]>gwl:
        sigma_total = z[0] * gamma[0] + np.cumsum(gamma * dz) + (z[0]-gwl) * 9.81
    else:
        sigma_total = z[0] * gamma[0] + np.cumsum(gamma * dz)
    sigma_eff = sigma_total - pwp

    Ic = Robertson_Ic_alternative(qc=qc, fs=fs, sigma_total=sigma_total, sigma_eff=sigma_eff)

    gamma_Ic_based = np.zeros_like(z)

    idx = np.where((z <= gwl) & (Ic<=2.6))[0]  # Ic Robertson alternative, sand wet
    gamma_Ic_based[idx] = gamma_dict['sand_dry']

    idx = np.where((z > gwl) & (Ic<=2.6))[0]  # Ic Robertson alternative, sand dry
    gamma_Ic_based[idx] = gamma_dict['sand_wet']

    idx = np.where((Ic>2.6) & (Ic<=3.6))[0]  # Ic Robertson alternative, clay
    gamma_Ic_based[idx] = gamma_dict['clay']

    idx = np.where(Ic>3.6)[0]  # Ic Robertson alternative, peat
    gamma_Ic_based[idx] = gamma_dict['peat']

    if z[0]>gwl:
        sigma_total_Ic_based = z[0] * gamma_Ic_based[0] + np.cumsum(gamma_Ic_based * dz) + (z[0]-gwl) * 9.81
    else:
        sigma_total_Ic_based = z[0] * gamma_Ic_based[0] + np.cumsum(gamma_Ic_based * dz)
    sigma_eff_Ic_based = sigma_total_Ic_based - pwp

    return Ic, sigma_total_Ic_based, sigma_eff_Ic_based

def Robertson_Ic_alternative(qc, fs, sigma_total, sigma_eff, Pa=101.3): # FK "Form1.cs" line 1343

    qc_kPa = qc.copy() * 1_000

    Q050 = np.abs(((qc_kPa - sigma_total) / Pa)) * (Pa / sigma_eff)**0.50
    Q075 = np.abs(((qc_kPa - sigma_total) / Pa))* (Pa / sigma_eff)**0.75
    Q100 = np.abs(((qc_kPa - sigma_total) / Pa)) * (Pa / sigma_eff)**1.00

    F = np.abs((fs / (qc_kPa - sigma_total))) * 100

    Ic050 = ((3.47 - np.log10(Q050))**2 + (1.22+np.log10(F))**2)**0.50
    Ic075 = ((3.47 - np.log10(Q075))**2 + (1.22+np.log10(F))**2)**0.50
    Ic100 = ((3.47 - np.log10(Q100))**2 + (1.22+np.log10(F))**2)**0.50

    Ic = np.zeros_like(Ic100)
    Ic = np.where(Ic100>=2.6, Ic100, Ic)
    Ic = np.where((Ic100<2.6) & (Ic050 <=2.6), Ic050, Ic)
    Ic = np.where((Ic100<2.6) & (Ic050 > 2.6), Ic075, Ic)

    return Ic

def calc_Fines(Ic, Cfc, fines_type='IB2014', FC_bnd=100):
    if fines_type=='Robertson': # Robertson Fines calculation method
        FC = 1.75 * Ic ** 3.25 - 3.7
        FC = np.where(Ic < 1.26, 0, FC)
        FC = np.where(Ic > 3.5, 100, FC)
    elif fines_type=='Deltares': # Deltares_Nam method, according to Piet Meijers
        FC = 2.8 * Ic ** 2.6
        FC = np.where(Ic < 1.26, 0, FC)
        FC = np.where(Ic > 3.5, 100, FC)
    elif fines_type=='IB2014': # IB2014 method, according to Piet Meijers
        FC = np.maximum(80 * (Ic + Cfc) - 137, 0)
    elif fines_type=='NPR_9998_2020': # NPR_9998_2020 method
        FC = np.where(Ic<2.05, 0, 20)
    FC = np.minimum(FC, FC_bnd)
    return FC

def calc_MSF_Gron(M, PGA, Vs12): # FK "Form1.cs" line 1241
    alpha1 = -0.2487
    alpha2 = -0.3131
    alpha3 = -0.0984
    alpha4 = 0.1766
    alpha5 = 0.0038
    if PGA<0.3:
        Neqm = np.exp(alpha1 + alpha2 * np.log(PGA) + alpha4 * M + alpha5 * Vs12)
    else:
        Neqm = np.exp(alpha1 + alpha2 * np.log(PGA) + alpha3 * np.log(PGA / 0.3) + alpha4 * M + alpha5 * Vs12)
    MSF = (7.25 / Neqm) ** 0.34
    MSF = min(MSF, 2.04)
    return MSF

def calc_FoS_KF(z, qc, fs,  gwl, pwp, a_max, rd, MSF, Cfc, Co=2.63, Pa=101.3 ,
                K_alpha=1, FC_bnd=100, pleistocene_corr=1.3, fines_type='IB2014'): # FK "Form1.cs" line 1022


    # error = 9999
    # cnt = 0
    # while (error > 10e-2) and (cnt <= 100):
    #     Ic, sigma_total_Ic_based, sigma_eff_Ic_based = Robertson_stress(z=z, qc=qc, fs=fs, gwl=gwl, pwp=pwp)
    #     KH = calc_thin_layer_correction(z=z, Ic=Ic, dc=0.08, Ic_bnd=2.6)
    #     qc_new = qc * KH
    #     error = np.linalg.norm(qc_new - qc) / len(qc_new)
    #     cnt += 1
    #     qc = qc_new.copy()

    Ic, sigma_total_Ic_based, sigma_eff_Ic_based = Robertson_stress(z=z, qc=qc, fs=fs, gwl=gwl, pwp=pwp)

    # qc *= calc_thin_layer_correction(z=z, Ic=Ic, dc=0.08, Ic_bnd=2.6)

    FC = calc_Fines(Ic=Ic, Cfc=Cfc, fines_type=fines_type, FC_bnd=FC_bnd)

    PGA = a_max/9.81

    CSR = 0.65 * PGA * (sigma_total_Ic_based / sigma_eff_Ic_based) * rd

    m = 0.5
    Cn = np.minimum((Pa / sigma_eff_Ic_based)**m, 1.7)
    qc1 = Cn * qc * 1_000
    qc1n = qc1 / Pa

    # Correct version February 2019
    qc1n_corr = (11.9 + qc1n / 14.6) * np.exp(1.63 - (9.7 / (FC + 2)) - (15.7 / (FC + 2))**2)
    qc1ncs = np.minimum(qc1n + qc1n_corr, 211)

    CRR = np.exp((qc1ncs / 113) + (qc1ncs / 1_000)**2 - (qc1ncs / 140)**3 + (qc1ncs / 137)**4 - Co)

    CRR *= pleistocene_corr

    C_sigma = np.minimum(1 / (37.3 - 8.27 * qc1ncs**0.264), 0.3)

    # Relax cap from 1.1 --> 1.5 ???
    K_sigma = np.maximum(np.minimum(1 - C_sigma * np.log(sigma_eff_Ic_based / Pa), 1.1), 0.1)

    CSR_star = calc_CSR_star(CSR=CSR, MSF=MSF, K_sigma=K_sigma, K_alpha=K_alpha)

    FoS = np.where(np.all(np.c_[Ic <= 2.6, z > gwl], axis=1), CRR / CSR_star, 9999)
    FoS = np.where(np.isinf(FoS), 9999, FoS)
    FoS = np.where(np.isnan(FoS), 9999, FoS)
    FoS = np.where(CRR > 0.60, 9999, FoS)

    return FoS, Ic, FC, qc1ncs, CRR

def calc_LPI_KF(z, FoS, Ic, Icbnd=2.6, z_max=20): # FK "Form1.cs" line 527
    if np.any(FoS < 1):

        liq_idx = np.where(FoS < 1.0)[0]
        H1 = min(z[liq_idx])

        z_max = min(z.max(),z_max)

        dz = np.append(z[1] - z[0], z[1:] - z[:-1])

        F_LPI = np.maximum(1 - FoS, 0)
        Wz = 10 -0.5 * z
        LPI_inc = F_LPI * Wz * dz
        LPI_inc = np.where(np.all(np.c_[(z>0), (z<z_max)], axis=1), LPI_inc, 0)
        LPI_inc = np.where(Ic<=Icbnd, LPI_inc, 0)
        LPI = LPI_inc.sum()


        m_FoS = np.where(FoS <= 0.95, np.exp(5 / (25.56 * (1 - FoS))) - 1, 100)
        F_LPI_ish = np.where(np.all(np.c_[(FoS[:, None] <= 1.0), (H1 * m_FoS[:, None] <= 3)], axis=1), 1 - FoS, 0)
        LPI_ish_inc = np.where(np.all(np.c_[(z >= H1), (z <= z_max),(FoS<9999)], axis=1), F_LPI_ish * 25.56 / z * dz, 0)
        LPI_ish_inc = np.where(Ic<=Icbnd, LPI_ish_inc, 0)
        LPIish = LPI_ish_inc.sum()

    else:
        LPI = 0
        LPIish = 0

    return LPI, LPIish


#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

def calc_epsvol_Zhang(FoS, q):
    a_factors = [0.3773, -0.0337, 1.5672, -0.1833]
    b_factor = [28.45, -9.3372, 0.7975]
    eps_vol = np.zeros_like(FoS)
    for i, (FoS_point, q_point) in enumerate(zip(FoS,q)):
        if FoS_point >= 2:
            eps_vol[i] = 0
        else:
            if FoS_point <= 2 - 1/(a_factors[2] + a_factors[3] * np.log(q_point)):
                eps_vol[i] = b_factor[0] + b_factor[1] * np.log(q_point) + b_factor[2] * np.log(q_point)**2
            else:
                eps_vol[i] = min((a_factors[0] + a_factors[1] * np.log(q_point))/(1/(2-FoS_point) - (a_factors[2] + a_factors[3] * np.log(q_point))),
                             b_factor[0] + b_factor[1] * np.log(q_point) + b_factor[2] * np.log(q_point)**2)

    return eps_vol/100


def calc_epsvol_NPR(FoS, qc1ncs):
    # calculation based on NPR - 9998 - 2020

    Dr = (0.478 * np.array(qc1ncs) ** 0.264 - 1.063) * 100

    F_ult = np.where(Dr<39.23, 0.9524, -0.0006 * Dr ** 2 + 0.047 * Dr + 0.032)

    gamma_max = np.zeros_like(FoS)

    for i, (FoS_point, F_ult_point) in enumerate(zip(FoS, F_ult)):
        if FoS_point >= 2:
            gamma_max[i] = 0.0
        else:
            if F_ult_point < FoS_point:
                gamma_max[i] = 3.5 * (2 - FoS_point) * (1 - F_ult_point) / (FoS_point - F_ult_point)
            else:
                gamma_max[i] = 1.0e15

    eps_vol = np.where(gamma_max<8, 1.5 * gamma_max * np.exp(-0.025 * Dr), 12 * np.exp(-0.025 * Dr))

    return eps_vol/100

def calc_settlement(FoS, qc1ncs, z, Ic, Icbnd=2.6):
    # eps_vol = calc_epsvol_Zhang(FoS=FoS, q=qc1ncs)
    eps_vol = calc_epsvol_NPR(FoS=FoS, qc1ncs=qc1ncs)
    dz = np.append(z[1] - z[0], z[1:] - z[:-1])
    IND = np.where(Ic<=Icbnd, 1, 0)
    return np.sum(eps_vol * dz * IND)
