import numpy as np
from scipy.stats import norm, lognorm
import pandas as pd
import emcee as mc
import seaborn as sns
import matplotlib.pyplot as plt
import Liquefaction_FoS as L_FoS
from Liquefaction_Probabilistic_Model import liq_sampler

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def FoS_model(df, rd, MSF, Cfc, Co):
    FoS, _, _, _ = L_FoS.calc_FoS_KF(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                                                  pwp=df['pwp'].to_numpy(), gwl=df['GW'].iloc[0],
                                                  a_max=df['amax_g'].iloc[0]*9.81,
                                                  rd=rd, MSF=MSF, Cfc=Cfc, Co=Co, fines_type='NPR_9998_2020')
    return FoS

def settlement_model(df, rd, MSF, Cfc, Co):
    FoS, Ic, FC, qc1ncs = L_FoS.calc_FoS_KF(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                                                  pwp=df['pwp'].to_numpy(), gwl=df['GW'].iloc[0],
                                                  a_max=df['amax_g'].iloc[0]*9.81,
                                                  rd=rd, MSF=MSF, Cfc=Cfc, Co=Co, fines_type='NPR_9998_2020')
    S = L_FoS.calc_settlement(FoS=FoS, qc1ncs=qc1ncs, z=df['Depth'].to_numpy(), Ic=Ic) * 100
    return S

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

filename = 'model_validation_data_ku2004'

df = pd.read_csv('D:\\Probabilistic_Liquefaction\\Model validation data\\'+filename+'.csv', index_col=False)



dummy_sampler = liq_sampler(z=df['Depth'].to_numpy(), qc=df['qc'].to_numpy(), fs=df['fs'].to_numpy(),
                            a_max_g=df['amax_g'].iloc[0], M=df['Mw'].iloc[0], Vs_12=140, pwp=df['pwp'].to_numpy(),
                            gwl=df['GW'].iloc[0], zone=0, n_samples=1_000)

dummy_sampler.initialize_variables()

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

f_model = lambda x: settlement_model(df=df, rd=dummy_sampler.rd_mu+dummy_sampler.rd_sd*x[0], MSF=x[1], Co=x[2], Cfc=0)

def log_prior(theta):
    # log_prior_rd = norm.logpdf(rd, dummy_sampler.rd_mu, dummy_sampler.rd_sd)
    log_prior_rd = norm.logpdf(theta[0], 0, 1)
    log_prior_MSF = lognorm.logpdf(theta[1], s=dummy_sampler.ln_MSF_sd, loc=dummy_sampler.ln_MSF_mu, scale=np.exp(dummy_sampler.ln_MSF_sd))
    log_prior_Co =  norm.logpdf(theta[2], dummy_sampler.Co_mu, dummy_sampler.Co_sd)
    return log_prior_rd + log_prior_MSF + log_prior_Co

def log_likelihood(theta, y_obs):
    y_model = f_model(theta)
    log_like = norm.logpdf(y_obs, loc=y_model, scale=2)
    return log_like

def log_post(theta, y_obs):
    lp = log_prior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood(theta, y_obs)




# y_obs = df['S_obs_mean'].iloc[0]
y_obs = f_model(np.array([-1,2, 3.21]))


pos = np.array([0, np.exp(dummy_sampler.ln_MSF_mu), dummy_sampler.Co_mu])

n_walkers,n_dim = 10,3
pos = pos + np.random.randn(n_walkers, n_dim) * 1e-2

sampler = mc.EnsembleSampler(n_walkers, n_dim, log_post, args=[y_obs])
pos, prob, state = sampler.run_mcmc(pos, 10_000, progress=True)

chains = sampler.chain

trace = pd.DataFrame(data=np.c_[chains.reshape(-1, 1, chains.shape[2], order='F').squeeze(),
                                np.tile(np.arange(chains.shape[1]), (chains.shape[0],1)).reshape(-1,1),
                                np.tile(np.arange(1, chains.shape[0] + 1).reshape(-1, 1), (1, chains.shape[1])).reshape(-1, 1)],
                     columns=['rd_offset', 'MSF', 'Co', 'sample_num','chain'])
sns.lineplot(data=trace, x='sample_num', y='Co', hue='chain')
sns.lineplot(data=trace, x='sample_num', y='MSF', hue='chain')
sns.lineplot(data=trace, x='sample_num', y='rd_offset', hue='chain')