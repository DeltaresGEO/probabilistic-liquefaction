import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import Liquefaction_FoS as LFoS

from os import listdir
from os.path import isfile, join
from pathlib import Path
from geolib_plus.gef_cpt import GefCpt
from geolib_plus.robertson_cpt_interpretation import RobertsonCptInterpretation
from geolib_plus.robertson_cpt_interpretation import UnitWeightMethod
from geolib_plus.robertson_cpt_interpretation import OCRMethod
from geolib_plus.robertson_cpt_interpretation import ShearWaveVelocityMethod

#-----------------------------------------------------------------------------------------------------------------------

def clean_zero_depth(self, z):
    idx_zero = np.where(z == 0)[0]
    return np.delete(np.arange(z.shape[0]), idx_zero)

def get_elevation(cpt_filename):
        cpt_file_gef = Path(cpt_filename)
        cpt_gef = GefCpt()
        cpt_gef.read(cpt_file_gef)
        return cpt_gef.local_reference_level


def calc_CRR(model):
    Pa = 101.3
    K_alpha = 1
    FC_bnd = 100
    pleistocene_corr = 1.3
    fines_type = 'IB2014'
    rd = model['MC'].rd_mu
    MSF = model['MC'].MSF_mu
    Co = 2.812
    Cfc = np.zeros(len(model['MC'].z))

    Ic, sigma_total_Ic_based, sigma_eff_Ic_based = LFoS.Robertson_stress(z=model['MC'].z, qc=model['MC'].qc,
                                                                         fs=model['MC'].fs, gwl=model['MC'].gwl,
                                                                         pwp=model['MC'].pwp)

    FC = LFoS.calc_Fines(Ic=Ic, Cfc=Cfc, fines_type=fines_type, FC_bnd=FC_bnd)

    m = 0.5
    Cn = np.minimum((Pa / sigma_eff_Ic_based) ** m, 1.7)
    qc1 = Cn * model['MC'].qc * 1_000
    qc1n = qc1 / Pa
    qc1n_corr = (11.9 + qc1n / 14.6) * np.exp(1.63 - (9.7 / (FC + 2)) - (15.7 / (FC + 2)) ** 2)
    qc1ncs = np.minimum(qc1n + qc1n_corr, 211)

    CRR = np.exp((qc1ncs / 113) + (qc1ncs / 1_000) ** 2 - (qc1ncs / 140) ** 3 + (qc1ncs / 137) ** 4 - Co)

    CRR *= pleistocene_corr

    return CRR, qc1ncs

#-----------------------------------------------------------------------------------------------------------------------

# path = 'D:\\Probabilistic_Liquefaction\\Railway analysis\\Groningen - AllRail'
# file = 'Groningen_AllRail_output_PGA_'

# path = '..\\Railway analysis\\Groningen - AllRail - Actual PGA'
# file = 'Groningen_AllRail_output'
path = '..\\Road analysis\\Groningen - AllRoad - Actual PGA'
file = 'Groningen_AllRoad_output'

df = pd.read_csv(path+'\\'+file+'.csv')
df['PGA'] = 0.10

# for pga in [0.15, 0.20, 0.25, 0.30]:
#     df_dummy = pd.read_csv(path+'\\'+file+str(pga)+'.csv')
#     df_dummy['PGA'] = pga
#     df = df.append(df_dummy)


gef_folder = 'CPT gefs'
cpt_files = [f for f in listdir(path + '\\' + gef_folder) if isfile(join(path + '\\' + gef_folder, f))]
cpt_names = [cpt_file.split('.')[0] for cpt_file in cpt_files]
elevation_dict = {}
for (cpt_file, cpt_name) in zip(cpt_files, cpt_names):
    elevation_dict[cpt_name] = get_elevation(cpt_filename=path + '\\' + gef_folder + '\\' + cpt_file)

#-----------------------------------------------------------------------------------------------------------------------

LPI_cols = ['LPI_'+str(i) for i in [5, 50, 95]]
S_cols = ['S_'+str(i) for i in [5, 50, 95]]
fig, axs = plt.subplots(1, 3)
for (LPI, S, ax) in zip(LPI_cols, S_cols, axs.flatten()):
    sns.scatterplot(data=df[[LPI, S, 'PGA']], x=LPI, y=S, hue='PGA', palette='colorblind', ax=ax, alpha=0.3)
    ax.set_xlabel('LPI [-]', fontsize=12)
    ax.axes.get_yaxis().set_visible(False)
    ax.set_title('Percentile of '+LPI.split('_')[1])
    ax.set_ylim([0, 125])
    ax.set_xlim([0, 25])
axs[0].axes.get_yaxis().set_visible(True)
axs[0].set_ylabel('Settlement [cm]', fontsize=12)


# pgas = pd.unique(df['PGA'])
# LPI_cols = ['LPI_'+str(i) for i in [5, 50, 95]]
# S_cols = ['S_'+str(i) for i in [5, 50, 95]]
# fig, axs = plt.subplots(3, len(pgas))
# for i, pga in enumerate(pgas):
#     for j, (LPI, S) in enumerate(zip(LPI_cols, S_cols)):
#         axs[j, i].axes.get_xaxis().set_visible(False)
#         axs[j, i].axes.get_yaxis().set_visible(False)
#         sns.scatterplot(data=df[df['PGA']==pga][[LPI, S]], x=LPI, y=S, palette='colorblind', ax=axs[j, i])
#         if j == 2:
#             axs[j, i].axes.get_yaxis().set_visible(True)
#             axs[j, i].set_xlabel('LPI [-]', fontsize=12)
#         if i == 0:
#             axs[j, i].set_title('PGA=' + str(round(pga, 2)))
#             axs[j, i].axes.get_yaxis().set_visible(True)
#             axs[j, i].set_ylabel('Settlement [cm]', fontsize=12)
#
#
# pgas = pd.unique(df['PGA'])
# LPI_cols = ['LPI_'+str(i) for i in [5, 50, 95]]
# S_cols = ['S_'+str(i) for i in [5, 50, 95]]
# fig, axs = plt.subplots(1, len(pgas))
# for i, pga in enumerate(pgas):
#     # df_dummy = df[df['PGA']==pga][LPI_cols+S_cols]
#     hue = np.array([np.repeat(int(col.split('_')[1]), len(df_dummy)) for col in LPI_cols]).flatten()
#     df_new = pd.DataFrame(data=[], columns=['LPI', 'S', 'Percentile'])
#     for j in [95, 50, 5]:
#         df_dummy = pd.DataFrame(data=np.c_[df[df['PGA']==pga][['LPI_'+str(j), 'S_'+str(j)]], np.repeat(j, len(df[df['PGA']==pga]))],
#                                 columns=['LPI', 'S', 'Percentile'])
#         df_new = df_new.append(df_dummy)
#     # df_new = pd.DataFrame(data=np.c_[df_dummy[LPI_cols].values.flatten(), df_dummy[S_cols].values.flatten(), hue],
#     #                       columns=['LPI', 'S', 'Percentile'])
#     axs[i].axes.get_yaxis().set_visible(False)
#     sns.scatterplot(data=df_new, hue='Percentile', x='LPI', y='S', palette='colorblind', ax=axs[i], alpha=0.3)
#     axs[i].set_xlabel('LPI [-]', fontsize=12)
#     axs[i].set_title('PGA=' + str(round(pga, 2)))
#     axs[i].set_ylim([0, 125])
#     axs[i].set_xlim([0, 25])
#     if i!=0:
#         axs[i].get_legend().remove()
# axs[0].axes.get_yaxis().set_visible(True)
# axs[0].set_ylabel('Settlement [cm]', fontsize=12)
#
#
# pgas = pd.unique(df['PGA'])
# LPI_cols = ['LPI_'+str(i) for i in [95]]
# S_cols = ['S_'+str(i) for i in [95]]
# fig, axs = plt.subplots(1, 1)
# pga = 0.3
# hue = np.array([np.repeat(int(col.split('_')[1]), len(df_dummy)) for col in LPI_cols]).flatten()
# df_new = pd.DataFrame(data=[], columns=['CPT ID', 'LPI', 'S', 'Percentile'])
# for j in [95]:
#     df_dummy = pd.DataFrame(data=np.c_[df[df['PGA']==pga][['CPT ID']+['LPI_'+str(j), 'S_'+str(j)]], np.repeat(j, len(df[df['PGA']==pga]))],
#                             columns=['CPT ID', 'LPI', 'S', 'Percentile'])
#     df_new = df_new.append(df_dummy)
# # sns.scatterplot(data=df_new, hue='Percentile', x='LPI', y='S', palette='colorblind', ax=axs, label='${95}^{th}$ quantile\n per location')
# axs.scatter(df_new['LPI'].values, df_new['S'].values, label='${95}^{th}$ quantile\n per location')
# axs.axvline(np.quantile(df_new['LPI'], 0.5), color='r')
# axs.axhline(np.quantile(df_new['S'], 0.5), color='r', label='Median of ${95}^{th}$ quantile')
# axs.set_xlabel('LPI [-]', fontsize=12)
# axs.set_title('PGA=' + str(round(pga, 2)))
# axs.set_ylim([0, 125])
# axs.set_xlim([0, 25])
# axs.set_ylabel('Settlement [cm]', fontsize=12)
# axs.legend(fontsize=14)
#
# A = df_new.sort_values(by=['S'])
# df_new['S'] = pd.to_numeric(df_new['S'], downcast='float')

#-----------------------------------------------------------------------------------------------------------------------

# from scipy import interpolate
# df_amax = pd.read_csv('D:\\Probabilistic_Liquefaction\\Data\\amax_data.csv', index_col=False)
# f = interpolate.interp2d(df_amax['X'].to_numpy(), df_amax['Y'].to_numpy(), df_amax['amax'].to_numpy(), kind='linear')
#
# columns = ['CPT ID', 'X', 'Y', 'PGA', 'LPI_50', 'S_50']
# df_hazzard = pd.DataFrame(data=[], columns=columns)
# for cpt in pd.unique(df['CPT ID']):
#     coord = df[df['CPT ID']==cpt][['X coord', 'Y coord']].values[0, :]
#     pga = min(max(0.10, f(coord[0], coord[1]).item()), 0.3)
#     if pga <= 0.10:
#         LPI_50 = df[(df['CPT ID']==cpt) & (df['PGA']==pga)]['LPI_50'].values.item()
#         S_50 = df[(df['CPT ID']==cpt) & (df['PGA']==pga)]['S_50'].values.item()
#     elif pga >=0.30:
#         LPI_50 = df[(df['CPT ID']==cpt) & (df['PGA']==pga)]['LPI_50'].values.item()
#         S_50 = df[(df['CPT ID']==cpt) & (df['PGA']==pga)]['S_50'].values.item()
#     else:
#         x1, x2 = np.max(df[(df['CPT ID']==cpt) & (df['PGA']<=pga)]['PGA'].values), np.min(df[(df['CPT ID']==cpt) & (df['PGA']>=pga)]['PGA'].values)
#         y1, y2 = np.max(df[(df['CPT ID']==cpt) & (df['PGA']<=pga)]['LPI_50'].values), np.min(df[(df['CPT ID']==cpt) & (df['PGA']>=pga)]['LPI_50'].values)
#         LPI_50 = y1 + (y2 - y1) / (x2 - x1) * (pga - x1)
#         y1, y2 = np.max(df[(df['CPT ID'] == cpt) & (df['PGA'] <= pga)]['S_50'].values), np.min(df[(df['CPT ID'] == cpt) & (df['PGA'] >= pga)]['S_50'].values)
#         S_50 = y1 + (y2 - y1) / (x2 - x1) * (pga - x1)
#     df_hazzard = df_hazzard.append(pd.DataFrame(data=[[cpt, coord[0], coord[1], pga, LPI_50, S_50]], columns=columns))
#
# df_hazzard = df_hazzard.reset_index(drop=True)
# df_hazzard.to_csv(path+'\\Rail_Hazzard_results.csv', index=False)

#-----------------------------------------------------------------------------------------------------------------------


# picklefile = open(path+'\\'+'Rail_analysis_0.25.pickle', 'rb')
# picklefile = open(path+'\\'+'Road_analysis_0.15.pickle', 'rb')
# picklefile = open(path+'\\'+'Groningen_AllRail_output_model.pickle', 'rb')
picklefile = open(path+'\\'+'Groningen_AllRoad_output_model.pickle', 'rb')
model_dict = pickle.load(picklefile)


columns = ['CPT ID', 'PGA', 'LPI', 'S']


dict = {}
for (cpt, model) in model_dict.items():
    h_crust = model['MC'].gwl

    CRR, qc1ncs = calc_CRR(model)

    idx_liq = np.where(np.logical_and(model['MC'].z<=20, model['MC'].FoS_det<9999))[0]
    if list(idx_liq) == []:
        dict[cpt] = {'H_crust': h_crust, 'H_liq': 9999, 'CRR_mean': 9999, 'CRR_std': 9999, 'qc1ncs_mean': 9999, 'qc1ncs_std': 9999}
    else:
        h_liq = 0
        memory = 0
        for idx in idx_liq:
            if idx == memory + 1:
                h_liq += model['MC'].z[idx] - model['MC'].z[memory]
            memory = idx
        dict[cpt] = {'H_crust': h_crust, 'H_liq': h_liq, 'CRR_mean': np.mean(CRR[idx_liq]),
                     'CRR_std': np.std(CRR[idx_liq]), 'qc1ncs_mean': np.mean(qc1ncs[idx_liq]), 'qc1ncs_std': np.std(qc1ncs[idx_liq])}

columns = ['CPT ID', 'H_crust', 'H_liq', 'CRR_mean', 'CRR_std', 'qc1ncs_mean', 'qc1ncs_std']
df_model = pd.DataFrame(data=[], columns=columns)
for key, val in dict.items():
    df_model = df_model.append(pd.DataFrame(data=[[key, val['H_crust'], val['H_liq'], val['CRR_mean'],
                                                   val['CRR_std'], val['qc1ncs_mean'], val['qc1ncs_std']]],
                                            columns=columns))
# df_model.to_csv(path+'\\Rail_LiqTrigger_output.csv', index=False)
df_model.to_csv(path+'\\Road_LiqTrigger_output.csv', index=False)


for (cpt, model) in model_dict.items():
    CRR, qc1ncs = calc_CRR(model)
    df_prof = pd.DataFrame(data=np.c_[model['MC'].z, elevation_dict[cpt] - model['MC'].z, model['MC'].FoS_det, CRR, qc1ncs],
                           columns=['Depth [m]', 'Elevation [mNAP]', 'FoS [-]', 'CRR [-]', 'qc1ncs [-]'])
    df_prof.to_csv(path+'\\Deterministic FoS profiles\\'+cpt+'.csv', index=False)


