import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import pickle
import Liquefaction_FoS as LFoS
from scipy.stats import t

#-----------------------------------------------------------------------------------------------------------------------

# path = '..\\Railway analysis\\Groningen - AllRail - Actual PGA - Screened locations'
# picklefile = open(path+'\\'+'Groningen_AllRail_output_model.pickle', 'rb')

path = '..\\Road analysis\\Groningen - AllRoad - Actual PGA - Screened locations'
picklefile = open(path+'\\'+'Groningen_AllRoad_output_model.pickle', 'rb')

model_dict = pickle.load(picklefile)

columns = ['CPT ID', 'CRR_mean_05', 'CRR_mean', 'CRR_mean_95']
df = pd.DataFrame(data=[], columns=columns)

for i, (key, val) in enumerate(model_dict.items()):
    CRR = val['MC'].CRR.flatten()
    qc1ncs = val['MC'].qc1ncs.flatten()
    Ic = val['MC'].Ic.flatten()
    FoS = val['MC'].FoS.flatten()

    filter_test = np.logical_and(CRR<=0.6, Ic<=2.6)

    if np.sum(filter_test) > 10:
        idx_filter = np.where(filter_test)[0]
        CRR_filt = CRR[idx_filter]
        qc1ncs_filt = qc1ncs[idx_filter]
        mean = CRR_filt.mean()
        sd = CRR_filt.std()
        mean_q05 = mean + sd / np.sqrt(CRR_filt.size) * t.ppf(0.05, df=CRR_filt.size-1)
        mean_q95 = mean + sd / np.sqrt(CRR_filt.size) * t.ppf(0.95, df=CRR_filt.size-1)
        df_dummy = pd.DataFrame(data=[[key, mean_q05, mean, mean_q95]],
                                columns=columns)
    else:
        df_dummy = pd.DataFrame(data=[[key, 9999, 9999, 9999]], columns=columns)
    df = df.append(df_dummy)

# df.to_csv(path + '\\' + 'Rail' + '_CRR_stats.csv', index=False)
df.to_csv(path + '\\' + 'Road' + '_CRR_stats.csv', index=False)

# CRR_575_dict = {}
# CRR_595_dict = {}
# CRR_dict = {}
# qc1ncs_dict = {}
#
# columns = ['CPT ID', 'z5_mean', 'z5_std', 'z75_mean', 'z75_std', 'z95_mean', 'z95_std',
#            'CRR_5_75_mean', 'CRR_5_75_std', 'CRR_5_95_mean', 'CRR_5_95_std']
# df = pd.DataFrame(data=[], columns=columns)
#
# for i, (key, val) in enumerate(model_dict.items()):
#     z = val['MC'].z
#
#     CRR_575_dict[key] = []
#     CRR_595_dict[key] = []
#     CRR_dict[key] = []
#     qc1ncs_dict[key] = []
#     z5 = []
#     z75 = []
#     z95 = []
#
#     for (fos, CRR, qc1ncs) in zip(val['MC'].FoS.T, val['MC'].CRR.T, val['MC'].qc1ncs.T):
#         cumulative_trigger = np.cumsum(np.where(fos <= 1, 1, 0))
#
#         perc5 = cumulative_trigger.max() * 0.05
#         perc75 = cumulative_trigger.max() * 0.75
#         perc95 = cumulative_trigger.max() * 0.95
#
#         if cumulative_trigger.max() >= 10:
#             z5.append(z[np.argmin(np.abs(cumulative_trigger-perc5))])
#             z75.append(z[np.argmin(np.abs(cumulative_trigger-perc75))])
#             z95.append(z[np.argmin(np.abs(cumulative_trigger-perc95))])
#             CRR_575_mean = np.mean(CRR[np.where(np.logical_and(z>=z5[-1], z<=z75[-1], fos<=1))[0]])
#             CRR_595_mean = np.mean(CRR[np.where(np.logical_and(z>=z5[-1], z<=z95[-1], fos<=1))[0]])
#         else:
#             CRR_575_mean = np.mean(CRR)
#             CRR_595_mean = np.mean(CRR)
#
#         CRR_575_dict[key].append(CRR_575_mean)
#         CRR_595_dict[key].append(CRR_595_mean)
#         CRR_dict[key].append(CRR)
#         qc1ncs_dict[key].append(qc1ncs)
#
#     z5 = np.array(z5)
#     z75 = np.array(z75)
#     z95 = np.array(z95)
#     CRR_575_dict[key] = np.array(CRR_575_dict[key])
#     CRR_595_dict[key] = np.array(CRR_595_dict[key])
#
#     df_dummy = pd.DataFrame(data=[[key,
#                                    np.mean(z5), np.std(z5),
#                                    np.mean(z75), np.std(z75),
#                                    np.mean(z95), np.std(z95),
#                                    np.mean(CRR_575_dict[key]), np.std(CRR_575_dict[key]),
#                                    np.mean(CRR_595_dict[key]), np.std(CRR_595_dict[key])]],
#                             columns=columns)
#     df = df.append(df_dummy)

