import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
from shutil import copyfile

main_folder_path = 'D:\\Probabilistic_Liquefaction\\Railway analysis\\Groningen - AllRail - Actual PGA'
# main_folder_path = 'D:\\Probabilistic_Liquefaction\\Road analysis\\Groningen - AllRoad'
download_folder = 'CPT all'
input_folder = 'CPT gefs'
amax_file = 'amax_data'
download_path = main_folder_path + '\\' + download_folder
input_path = main_folder_path + '\\' + input_folder
# output_filename ='Groningen_Delfzijl_Rail_input'
# output_filename ='Groningen_Eemshaven_Rail_input'
# output_filename ='Groningen_Winschoten_Rail_input'
# output_filename ='Groningen_Assen_Rail_input'
# output_filename ='Groningen_Westereen_Rail_input'
output_filename ='Groningen_AllRoad_input'

downloaded_files = [f for f in listdir(download_path) if isfile(join(download_path, f))]
gef_files = [f.split('.')[0] for f in downloaded_files if f.split('.')[1] == 'gef']
gef_files = [f for f in gef_files if f.split('_')[-1][:3] != 'DIS'] # Remove child CPTs (they include "_DISX" at the end of the name)

for f in gef_files:
    src = download_path + '\\' + f + '.gef'
    dst = input_path + '\\' + f + '.gef'
    copyfile(src, dst)

zone = [0 for f in gef_files]
Vs12 = [140 for f in gef_files]
amax = [0.25 for f in gef_files]
M = [5 for f in gef_files]
FC = [20 for f in gef_files]
gwl = [0.5 for f in gef_files]

df_input = pd.DataFrame(data = list(zip(gef_files, zone, Vs12, amax, M, FC, gwl)),
                        columns=['CPT_ID', 'zone', 'Vs_12', 'a_max', 'M', 'FC', 'GWL'])
df_input.to_csv(main_folder_path + '\\' + output_filename + '.csv', index=False)

