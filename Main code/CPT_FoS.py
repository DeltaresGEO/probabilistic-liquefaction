import os
import numpy as np
import xlrd
import json
import graphs
np.warnings.filterwarnings('ignore')
import matplotlib.pylab as plt
from PIL import Image


def IC_calc(qc_list, frc_list, tot_stress, eff_stress, name, approach="IB2008"):

    IC = []

    for idx, qc in enumerate(qc_list):
        if approach == 'NPR':
            Pa = 101.3
            Q5 = np.array([((qc[i] - tot_stress[idx][i]) / Pa) * (Pa / eff_stress[idx][i]) ** 0.5 for i, _ in enumerate(qc)])
            Q5[Q5 < 0] = 0
            Q75 = np.array([((qc[i] - tot_stress[idx][i]) / Pa) * (Pa / eff_stress[idx][i]) ** 0.75 for i, _ in enumerate(qc)])
            Q75[Q75 < 0] = 0
            Q1 = np.array([((qc[i] - tot_stress[idx][i]) / Pa) * (Pa / eff_stress[idx][i]) ** 1 for i, _ in enumerate(qc)])
            Q1[Q1 < 0] = 0
            F = np.array([(frc_list[idx][i] / (qc[i] - tot_stress[idx][i])) * 100 for i, _ in enumerate(qc)])
            F[F < 0] = 0

            ic5 = ((3.47 - np.log10(np.array(Q5))) ** 2. + (np.log10(F) + 1.22) ** 2.) ** 0.5
            ic75 = ((3.47 - np.log10(np.array(Q75))) ** 2. + (np.log10(F) + 1.22) ** 2.) ** 0.5
            ic1 = ((3.47 - np.log10(np.array(Q1))) ** 2. + (np.log10(F) + 1.22) ** 2.) ** 0.5

            ic = np.ones(len(ic1)) * np.nan                   #ToDo is correct? this changed to prevent IC = 1 while IC = nan
            ic[ic1 >= 2.6] = ic1[ic1 >= 2.6]
            ic[(ic1 < 2.6) & (ic5 <= 2.6)] = ic5[(ic1 < 2.6) & (ic5 <= 2.6)]
            ic[(ic1 < 2.6) & (ic5 > 2.6)] = ic75[(ic1 < 2.6) & (ic5 > 2.6)]

        else:
            Pa = 100
            F = [(frc_list[idx][i] / (qc[i] - tot_stress[idx][i])) * 100 for i, _ in enumerate(qc)]
            Q = [((qc[i] - tot_stress[idx][i]) / Pa) * (Pa / eff_stress[idx][i]) ** 0.5 for i, _ in enumerate(qc)]
            ic = ((3.47 - np.log10(np.array(Q))) ** 2. + (np.log10(F) + 1.22) ** 2.) ** 0.5

        IC.append(ic)

    return IC

def read_key(file_name):
    # read the key of the GEF files
    with open(file_name, 'r') as f:
        file = f.readlines()

    dat = [int(line.strip('\n\r').split('=')[1]) for line in file]

    key = {'depth': dat[0],
           'tip': dat[1],
           'friction': dat[2],
           'friction_nb': dat[3],
           'water': dat[4]}
    return key


def read_locations(file_name):
    # read the location file: section and number of CPT
    with open(file_name, 'r') as f:
        file = f.readlines()

    file = [line.strip('\n\r').split(';') for line in file]

    dat = {}
    for val in file[1:]:
        dat.update({str(val[0]): {}})
        dat[val[0]].update({'qc_ini': val[1]})
        dat[val[0]].update({'qc_end': val[2]})
        dat[val[0]].update({'pwp': val[3]})

    return dat


def n_iter(qc1ncs):
    """
    Computation of stress exponent *n*
    :param qc1ncs: corrected tip resistance with Fine
    """
    qc1ncs[qc1ncs>254] = qc1ncs[qc1ncs>254] = 254
    n = 1.338 - 0.294 * qc1ncs ** 0.264
    return n


def read_pleistoce_layers(file_name, dic):
    # read the dept of the pleistocene layers: section and number of CPT
    with open(os.path.join(file_name), 'r') as f:
        file = f.readlines()

    file = [line.strip('\n\r').split(';') for line in file]

    for val in dic:
        i = [idx for idx, nam in enumerate(file) if nam[0] == val]
        if i:
            dic[val].update({'pleistocene': float(file[i[0]][1])})
            dic[val].update({'water': float(file[i[0]][2])})

    return dic


def read_csr(file_path, locs):
    # reads for each section the CSR for kruin and achterland
    for sec in locs:
        # read the CSR file corresponding to  the section
        sec_path = os.path.join(file_path, 'vak' + str(sec))
        name_kruin = 'vak' + str(sec) + r'-Kruin'
        sec_path_kruin = os.path.join(sec_path, r'res_deepSoil\batch_' + name_kruin)
        data = np.genfromtxt(os.path.join(sec_path_kruin, 'csr_NAP_average_' + name_kruin + '.csv'), delimiter=';',
                             skip_header=2)

        locs[sec].update({'depth_kruin': data[:, 0]})
        locs[sec].update({'CSR_kruin': data[:, 1]})

        name_acth = 'vak' + str(sec) + r'-Achterland'
        sec_path_acher = os.path.join(sec_path, r'res_deepSoil\batch_' + name_acth)
        data = np.genfromtxt(os.path.join(sec_path_acher, 'csr_NAP_average_' + name_acth + '.csv'), delimiter=';',
                             skip_header=2)

        locs[sec].update({'depth_acht': data[:, 0]})
        locs[sec].update({'CSR_acht': data[:, 1]})

    return locs


def read_cpt(key_cpt, location, file_path, path_results, assumptions, do_plots=True):
    # list all gef files
    gef_files = [fil + '.GEF' for fil in location]
    # gef_files = gef_files[:10]  # ToDo: delete

    # create results
    res = FileJson()

    # read the GEF files
    name, coord, km, depth, NAP, tip, friction, friction_nb, gamma, gamma_l, gamma_k, aux_lit, water = read_gef(file_path, gef_files, key_cpt, location, gws=assumptions["gws"])
    # print(name)

    # calculate stresses
    # Robertson & Cabal
    sigma_tot, sigma_eff, fos, depth_p, PGA = stress_calc(name, gamma, NAP, depth, location, water)
    # Douglas & Olson
    sigma_tot_l, sigma_eff_l, fos_l, _, PGA_l = stress_calc(name, gamma_l, NAP, depth, location, water)
    # Lengkeek et al
    sigma_tot_k, sigma_eff_k, fos_k, _, PGA_k = stress_calc(name, gamma_k, NAP, depth, location, water)

    # plt.plot(sigma_tot[0], NAP[0], sigma_tot_k[0], NAP[0])

    # compute IC
    IC = IC_calc(tip, friction, sigma_tot, sigma_eff, name, approach=assumptions["approach"])
    IC_l = IC_calc(tip, friction, sigma_tot_l, sigma_eff_l, name, approach=assumptions["approach"])
    IC_k = IC_calc(tip, friction, sigma_tot_k, sigma_eff_k, name, approach=assumptions["approach"])

    # plt.plot(IC[0], NAP[0], IC_k[0], NAP[0])


    # normalise qc -> qc1n
    #ToDO: with NPR it returns qc1ncs
    # with IB returns only qc1n
    qc1n, lito, n, Cn, fine_c, dq = qc_to_qc1n(tip, friction, sigma_tot, sigma_eff, aux_lit, IC, name, approach=assumptions["approach"]) #removed psi
    qc1n_l, lito_l, n_l, Cn_l, fine_c_l, dq_l = qc_to_qc1n(tip, friction, sigma_tot_l, sigma_eff_l, aux_lit, IC_l, name, approach=assumptions["approach"])
    qc1n_k, lito_k, n_k, Cn_k, fine_c_k, dq_k = qc_to_qc1n(tip, friction, sigma_tot_k, sigma_eff_k, aux_lit, IC_k, name, approach=assumptions["approach"])

    # liquefaction analysis - robertson & cabal
    RC_2014 = calculationFS_IB(qc1n, IC, sigma_tot, sigma_eff, name, NAP, depth, location, assumptions, fos, water, PGA)
    DO_2014 = calculationFS_IB(qc1n_l, IC_l, sigma_tot_l, sigma_eff_l, name, NAP, depth, location, assumptions, fos_l, water, PGA)
    LK_2018 = calculationFS_IB(qc1n_k, IC_k, sigma_tot_k, sigma_eff_k, name, NAP, depth, location, assumptions, fos_k, water, PGA)

    if isinstance(assumptions['LPI'], float):
        # if LPI is a float, it computes LPI starting at the float
        LPI_RC, F, w = calc_LPI(depth, RC_2014[0], lito, IC, assumptions['LPI'], approach=assumptions["approach"])
        LPI_DO, F_DO, w_DO = calc_LPI(depth, DO_2014[0], lito_l, IC_l, assumptions['LPI'], approach=assumptions["approach"])
        LPI_LK, F_LK, w_LK = calc_LPI(depth, LK_2018[0], lito_k, IC_k, assumptions['LPI'], approach=assumptions["approach"])

    else:
        # it computes LPI starting at pleistocene depth
        LPI_RC, F, w = calc_LPI(depth, RC_2014[0], lito, IC, depth_p, approach=assumptions["approach"])
        LPI_DO = calc_LPI(depth, DO_2014[0], lito_l, IC_l, depth_p, approach=assumptions["approach"])
        LPI_LK = calc_LPI(depth, LK_2018[0], lito_k, IC_k, depth_p, approach=assumptions["approach"])

    # FoS percentage
    FoS_perc = compute_percentage(RC_2014[0], lito)
    Ru_perc = compute_percentage(RC_2014[1], lito)

    # FoS_perc_km = compute_percentage(RC_2014[0], lito, km=[8, km]) #ToDo change amount of km
    # Ru_perc_km = compute_percentage(RC_2014[1], lito, km=[8, km])

    if do_plots:
        # plot percentages
        # graphs.xy_plot([np.sort(FoS_perc)], [np.linspace(0, 100, len(FoS_perc))], '', [''], 'FoS [-]', 'Percentage [%]',
        #                path_results, 'FoS_percentage', extension='png', ylim=(0, 100), xlim=(0, 3))
        #
        # graphs.xy_plot([np.sort(Ru_perc)], [np.linspace(0, 100, len(Ru_perc))], '', [''], 'Ru [-]', 'Percentage [%]',
        #                path_results, 'Ru_percentage', extension='png', ylim=(0, 100), xlim=(0, 1.2))

        # graphs.xy_plot([np.sort(FoS_perc_km)], [np.linspace(0, 100, len(FoS_perc_km))], '', [''], 'FoS [-]', 'Percentage [%]',
        #                path_results, 'FoS_percentage_km_0_3.7', extension='png', ylim=(0, 100), xlim=(0, 3))
        #
        # graphs.xy_plot([np.sort(Ru_perc_km)], [np.linspace(0, 100, len(Ru_perc_km))], '', [''], 'Ru [-]', 'Percentage [%]',
        #                path_results, 'Ru_percentage_km_0_3.7', extension='png', ylim=(0, 100), xlim=(0, 1.2))

        # # compare Robertson and Douglas
        # graphs.make_plots_comp(name, [qc1n, qc1n], [IC, IC_l], [RC_2014[3], DO_2014[3]],
        #                        [RC_2014[2], DO_2014[2]], NAP, [r'Robertson & Cabal', r'Douglas & Olson'],
        #                        os.path.join(path_results, 'BI2014_qc'), lito)
        # #ToDo removed Psi and replaced by Ru
        # graphs.make_plots_comp2(name, [RC_2014[6], DO_2014[6]], [RC_2014[5], DO_2014[5]], [RC_2014[0], DO_2014[0]],
        #                         [RC_2014[1], DO_2014[1]], [RC_2014[1], DO_2014[1]], [LPI_RC, LPI_DO], NAP,
        #                         [r'Robertson & Cabal', r'Douglas & Olson'],
        #                         os.path.join(path_results, 'BI2014_FoS'), lito)
        # graphs.make_plots_comp3(name, RC_2014[2], IC, RC_2014[6], RC_2014[5], RC_2014[0], depth,
        #                         '', os.path.join(path_results, 'BI2014_Fugro'), lito)

        graphs.make_plots_comp4(name, [tip], [friction], [friction_nb], [water], NAP, [name[0].split(".GEF")[0]],
                                os.path.join(path_results, 'NPR_cpt'))

        graphs.make_plots_comp(name, [qc1n, qc1n], [sigma_eff, sigma_eff_k], [IC, IC_k],
                               [RC_2014[2], LK_2018[2]], NAP, [r'Robertson & Cabal', r'Lengkeek'],
                               os.path.join(path_results, 'NPR_qc'), [lito, lito_k])
        graphs.make_plots_comp2(name, [RC_2014[6], LK_2018[6]], [RC_2014[5], LK_2018[5]], [RC_2014[0], LK_2018[0]],
                                [RC_2014[1], LK_2018[1]], [LPI_RC, LPI_LK], NAP,
                                [r'Robertson & Cabal', r'Lengkeek'],
                                os.path.join(path_results, 'NPR_FoS'), [lito, lito_k])

        # compare Robertson and Douglas
        # graphs.make_plots_NPR(name, [qc1n, qc1n], [IC, IC_l], [RC_2014[3], DO_2014[3]],
        #                        [RC_2014[2], DO_2014[2]], NAP, [r'Robertson & Cabal', r'Douglas & Olson'],
        #                        os.path.join(path_results, 'NPR_qc'), lito)


    # write csv file
    write_csv(name, LPI_RC, LPI_LK, NAP, qc1n, LK_2018, IC, IC_k, tip, friction_nb, sigma_tot, sigma_eff, n, Cn, fine_c,
              dq, F, w, F_LK, w_LK, lito, lito_k, sigma_eff_k, sigma_tot_k, gamma, gamma_k, os.path.join(path_results, 'csv'))

    # create dictionary structure ToDo fix this
    res.add(name, coord, km, LPI_RC, LPI_LK, NAP, LK_2018, IC_k, lito, depth_p) #removed psi
    res.write(path_results)

    return


def write_csv(name, LPI_RC, LPI_LK, NAP, qc1n, data, IC, IC_k, tip, friction_nb, tot_stress, eff_stress, n, Cn, fine_c, dq, F, w,
              F_LK, w_LK, lito, lito_k, sigma_eff_k, sigma_tot_k, gamma, gamma_k, path_res):

    # check if folder exists. if not creates
    if not os.path.exists(path_res):
        os.makedirs(path_res)

    for i in range(len(name)):
        with open(os.path.join(path_res, name[i] + '.csv'), 'w') as f:
            # f.write('name;depth NAP;qc_kPa;qc1ncs;tot_stress;eff_stress;Ic;CRR;CSR;FoS;n;Cn;FC;dq;Csigma;Ksigma;F(z);w;LPI\n')
            f.write('name;depth NAP;qc_kPa;Fn_%;qc1ncs;lito_RC;gamma;tot_stress;eff_stress;Ic;lito_LK;gamma_LK;tot_stress_LK;eff_stress_LK;Ic;CRR;CSR;FoS;F(z);w;LPI_RC;F(z);w;LPI_LK\n')
            for j in range(len(NAP[i])):
                f.write(name[i] + ';' +
                        str(NAP[i][j]) + ';' +
                        str(tip[i][j]) + ';' +
                        str(friction_nb[i][j]) + ';' +
                        # str(qc1n[i][j]) + ';' +
                        str(data[2][i][j]) + ';' +
                        str(lito[i][j]) + ';' +
                        str(gamma[i][j]) + ';' +
                        str(tot_stress[i][j]) + ';' +
                        str(eff_stress[i][j]) + ';' +
                        str(IC[i][j]) + ';' +
                        str(lito_k[i][j]) + ';' +
                        str(gamma_k[i][j]) + ';' +
                        str(sigma_tot_k[i][j]) + ';' +
                        str(sigma_eff_k[i][j]) + ';' +
                        str(IC_k[i][j]) + ';' +
                        str(data[5][i][j]) + ';' +
                        str(data[6][i][j]) + ';' +
                        str(data[0][i][j]) + ';' +
                        # str(n[j]) + ';' +
                        # str(Cn[j]) + ';' +
                        # str(fine_c[j]) + ';' +
                        # str(dq[j]) + ';' +
                        # str(data[8][j]) + ';' +
                        # str(data[7][j]) + ';' +
                        str(F[i][j]) + ';' +
                        str(w[i][j]) + ';' +
                        str(LPI_RC[i]) + ';' +
                        str(F_LK[i][j]) + ';' +
                        str(w_LK[i][j]) + ';' +
                        str(LPI_LK[i]) + '\n')

            # print('LPI_RC = ' + str(LPI_RC[i]))
            # print('LPI_LK = ' + str(LPI_LK[i]))
    return

def write_csv_LPI(name, LPI, path_res):

    # check if folder exists. if not creates
    if not os.path.exists(path_res):
        os.makedirs(path_res)

    for i in range(len(name)):

        with open(os.path.join(path_res, name[i] + '_LPI.csv'), 'w') as f:
            f.write('name;LPI\n')
            f.write(name[i] + ';' + str(LPI[0][i]) + '\n')
    return


def compute_percentage(data, lito, km=False):
    # compute percentage
    perc = []
    for i, foo in enumerate(data):
        if not km:
            aux = [foo[j] for j in range(len(foo)) if lito[i][j] == 'sand']
            perc = np.concatenate((perc, aux))
        else:
            if km[1][i] <= km[0]:
                aux = [foo[j] for j in range(len(foo)) if lito[i][j] == 'sand']
                perc = np.concatenate((perc, aux))

    perc = perc[~np.isnan(perc)]

    return perc


def calc_LPI(depth, FoS_list, lito, IC, shift, approach="IB2008"):

    LPI = []
    F = []
    w = []

    if isinstance(shift, float):
    # if the shift is only one value, it will be constant for all of it
        shift = [-shift] * len(FoS_list)

    for idx, FS in enumerate(FoS_list):
        depth_LPI = - np.array(depth[idx])

        if (approach == 'IB2008') or (approach == 'BI2014'):
            F = 1. - FS

            F[FS > 1] = 0
            F[np.isnan(FS)] = 0
            indices = [i for i, x in enumerate(lito[idx]) if x == 'not']
            F[indices] = 0

            w = 10 - 0.5 * depth_LPI

            aux_lpi = F * w

            idx_min = (np.abs(depth_LPI - (0.0 - shift[idx]))).argmin()
            idx_max = (np.abs(depth_LPI - (20.0 - shift[idx]))).argmin()

            LPI.append(np.trapz(aux_lpi[idx_min:idx_max], np.array(depth_LPI)[idx_min:idx_max]))

            # voor HHNK wordt standaard LPI gebruikt
        elif approach == 'NPR':
            aux_F = 1. - FS

            aux_F[FS > 1] = 0
            aux_F[np.isnan(FS)] = 0
            # indices = [i for i, x in enumerate(lito[idx]) if x == 'not']
            indices = [i for i, x in enumerate(IC[idx]) if x >= 2.6] #if IC is greater than 2.6 than no LPI
            aux_F[indices] = 0

            aux_w = 10 - 0.5 * depth_LPI

            aux_lpi = aux_F * aux_w

            idx_min = (np.abs(depth_LPI - (0.0 - shift[0]))).argmin()
            idx_max = (np.abs(depth_LPI - (20.0 - shift[0]))).argmin()

            LPI.append(np.trapz(aux_lpi[idx_min:idx_max], np.array(depth_LPI)[idx_min:idx_max]))
            F.append(aux_F)
            w.append(aux_w)

    return LPI, F, w


def calculationFS_IB(qc1n, IC, sigma_tot, sigma_eff, name, NAP, depth, location, approach, fos, water, PGA, qc_lim=500):

    if approach["approach"] == "NPR": #ToDo fix this. with NPR qc1n is already qc1ncs, but FC is not passed
        qc1ncs = [np.array(q) for q in qc1n]
        FC = [np.zeros(len(q)) for q in qc1n]
    else:
        # fine correction and layer correction
        qc1ncs, FC = fine_correction(qc1n, IC, name, approach['approach'], approach['Cfc'])

    # # layer correction - kh = 1.8 for layer 8
    # qc1ncs = layer_corr(name, qc1ncs, NAP, location[section])

    # limit maximum value of qc1ncs for plotting
    qc1ncs_edt = []
    for q in qc1ncs:
        q[q > qc_lim] = qc_lim
        qc1ncs_edt.append(q)

    # CRR calculation
    crr = crr_calc(qc1ncs, approach['approach'])

    # aging factor correction - for pleistocene sand Kdr factor
    k_dr = aging_fct(name, NAP, location, ag_fct=approach['Kdr'])

    # kappa sigma
    k_sigma, C_sigma = kappa_sigma(qc1ncs, sigma_eff, approach['approach'])

    # determine CSR
    csr = csr_calc(name, NAP, depth, PGA, sigma_tot, sigma_eff, approach['VS12'], approach=approach['approach'])

    # # determine k_alpha
    # k_alpha = k_alpha_calc(name, location[section], NAP)

    # FoS calculation crest - with safety factor
    FS, Ru = FoS(crr, k_sigma, csr, k_dr, qc1ncs, fos, PGA, water, approach['VS12'], k_alpha=1, theta=0.7, MSF_method=approach['approach'])

    # # safety factor on FS
    # FoS_fs = [val / fos[i] for i, val in enumerate(FS)]

    # residual strength
    res = res_str(qc1n, FC)

    return [FS, Ru, qc1ncs_edt, FC, res, crr, csr, k_sigma, C_sigma]


def k_alpha_calc(name, loc_sec, NAP):
    k_alpha = [np.ones((len(q))) for q in NAP]

    for ind, nam in enumerate(name):
        cpt_number = int(nam.split('.GEF')[0].split('DKP')[-1])

        if 8 in loc_sec.keys():
            idx = [j for j, x in enumerate(loc_sec[8]['cpt_nr']) if x == cpt_number]
            top = [loc_sec[8]['top'][j] for j in idx]
            bot = [loc_sec[8]['bottom'][j] for j in idx]
            for j, _ in enumerate(idx):
                for i, _ in enumerate(k_alpha[ind]):
                    if top[j] >= NAP[ind][i] >= bot[j]:
                        if str(cpt_number)[-2:] == '50':
                            k_alpha[ind][i] = 1.0
                        else:
                            k_alpha[ind][i] = loc_sec[8]['k_alpha']

        if 9 in loc_sec.keys():
            idx = [j for j, x in enumerate(loc_sec[9]['cpt_nr']) if x == cpt_number]
            top = [loc_sec[9]['top'][j] for j in idx]
            bot = [loc_sec[9]['bottom'][j] for j in idx]

            for j, _ in enumerate(idx):
                for i, _ in enumerate(k_alpha[ind]):
                    if top[j] >= NAP[ind][i] >= bot[j]:
                        if str(cpt_number)[-2:] == '50':
                            k_alpha[ind][i] = 1.0
                        else:
                            k_alpha[ind][i] = loc_sec[9]['k_alpha']

    return k_alpha


def res_str(qc1n, FC):
    # polynomial fit of data of IB2008
    fines = [0, 10, 25, 50, 75]
    delta_q = [0, 10, 25, 45, 55]
    # f = np.poly1d(np.polyfit(fines, delta_q, 2))

    # least square minimisation forcing to pass by origin
    coeff = np.transpose([np.array(fines) ** 2, np.array(fines)])
    ((a, b), _, _, _) = np.linalg.lstsq(coeff, np.array(delta_q))
    polynomial = np.poly1d([a, b, 0])

    # import matplotlib.pylab as plt
    # plt.plot(fines, delta_q, marker='x', linestyle='')
    # plt.plot(np.linspace(0 ,100, 100), f(np.linspace(0,100,100)), color='r')
    # plt.plot(np.linspace(0, 100, 100), polynomial(np.linspace(0, 100, 100)), color='r')
    # plt.show()

    res = []
    for i, qc in enumerate(qc1n):
        qc1n_sr = qc + polynomial(FC[i])
        residual = np.arctan(np.exp(qc1n_sr / 24.5 - (qc1n_sr / 61.7) ** 2 + (qc1n_sr / 106) ** 3 - 4.42))
        res.append(residual * 180. / np.pi)

    return res


def aging_fct(name, NAP, locx, ag_fct=1.3):
    aging = []
    # aging correction
    for ind, nam in enumerate(name):
        depth = locx[nam.split('.GEF')[0]]['pleistocene']

        ag_vec = np.ones(len(NAP[ind]))

        for i, k in enumerate(ag_vec):
            if NAP[ind][i] < depth:
                ag_vec[i] = ag_fct

        aging.append(ag_vec)
    return aging


def layer_corr(name, qc1n, NAP, loc_sec, kh=1.8):
    # interlayer correction: only for layer 8
    for ind, nam in enumerate(name):
        cpt_number = int(nam.split('.GEF')[0].split('DKP')[-1])

        if 8 in loc_sec.keys():
            if cpt_number in loc_sec[8]['cpt_nr']:

                kh_vec = np.ones(len(qc1n[ind]))

                idx = [j for j, x in enumerate(loc_sec[8]['cpt_nr']) if x == cpt_number]
                top = [loc_sec[8]['top'][j] for j in idx]
                bot = [loc_sec[8]['bottom'][j] for j in idx]

                for j, _ in enumerate(idx):
                    for i, k in enumerate(kh_vec):
                        if top[j] >= NAP[ind][i] >= bot[j]:
                            kh_vec[i] = kh

                qc1n[ind] *= kh_vec

    return qc1n


def read_gef(file_path, gef_files, key_cpt, location, gws):

    depth = []
    coord = []
    km = []
    z_NAP = []
    tip = []
    friction = []
    friction_nb = []
    water = []
    name = []
    gamma = []
    gamma_lito = []
    gamma_k = []
    lito_DO = []
    Pa = 101.3

    for gef in gef_files:
        try:
            with open(os.path.join(file_path, gef), 'r') as f:
                data = f.readlines()

            # search NAP
            idx_nap = [i for i, val in enumerate(data) if val.startswith(r'#ZID=')][0]
            NAP = float(data[idx_nap].split(',')[1])
            # search end of header
            idx_EOH = [i for i, val in enumerate(data) if val.startswith(r'#EOH=')][0]
            # search for coordinates
            idx_coord = [i for i, val in enumerate(data) if val.startswith(r'#XYID=')][0]
            # search index depth
            idx_depth = [int(val.split(',')[0][-1]) - 1 for i, val in enumerate(data)
                         if val.startswith(r'#COLUMNINFO=') and int(val.split(',')[-1]) == int(key_cpt['depth'])][0]
            # search index depth
            idx_tip = [int(val.split(',')[0][-1]) - 1 for i, val in enumerate(data)
                       if val.startswith(r'#COLUMNINFO=') and int(val.split(',')[-1]) == int(key_cpt['tip'])][0]
            # search index depth
            idx_friction = [int(val.split(',')[0][-1]) - 1 for i, val in enumerate(data)
                            if val.startswith(r'#COLUMNINFO=') and int(val.split(',')[-1]) == int(key_cpt['friction'])][0]
            # search index depth
            idx_friction_nb = [int(val.split(',')[0][-1]) - 1 for i, val in enumerate(data)
                               if
                               val.startswith(r'#COLUMNINFO=') and int(val.split(',')[-1]) == int(key_cpt['friction_nb'])][0]
            try:
                idx_water = [int(val.split(',')[0].split("=")[-1]) - 1 for i, val in enumerate(data) if
                             val.startswith(r'#COLUMNINFO=') and int(val.split(',')[-1]) == int(key_cpt['water'])][0]
            except IndexError:
                # print("CPT " + str(gef) + " has no water")
                idx_water = False

            # if first column is -9999 it is error and is is neglected
            if float(data[idx_EOH + 1].replace('|', ' ').split()[1]) == -9999:
                idx_EOH += 1

            # read data & correct depth to NAP
            depth_tmp = [float(data[i].replace('|', ' ').split()[idx_depth]) for i in range(idx_EOH + 1, len(data))]
            tip_tmp = [float(data[i].replace('|', ' ').split()[idx_tip]) * 1000. for i in range(idx_EOH + 1, len(data))]
            friction_tmp = [float(data[i].replace('|', ' ').split()[idx_friction]) * 1000. for i in range(idx_EOH + 1, len(data))]
            friction_nb_tmp = [float(data[i].replace('|', ' ').split()[idx_friction_nb]) for i in range(idx_EOH + 1, len(data))]
            # if idx_water:
            #     water_tmp = [float(data[i].replace('|', ' ').split()[idx_water]) * 1000 for i in range(idx_EOH + 1, len(data))]
            # else:
            #     water_tmp = np.cumsum(9.81 * np.diff(depth_tmp))

            #create waterspanning
            water_tmp = np.append(0., np.cumsum(9.81 * np.diff(depth_tmp)))
            water_tmp = water_tmp -((abs(gws))*9.81)
            water_tmp[water_tmp < 0] = 0

            name.append(gef)

            aux = data[idx_coord].split('\n')[0].split(',')[1:3]
            aux2 = [i.split('.') for i in aux]
            x_cord = ''.join(aux2[0][:-1]) + '.' + aux2[0][-1]
            y_cord = ''.join(aux2[1][:-1]) + '.' + aux2[1][-1]
            coord.append([x_cord, y_cord])

            km.append(location[gef.split('.GEF')[0]]['km'])

            # if tip / friction / friction number are negative -> zero
            tip_tmp = np.array(tip_tmp)
            tip_tmp[tip_tmp <= 0] = 1e-12
            friction_tmp = np.array(friction_tmp)
            friction_tmp[friction_tmp <= 0] = 1e-12
            friction_nb_tmp = np.array(friction_nb_tmp)
            friction_nb_tmp[friction_nb_tmp <= 0] = 1e-12
            # if "water" in key_cpt.keys():
            water_tmp = np.array(water_tmp)
            # water_tmp[water_tmp < 0] = 0

            # remove the points with error: value == -9999
            idx = [[i for i, d in enumerate(depth_tmp) if d == -9999],
                   [i for i, d in enumerate(tip_tmp) if d == -9999],
                   [i for i, d in enumerate(friction_tmp) if d == -9999],
                   [i for i, d in enumerate(friction_nb_tmp) if d == -9999],
                   [i for i, d in enumerate(water_tmp) if d == -9999]]

            idx_remove = [item for sublist in idx for item in sublist]

            depth.append([-i for j, i in enumerate(depth_tmp) if j not in idx_remove])
            z_NAP.append([-i + NAP for j, i in enumerate(depth_tmp) if j not in idx_remove])
            tip.append([i for j, i in enumerate(tip_tmp) if j not in idx_remove])
            friction.append([i for j, i in enumerate(friction_tmp) if j not in idx_remove])
            friction_nb.append([i for j, i in enumerate(friction_nb_tmp) if j not in idx_remove])
            water.append([i for j, i in enumerate(water_tmp) if j not in idx_remove])

            # calculate unit weight according to Robertson & Cabal 2015
            aux = 0.27 * np.log10(np.array(friction_nb[-1])) + 0.36 * np.log10(np.array(tip[-1]) / Pa) + 1.236
            aux[np.abs(aux) == np.inf] = 0
            aux[aux <= 0] = 1e-12
            gamma.append(aux * 9.81)

            # determine type according to Douglas & Olson [adapted by Fred Klosterman]
            typ = ['peat'] * len(gamma[-1])

            # clay limit
            lim = np.tan(np.pi / 2 * np.exp(np.array(friction_nb[-1]) - 6.0)) * 10 + 0.08
            idx = (np.array(tip[-1]) / 1000 > lim).nonzero()[0]
            for ij in idx:
                typ[ij] = 'clay'

            # sand limit
            lim = np.tan(np.pi / 2 * np.exp(np.array(friction_nb[-1]) - 4.5)) * 10 + 0.2
            idx = (np.array(tip[-1]) / 1000 > lim).nonzero()[0]
            for ij in idx:
                typ[ij] = 'sand'

            # assign unit weight to the types
            gamma_l = [None] * len(gamma[-1])
            for i, val in enumerate(typ):
                if val == 'sand':
                    gamma_l[i] = 20
                elif val == 'clay':
                    gamma_l[i] = 14 #17
                elif val == 'peat':
                    gamma_l[i] = 10.5 #12

            gamma_lito.append(gamma_l)
            lito_DO.append(typ)

            #method Lengkeek et al [CPT2018]
            beta = 4.12
            gamma_ref = 19
            qt_ref = 5000 #kPa
            Rf_ref = 30
            gamma_k_temp = gamma_ref - beta * (np.log10(qt_ref / (tip_tmp + 1e-12)) / np.log10(Rf_ref / (friction_nb_tmp + 1e-12)))
            gamma_k_temp[gamma_k_temp < 10.5] = 10.5  #ToDo check if this is correct
            gamma_k_temp[gamma_k_temp > 22] = 22  #ToDo check if this is correct
            gamma_k.append(gamma_k_temp)

            # n = 0.5
            # z = np.diff(np.abs(np.array(z_NAP[0]) - z_NAP[0][0]))
            # z = np.append(z, z[-1])
            # # total stress
            # total_stress = np.cumsum(gamma_k_temp * z)
            # effective_stress = total_stress - np.cumsum(9.81 * z)
            # Cn = (Pa / effective_stress) ** n
            # Qtn = (tip_tmp - total_stress) / Pa * Cn
            # Fr = friction_tmp / (tip_tmp - total_stress) * 100.
            # IC = ((3.47 - np.log10(Qtn)) ** 2. + (np.log10(Fr) + 1.22) ** 2.) ** 0.5
            #
            #
            # plt.plot(gamma[0], depth[0], gamma_k[0], depth[0])

        except:
            print("Cpt: " + str(gef) + " will not open/ not available") #ToDo fix this


    return name, coord, km, depth, z_NAP, tip, friction, friction_nb, gamma, gamma_lito, gamma_k, lito_DO, water


def fine_correction(qc1n, IC, name, approach='IB2008', Cfc=0):
    # fine correction
    qc1ncs = []
    fine_c = []
    for i, qc in enumerate(qc1n):
        # cpt_number = int(name[i].split('.GEF')[0].split('DKP')[-1])

        if approach == 'IB2008':
            # If approach is IB2008
            qc1ncs_aux = np.array(qc)
            FC = 2.8 * np.array(IC[i]) ** 2.6
            FC[FC > 100] = 100
            FC[FC < 0] = 0
            dq = (5.4 + np.array(qc) / 16) * np.exp(1.63 + 9.7 / (FC + 0.01) - (15.7 / (FC + 0.01)) ** 2)

        elif approach == 'BI2014':
            # if approach is BI2014
            qc1ncs_aux = qc
            FC = 80 * (IC[i] + Cfc) - 137
            FC[FC > 100] = 100
            FC[FC < 0] = 0
            dq = (11.9 + np.array(qc) / 14.6) * np.exp(1.63 - 9.7 / (FC + 2.) - (15.7 / (FC + 2.)) ** 2)

        elif approach == 'NPR':
            # If approach is NPR
            qc1ncs_aux = np.array(qc)
            # FC = 2.8 * IC[i] ** 2.6
            FC = 80 * (IC + Cfc) - 137
            FC[FC > 100] = 100
            FC[FC < 0] = 0
            dq = (11.9 + np.array(qc) / 16) * np.exp(1.63 - 9.7 / (FC + 2.) - (15.7 / (FC + 2.)) ** 2)  # todo check if first value, 11.9 and 16

        qc1ncs_aux += dq

        qc1ncs.append(qc1ncs_aux)
        fine_c.append(FC)

    return qc1ncs, fine_c, dq


def csr_calc(name, NAP, depth, PGA, stress, eff_stress, Vs12, approach='IB2008', M=5.0):
    csr = []

    for idx, nam in enumerate(name):

        # calculation of CSR
        z = np.abs(np.array(NAP[idx]) - NAP[idx][0])

        if (approach == 'IB2008') or (approach == 'BI2014'):
            alpha = -1.012 - 1.126 * np.sin(z / 11.73 + 5.133)
            beta = 0.106 + 0.118 * np.sin(z / 11.28 + 5.142)
            rd = np.exp(alpha + beta * float(M))
        elif approach == 'NPR':
            z = np.abs(np.array(depth[idx]))
            Ard = 2.5725 + -0.2296 * min(float(M), 6.5) + 0.0734 * np.log(PGA[idx]) + -0.0041 * Vs12
            rd = 1 - Ard / (1 + np.exp(-(np.log(z)-(1.145 + 0.1634 * M)) / (0.4983 + 0.0437 * M)))

        csr_aux = 0.65 * np.array(stress[idx]) / np.array(eff_stress[idx]) * PGA[idx] * rd
        csr.append(csr_aux)

    return csr


def stress_calc(name, gamma, NAP, depth, location, water):
    sigma_tot = []
    sigma_eff = []
    fos = []
    PGA = []
    depth_p = []

    for idx, nam in enumerate(name):
        # z_pwp = location[nam.split('.GEF')[0]]['water']
        # pwp = (z_pwp - np.array(NAP[idx])) * 9.81
        pwp = np.array(water[idx])
        gamma[idx][gamma == np.inf] = 0
        # abs depth
        z = np.diff(np.abs(np.array(NAP[idx]) - NAP[idx][0]))
        z = np.append(z, z[-1])
        # total stress
        sigma_tot.append(np.cumsum(gamma[idx] * z) + np.abs(depth[idx][0]) * np.mean(gamma[idx][:10]))
        # remove negative pwp
        pwp[pwp < 0] = 0
        # effective stress
        sigma_eff.append(sigma_tot[-1] - pwp)
        sigma_eff[-1][sigma_eff[-1] < 0] = 1e-12

        # read safety factor
        fos.append(float(location[nam.split('.GEF')[0]]['FS']))
        PGA.append(float(location[nam.split('.GEF')[0]]['PGA']))

        # read depth pleistocene
        depth_p.append(location[nam.split('.GEF')[0]]['pleistocene'])

    return sigma_tot, sigma_eff, fos, depth_p, PGA


def qc_to_qc1n(qc_list, frc_list, tot_stress, eff_stress, aux_lit, IC, name, IC_lim=2.60, approach="IB2008"):
    # normalise qc and calculate IC
    Pa = 100
    qc1n = []
    lito = []
    # psi = []
    Cn= []

    # for each qc
    for idx, qc in enumerate(qc_list):

        if approach == 'NPR':
            n = [0.5] * len(aux_lit[idx])
            Cn = [qc[i] / qc[i] * np.min([1.7, (Pa / eff_stress[idx][i]) ** n[i]]) for i, _ in enumerate(qc)] #ToDo make it correct (so without qc / qc)
            qc100 = [qc[i] * np.min([1.7, (Pa / eff_stress[idx][i]) ** n[i]]) for i, _ in enumerate(qc)]
            qc1 = [[q / Pa for q in qc100]]
            qc1ncs, fine_c, dq = fine_correction(qc1, IC[idx], name, approach='NPR')
            qc1ncs = qc1ncs[0]

            tol = 1.e-6 #ToDo tolarantie versoepeld
            error = 1
            max_ite = 100 #ToDo changed max iterations from 10000 to 2000
            itr = 0
            while error >= tol:
                # if did not converge
                if itr >= max_ite:
                    break
                n1 = n_iter(qc1ncs) #is functie om nieuwe n uit te rekenen
                error = np.linalg.norm(n1 - n) / np.linalg.norm(n1)
                n = n1
                itr += 1
                qc100 = [qc[i] * np.min([1.7, (Pa / eff_stress[idx][i]) ** n[i]]) for i, _ in enumerate(qc)]
                qc1 = [[q / Pa for q in qc100]]
                qc1ncs, _, _ = fine_correction(qc1, IC[idx], name, approach='NPR')
                qc1ncs = qc1ncs[0]
                # print(qc1ncs[10:20])
            qc1n.append(qc1ncs)
            # print(name[idx] + ' n iterations = ' + str(itr) + ' error = ' + str(error))

        else:
            # exponent n for IC calculation
            n = [1.0] * len(aux_lit[idx])
            for i, x in enumerate(aux_lit[idx]):
                if x == 'sand':
                    n[i] = 0.5

            qc100 = [qc[i] * np.min([1.7, (Pa / eff_stress[idx][i]) ** n[i]]) for i, _ in enumerate(qc)]
            qc1n.append([q / Pa for q in qc100])

        lito_aux = ["not"] * len(IC[idx])
        for i in range(len(IC[idx])):
            if IC[idx][i] <= IC_lim:
                lito_aux[i] = "sand"
        lito.append(lito_aux)

        # calculation of state parameter according to Jefferies & Been pg 156
        # lambda_10 = 1. / (34. - 10. * np.array(IC[idx])) #todo changend IC[-1] in IC[idx]
        # kappa = 8. + 0.55 / (lambda_10 - 0.01)
        # m = 8.1 - 2.3 * np.log10(lambda_10)
        # Q_p = [(qc[i] - tot_stress[idx][i]) / eff_stress[idx][i] for i, _ in enumerate(qc)]
        # psi.append(-np.log(np.array(Q_p) / kappa) / m)

    return qc1n, lito, n, Cn, fine_c, dq #removed psi


def crr_calc(qc1n_list, approach):
    # calculate CRR
    crr = []
    for qc1n in qc1n_list:
        qc1n = np.array(qc1n)
        # qc1n[qc1n > 211] = 211
        if approach == 'IB2008':
            crr.append(np.exp((qc1n / 540.) + (qc1n / 67.) ** 2 - (qc1n / 80.) ** 3 + (qc1n / 114.) ** 4 - 3.))
        elif approach == 'BI2014':
            crr.append(np.exp((qc1n / 113.) + (qc1n / 1000.) ** 2 - (qc1n / 140.) ** 3 + (qc1n / 137.) ** 4 - 2.8))
        elif approach == 'NPR':
            crr.append(np.exp((qc1n / 113.) + (qc1n / 1000.) ** 2 - (qc1n / 140.) ** 3 + (qc1n / 137.) ** 4 - 2.63))

    for j in range(len(crr)):
        crr[j][crr[j] > 0.6] = 0.6

    return crr


def kappa_sigma(qc1n_list, sigma_list, approach):
    # calculate kappa sigma, qc1n == qc1ncs

    # atmospheric pressure
    Pa = 100

    kappa = []
    for i, qc1n in enumerate(qc1n_list):
        sigma_eff = np.array(sigma_list[i])
        qc1n = np.array(qc1n)
        # check if qc1n is positive
        qc1n[qc1n < 0] = 0

        if (approach == 'IB2008') or (approach == 'BI2014'):
            # relative density
            Re = 0.478 * qc1n ** 0.264 - 1.063
            Re[Re < 0] = 0
            Re[Re > 1.05] = 1.05
            # C_sigma
            C_sigma = 1 / (18.9 - 17.3 * Re)
            C_sigma[C_sigma > 0.3] = 0.3

        elif approach == 'NPR':
            C_sigma = 1 / (37.3 - 8.27 * qc1n**0.264)
            C_sigma[qc1n > 211] = 0.3
            C_sigma[C_sigma > 0.3] = 0.3

        # kappa_sigma
        k_sigma = 1 - C_sigma * np.log(sigma_eff / Pa)
        k_sigma[k_sigma > 1.1] = 1.1

        kappa.append(k_sigma)

    return kappa, C_sigma


def FoS(crr_list, k_sigma_list, csr, kdr_list, qc1ncs, FS_norm, PGA, water, Vs12, k_alpha=1, MSF_method='IB2008', theta=0.7, M=5.0):
    # calculate FoS and Ru
    FS = []
    Ru = []

    for i, crr in enumerate(crr_list):
        # MSF calculation
        if MSF_method == 'IB2008':
            MSF = 1.8
            b = 0.34
        elif MSF_method == 'IB2014':
            MSF_max = 1.09 + (qc1ncs[i] / 180.0) ** 3
            MSF_max[MSF_max >= 2.2] = 2.2
            MSF = 1. + (MSF_max - 1.) * (8.64 * np.exp(- M / 4.0) - 1.325)

            b = (np.log10(1.09 / 0.65 + 1 / 0.65 * (qc1ncs[i] / 180) ** 3)) / np.log10(20.)
        elif MSF_method == 'NPR':
            neqM = np.exp(-0.2487 + -0.3131 * np.log(PGA[i]) + 0.1766 * M + 0.0038 * Vs12)
            MSF = (7.25 / neqM) ** 0.34
            b = 0.2 / theta

        fos = crr / csr[i] * MSF * k_sigma_list[i] * k_alpha * kdr_list[i]
        # apply safety factor
        fos = fos / FS_norm[i]
        fos[fos < 0] = 0
        FS.append(fos)

        Ru_aux = 2 / np.pi * np.arcsin(fos ** (-1 / (2 * b * theta)))
        Ru_aux[fos == float('Inf')] = 0
        Ru_aux[fos <= 1] = 1.0
        Ru.append(Ru_aux)

        # correction for zone above freatic level #todo check if this is correct
        for j in range(len(FS[0])):
            if water[0][j] == 0:
                FS[0][j] = 0
                Ru[0][j] = 0
    return FS, Ru


def read_excel_layers(path, file_name, locs):
    wb = xlrd.open_workbook(os.path.join(path, file_name))

    # for each section assign the data
    for i, section in enumerate(wb.sheet_names()):
        # read layer and remove blanks
        aux_data = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(7)[3:]))
        # unique number of layer
        layers_unique = set(aux_data)
        for layer in layers_unique:
            locs[section].update({int(layer): {}})

            cpt = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(6)[3:]))
            lay = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(7)[3:]))
            top = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(8)[3:]))
            bot = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(9)[3:]))

            # list of indices where layer exists
            idx = []
            for var, x in enumerate(lay):
                if x == layer:
                    idx.append(var)

            # slice the lists
            cpt_idx = [int(cpt[j]) for j in idx]
            top_idx = [top[j] for j in idx]
            bot_idx = [bot[j] for j in idx]

            # assign to dictionary
            locs[section][int(layer)].update({'cpt_nr': cpt_idx})
            locs[section][int(layer)].update({'top': top_idx})
            locs[section][int(layer)].update({'bottom': bot_idx})
            locs[section][int(layer)].update({'k_alpha': 1.0})

    # for each section assign the data
    for i, section in enumerate(wb.sheet_names()):
        # read layer and remove blanks
        aux_data = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(2)[3:]))
        # unique number of layer
        layers_unique = set(aux_data)
        for layer in layers_unique:
            # locs[section].update({int(layer): {}})

            cpt = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(1)[3:]))
            lay = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(2)[3:]))
            top = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(3)[3:]))
            bot = list(filter(lambda x: x != '', wb.sheet_by_index(i).col_values(4)[3:]))

            # list of indices where layer exists
            idx = []
            for var, x in enumerate(lay):
                if x == layer:
                    idx.append(var)

            # slice the lists
            cpt_idx = [int(cpt[j]) for j in idx]
            top_idx = [top[j] for j in idx]
            bot_idx = [bot[j] for j in idx]

            # assign to dictionary
            [locs[section][int(layer)]['cpt_nr'].append(cpt) for cpt in cpt_idx]
            [locs[section][int(layer)]['top'].append(top) for top in top_idx]
            [locs[section][int(layer)]['bottom'].append(bot) for bot in bot_idx]

    return locs


def read_k_alpha(path, file_name, locs):
    with open(os.path.join(path, file_name), 'r') as f:
        data = f.readlines()

    file = [line.strip('\n\r').split(';') for line in data[1:]]

    for line in file:
        locs[line[0]][int(line[1])]['k_alpha'] = float(line[2])

    return locs


def read_pga(file_name):
    with open(os.path.join(file_name), 'r') as fil:
        data = fil.readlines()

    file = [line.strip('\n\r').split(';') for line in data[1:]]

    locsx = {}

    for val in file:
        locsx.update({val[0]: {}})
        locsx[val[0]].update({'PGA': float(val[1])})
        locsx[val[0]].update({'FS': float(val[2])})
        locsx[val[0]].update({'km': float(val[3])})

    return locsx


class FileJson:

    def __init__(self):
        self.data = {'name': [],
                     'coord': [],
                     'depth_pleist': [],
                     'LPI_RC': [],
                     'LPI_LK': [],
                     'FoS': {},
                     'FC': {},
                     'Ru': {},
                     'NAP': [],
                     'IC': {},
                     'qc1ncs': {},
                     'lito': {},
                     # 'psi': {},
                     'km': []}

        return

    def add(self, name, coord, km, LPI_RC, LPI_LK, NAP, RC, IC, lito, depth_pleist): #ToDo removed psi
        # create dictionary structure
        for i in name:
            self.data['name'].append(i)

        # # compute distance in relation to the minimum x coord
        # dist = [[float(val[0]), float(val[0])] for val in cord]
        # dist = np.array(dist)
        # idx_min = np.argmin(dist[:, 0])
        # aux = []
        # for i in dist:
        #     aux.append(((i[0] - dist[idx_min][0]) ** 2. + (i[1] - dist[idx_min][1]) ** 2.) ** .5)
        #
        # self.data['km'] = [i - min(aux) for i in aux]

        for i in coord:
            self.data['coord'].append([i[0], i[1]])
        for i in km:
            self.data['km'].append(i)
        for i in LPI_RC:
            self.data['LPI_RC'].append(i)
        for i in LPI_LK:
            self.data['LPI_LK'].append(i)
        for i in NAP:
            self.data['NAP'].append(i)
        for i in depth_pleist:
            self.data['depth_pleist'].append(i)
        for i in range(len(RC[0])):
            self.data['FoS'].update({i: []})
            for j in RC[0][i]:
                self.data['FoS'][i].append(j)
            self.data['FC'].update({i: []})
            for j in RC[3][i]:
                self.data['FC'][i].append(j)
            self.data['Ru'].update({i: []})
            for j in RC[1][i]:
                self.data['Ru'][i].append(j)
            self.data['IC'].update({i: []})
            for j in IC[i]:
                self.data['IC'][i].append(j)
            self.data['lito'].update({i: []})
            for j in lito[i]:
                self.data['lito'][i].append(j)
            # self.data['psi'].update({i: []})
            # for j in psi[i]:
            #     self.data['psi'][i].append(j)
            self.data['qc1ncs'].update({i: []})
            for j in RC[2][i]:
                self.data['qc1ncs'][i].append(j)

        return

    def write(self, path_results):
        with open(os.path.join(path_results, 'data.json'), 'w') as fil:
            json.dump(self.data, fil)
        return

    # def write(self, path_results):
    #     pass


def combine_figs(base_path, paths_figures, output_folder):
    # combine paths
    output_folder = os.path.join(base_path, output_folder)
    paths_figures = [os.path.join(base_path, i) for i in paths_figures]

    # check if output folder exists. if not creates
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # get all the figs in first path
    gef_name = os.listdir(paths_figures[0])

    # for each gef file
    for i in range(len(gef_name)):
        # open all the figures with the same gef file in the paths
        images = []
        for path in paths_figures:
            images.append(Image.open(os.path.join(path, gef_name[i])))
        # size of the images
        widths, heights = zip(*(i.size for i in images))

        # get dimensions of new image
        max_width = max(widths)
        total_height = sum(heights)

        # create new image
        new_im = Image.new('RGB', (max_width, total_height))

        y_offset = 0
        for im in images:
            new_im.paste(im, (0, y_offset))
            y_offset += im.size[1]

        # save image
        new_im.save(os.path.join(output_folder, gef_name[i] + '.png'))

    return


if __name__ == "__main__":
    # base_path = r'C:\Users\kolk_bd\Aarbevingen\HHNK\gws-05m'
    base_path = r'C:\Users\mavritsa\Projects\Probabilistic Liquefaction\Data'

    assumpts = {'Kdr': 1.,      #ToDo check kdr=1 or1.3 with piet
                'approach': 'NPR',
                'Cfc': 0.0,
                'LPI': 0.0,  # 0.0 or 'pleistocene'
                'VS12': 140.,
                'gws': -0.5} #todo change


    # ToDo change to correct assumptions
    with open(os.path.join(base_path, 'assumptions.txt'), 'w') as f: #ToDo change to correct assumptions
        f.write('assumption: if gamma == inf (from robertson and cabal) -> gamma = 22\n')
        f.write('assumed that unit weight sand is always wet (20)\n')
        f.write('assumed that it is sand for IC<2.6\n')
        f.write('K alpha is 1.3\n')
        f.write('Kdr: ' + str(assumpts['Kdr']) + '\n')
        f.write('Cfc: ' + str(assumpts['Cfc']) + '\n')
        f.write('LPI top NAP: ' + str(assumpts['LPI']) + '\n')
        f.write('Vs12: ' + str(assumpts['VS12']) + '\n')
        f.write('for the pre-boring it is assumed that the depth of the soil above the ground has the same unit weight '
                'as the average unit weight of the next 10 samples (10cm)\n')


    key = read_key(os.path.join(base_path, 'key.txt'))
    # locs = read_pga(os.path.join(base_path, 'PGA_test.csv'))
    locs = read_pga(os.path.join(base_path, 'PGA.csv'))
    locs = read_pleistoce_layers(os.path.join(base_path, r'pleistocene.csv'), locs)
    # read_cpt(key, locs, r'C:\test\Aardbevingen_HHNK\gefs', base_path, assumpts, do_plots=True)
    read_cpt(key, locs, r'C:\test\SAFE\GEFs', base_path, assumpts, do_plots=True)

    combine_figs(base_path, [r"NPR_cpt", r"NPR_qc", r"NPR_FoS"], r'NPR_combined')
