LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0121	243930	569380	Rijksdriehoeksmeting	12E		1.5		12EA3121		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0121	31-05-1963	C1963-05-1059		nee	3723.0	9725.0	31-05-1963	18	54.8	15.2	33.72	3.6	193.4	10	4.7	4.5	.21	.18	.08	0	0	13.8	5	0	9.3	19.6	0	.6	18	3.13	1.566	7.55	
B12E0121	20-05-1963	C1963-05-1037		nee	9780.0	9880.0	20-05-1963	18		18.7	40.64	3.6	242.2	8	5.3		.18	.37						0			0		18	3.99	2.016	7.58	
