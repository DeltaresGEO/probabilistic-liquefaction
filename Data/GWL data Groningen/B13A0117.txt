LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B13A0117	261000	571470	Rijksdriehoeksmeting	13A		0.8		13AA3117		1			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	111CL3EA (ug/l)	112CL3EA (ug/l)	123CL3PA (ug/l)	12CL2EA (ug/l)	12CL2PA (ug/l)	13CL2PA (ug/l)	24-BENZE (ug/l)	Al (mg/l)	As (ug/l)	BR2CLMA (ug/l)	BRCL2MA (ug/l)	BRCL3MA (ug/l)	BRCLMA (ug/l)	Ba (ug/l)	Benzeen (ug/l)	CL3EE (ug/l)	CL3MA (ug/l)	CL4EA (ug/l)	CL4EE (ug/l)	CO2 (mg/l)	Ca (mg/l)	Cd (ug/l)	Cl- (mg/l)	Cr (ug/l)	Cu (ug/l)	DOC (mg/l)	EC (mS/m)	F- (ug/l)	Fe (mg/l)	HCO3 (mg/l)	K (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	MY-NCO (ug/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	Ni (ug/l)	O-PO4 (mg/l)	O2 (mg/l)	Pb (ug/l)	SO4 (mg/l)	Se (ug/l)	SiO2 (mg/l)	T-O2 (C)	TOLUEEN (ug/l)	TOTH (mmol/l)	Zn (ug/l)	pH (-)	
B13A0117		C1993-11-1168		nee	3400.0	16000.0	09-11-1993	<.1	<.5	<.5	<1	<.5	<1	<.01	<.005	<.5	<.1	<.1	<.1	<.1	19	<.01	<.1	<.1	<.1	<.1	165	136	<.2	34	<1	2	8.6	74.9	.1	23.4	500	2.9	150	29.63	<.1	9.4	.665	3.8	<.005	<.5	14.8	<1	1.8	<.5	<2	<.5	<.5	38.52	9	<.01	3.78	11	6.92	
