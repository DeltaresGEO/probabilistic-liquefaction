LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07D0196	233950	582740	Rijksdriehoeksmeting	07D		0.7		07DA3196		3			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B07D0196	17-04-1963	C1963-04-1026		nee	6979.0	8107.0	17-04-1963	78	0	640	4000	1162.52	20.1	537	9	28	192	.58	8.4	.21	0		1815	0	237	18.4	.16	18	8.78	24	7.22	
B07D0196	25-03-1963	C1963-03-1018		nee	7900.0	8000.0	25-03-1963	74			5690	1560.5	20	660	26	59		.7	8.4		0	0		0	305			18	10.78	28.89	7.33	
B07D0196	08-03-1963	C1963-03-1004		nee	6800.0	7000.0	08-03-1963	170	0		1180	425.21	11.4	640	16	25		.37	13.1		0	0		0	228			18	10.5	12.804	6.95	
