LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07G0077	241205	575045	Rijksdriehoeksmeting	07G		0.3		07GA3077		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B07G0077	26-04-1963	C1963-04-1048		nee	3990.0	8500.0	26-04-1963	29	74.6	40.4	49.85	2.9	267.2	15	13	6.8	.11	1	.09	0	0	29.1	0	0	6.7	25.8	0	.57	18	4.32	2.16	7.43	
B07G0077	11-04-1963	C1963-04-1023		nee	8560.0	8660.0	11-04-1963	22		28	41.68	2	239.1	12	7.6		.07	1.1						0			0		18	3.81	1.91	7.4	
