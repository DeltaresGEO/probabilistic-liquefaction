LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07D0399	231540	583140	Rijksdriehoeksmeting	07D		0.51		07DP0399		3			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	TEMP-V (C)	
B07D0399		C1992-10-1466		nee	680.0	780.0	30-10-1992	370		
B07D0399	16-09-1999	T1999-00-2516		nee	680.0	780.0	16-09-1999	308		
B07D0399	23-04-1997	C1997-04-1157		nee	680.0	780.0	23-04-1997	185	20	
