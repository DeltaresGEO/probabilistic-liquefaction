LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B13B0025	272880	569880	Rijksdriehoeksmeting	13B		0.4		13BA3025		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-G (mg/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B13B0025	20-07-1955	C1955-07-1025		nee	3675.0	4550.0	20-07-1955	187	0	21.8	28	25.03	7.7	98	100		36	4.2	.24	1.6	.17	0		21.9	18	4.5	29.2	1.4	18	1.4	.702	<6.5	
B13B0025	05-12-1949	C1949-12-1003		nee	3675.0	4550.0	05-12-1949	27.8			25	22.62	10.1	120	133	44.3	44.3		.26	2.1		0	0			3.9			18	1.54	.774	7.02	
