LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B06H0192	210222	578381	Rijksdriehoeksmeting	06H	GPS (Global Positioning System)	-0.51		06HP7064		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	HCO3 (mg/l)	K (mg/l)	Mg (mg/l)	NH4 (mg/l)	NO3 (mg/l)	Na (mg/l)	SO4 (mg/l)	TOTH (mmol/l)	pH (-)	
B06H0192	01-10-1996	C1996-10-1018		nee	50.0	100.0	01-10-1996	65.5	18.4	49.9	25.5	.93	8.18	.13	10.2	15.8	171	1.97	5.68	
B06H0192	01-10-1996	C1996-10-1019		nee	450.0	500.0	01-10-1996	25.9	70.9	23	91.2	1.32	20.3	1.13	2.94	10.9	0	1.49	6.14	
