LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07G0229	241656	580470	Rijksdriehoeksmeting	07G		-0.982		07GP0015		20			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Ca (mg/l)	Cl- (mg/l)	EC (mS/m)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	K (mg/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	SO4 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TOTH (mmol/l)	pH (-)	
B07G0229		C1990-06-1177		nee	1135.0	1235.0	20-06-1990		34																		
B07G0229		C1989-06-1217		nee	1135.0	1235.0	22-06-1989		33																		
B07G0229		C1984-10-1116		nee	144.0	244.0	12-10-1984		82	421.02		4.22								.55				18			
B07G0229		C1985-05-1007		nee	144.0	244.0	02-05-1985		25	393.79														18			
B07G0229		C1988-06-1187		nee	1135.0	1235.0	10-06-1988		34																		
B07G0229		C1984-04-1085		nee	1135.0	1235.0	13-04-1984		34	469.2		28.5								.03				18			
B07G0229		C1982-10-1089		nee	1135.0	1235.0	19-10-1982		33	481.76										.02				18			
B07G0229		C1984-10-1117		nee	1135.0	1235.0	12-10-1984		36	507.95		29.68												18			
B07G0229		C1983-04-1054		nee	1135.0	1235.0	19-04-1983		32																		
B07G0229		C1985-05-1008		nee	1135.0	1235.0	02-05-1985		32	472.34														18			
B07G0229		C1991-06-1250		nee	1135.0	1235.0	21-06-1991		33																		
B07G0229	31-07-2000	T2000-00-2732		nee	1138.0	1238.0	31-07-2000		43																		
B07G0229	31-07-2000	T2000-00-2731		nee	147.0	247.0	31-07-2000		18																		
B07G0229	10-09-1998	C1998-09-1051		nee	151.0	251.0	10-09-1998		<6																		
B07G0229	10-09-1998	C1998-09-1052		nee	1142.0	1242.0	10-09-1998		38																		
B07G0229	27-01-1988	C1988-01-1066		nee	1145.0	1245.0	27-01-1988	51.1	37		38.1	12.01	184.4	1.79	47	7.38	1.41	5.225	.033	.177	14.24	21	3.07	20	1.578	6.48	
B07G0229	07-12-1987	C1987-12-1014		nee	1145.0	1245.0	07-12-1987	51.1	37		38.1	12.01	184.4	1.79	47	7.38	1.41	5.225	.033	.177	14.24	21	3.07	20	1.578	6.48	
B07G0229	02-12-1983	C1983-12-1061		nee	1141.0	1241.0	02-12-1983		37		46.5	24.3			26		.983	4.7		.222				20		6.4	
B07G0229	19-04-1983	C1983-04-1053		nee	151.0	251.0	19-04-1983		32		53	29.56			34		1.15	5.93						20		6.7	
B07G0229	22-07-1982	C1982-07-1054		nee	1141.0	1241.0	22-07-1982		31		44.1	28.15			34		.334							20		6.7	
