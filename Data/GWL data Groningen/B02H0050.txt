LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B02H0050	217406	607705	Rijksdriehoeksmeting	02H		-1.45		02HA3050		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B02H0050	08-11-1974	C1974-11-1021		nee	3850.0	3950.0	08-11-1974	46	480	17470	4576.77	4.5	354	24	53	1272	.8	36	.22	0	9530	0	2314	17	3.6	18	5.83	66.528	7.03	
B02H0050	08-11-1974	C1974-11-1020		nee	2600.0	2700.0	08-11-1974	30	440	17650	4241.63	6	107	18	76	1344	1.2	12	4	0	9582	0	2254	21.5	4.1	18	7.05	67.536	7.36	
