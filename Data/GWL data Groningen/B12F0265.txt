LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12F0265	251740	569184	Rijksdriehoeksmeting	12F	GPS (Global Positioning System)	1.92	Landmeting			5			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B12F0265	01-10-2004	T2003-00-0258		nee	3500.0	3600.0	01-10-2004	25	
B12F0265	01-10-2004	T2003-00-0259		nee	6600.0	6700.0	01-10-2004	19	
B12F0265	01-10-2004	T2003-00-0256		nee	900.0	1000.0	01-10-2004	56	
B12F0265	01-10-2004	T2003-00-0257		nee	1700.0	1800.0	01-10-2004	64	
B12F0265	01-10-2004	T2003-00-0260		nee	9200.0	9300.0	01-10-2004	19	
