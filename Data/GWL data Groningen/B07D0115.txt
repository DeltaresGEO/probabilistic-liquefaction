LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07D0115	236850	578280	Rijksdriehoeksmeting	07D		1.27		07DA3115		12			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	TEMP-V (C)	
B07D0115		C1926-11-1003		nee	5527.0	6177.0	01-11-1926	70	0	
B07D0115		C1926-11-1025		nee	5527.0	6177.0	15-11-1926	68.9	0	
B07D0115		C1926-11-1050		nee	5527.0	6177.0	29-11-1926	72.5	0	
B07D0115		C1926-12-1018		nee	5527.0	6177.0	13-12-1926	68.9	0	
B07D0115		C1926-12-1037		nee	5527.0	6177.0	27-12-1926	69.4	0	
B07D0115		C1927-06-1010		nee	5527.0	6177.0	08-06-1927	73.8	0	
B07D0115		C1927-03-1029		nee	5527.0	6177.0	14-03-1927	73	0	
B07D0115		C1927-03-1060		nee	5527.0	6177.0	29-03-1927	74.8	0	
B07D0115		C1927-04-1031		nee	5527.0	6177.0	11-04-1927	72.7	0	
B07D0115		C1927-04-1061		nee	5527.0	6177.0	25-04-1927	76.3	0	
B07D0115		C1927-05-1015		nee	5527.0	6177.0	09-05-1927	72.7	0	
B07D0115		C1927-01-1016		nee	5527.0	6177.0	10-01-1927	113.9	0	
