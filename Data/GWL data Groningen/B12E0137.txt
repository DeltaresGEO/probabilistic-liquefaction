LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0137	244300	568760	Rijksdriehoeksmeting	12E		0.7		12EA3137		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0137	05-08-1964	C1964-08-1011		nee	6044.0	9840.0	05-08-1964	21	50	18.2	32.36	5.8	173.2	5	5.1	4.7	.26	.25	.04	0	0	14.1	0		11.1	26		.51	18	2.88	1.458	7.21	
B12E0137	09-07-1964	C1964-07-1011		nee	9800.0	9900.0	09-07-1964	24		18	31.84	3.3	181.3	5	5.3		.18	.24						0			0		18	2.95	1.566	7.22	
