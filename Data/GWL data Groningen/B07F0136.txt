LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07F0136	258811	592747	Rijksdriehoeksmeting	07F		-0.3		07FA3136		8			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	EC (uS/cm)	TEMP-V (C)	
B07F0136		C1977-01-1007		nee		18000.0	01-01-1977	5600	1440.06	18	
B07F0136		C1977-01-1001		nee		1000.0	01-01-1977	5050	1508.13	18	
B07F0136		C1977-01-1005		nee		11000.0	01-01-1977	2275	659.81	18	
B07F0136		C1977-01-1003		nee		7000.0	01-01-1977	1675	492.24	18	
B07F0136		C1977-01-1008		nee		20000.0	01-01-1977	5700	1455.77	18	
B07F0136		C1977-01-1006		nee		15000.0	01-01-1977	2725	775.01	18	
B07F0136		C1977-01-1004		nee		9500.0	01-01-1977	2020	591.73	18	
B07F0136		C1977-01-1002		nee		4500.0	01-01-1977	390	144.53	18	
