LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0133	244070	569360	Rijksdriehoeksmeting	12E		1.4		12EA3133		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0133	11-05-1964	C1964-05-1022		nee	6913.0	9619.0	11-05-1964	11	0	54.2	18.8	34.56	4.4	164.7	6	3.9	4.9	.21	.52	.12	0	0	18.7	0	0	31.9	19	0	.45	18	2.73	1.566	7.45	
B12E0133	24-04-1964	C1964-04-1035		nee	9520.0	9620.0	24-04-1964				18.8	35.4		187.9					.4						0			0		18	3.09	1.71	7.41	
