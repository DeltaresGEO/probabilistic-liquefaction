LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12B0072	239850	574340	Rijksdriehoeksmeting	12B		1.5		12BA3072		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO3-- (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-G (mg/l)	Mn (mg/l)	NH4 (mg/l)	SO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12B0072	22-07-1960	C1960-07-1041		nee	7142.0	11149.0	22-07-1960		28	33.51	7.7	136	4	7	.45	.04		18	2.21	1.5	6.9	
B12B0072	27-06-1960	C1960-06-1032		nee	9250.0	9350.0	27-06-1960	0	25	31.42	5.8	143		7.2	.36	.33	23.5	18	2.35	1.54	6.9	
