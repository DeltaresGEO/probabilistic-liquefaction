LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0160	240465	574805	Rijksdriehoeksmeting	12E		0.19		12EA3160		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0160	09-01-1964	C1964-01-1016		nee	4074.0	8415.0	09-01-1964	19	57.2	18.9	35.4	2.7	192.8	8	7.1	5.4	.11	.21	.04	0	0	14.3	0	0	12.1	20.2	0	.77	18	3.14	1.66	7.39	
B12E0160	30-12-1963	C1963-12-1036		nee	8439.0	8539.0	30-12-1963	21		19	39.59	2.7	243.4	8	7.3		.09	.44											18	4	2	7.45	
