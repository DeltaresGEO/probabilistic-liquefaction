LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07D0132	233460	581000	Rijksdriehoeksmeting	07D		2.62		07DA3132		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	Fe (mg/l)	HCO3 (mg/l)	KMNO4V-O (mg/l)	Mg (mg/l)	NH4 (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	
B07D0132		C1934-02-1003		nee		6050.0	06-02-1934	10.4	96.4	56	2	339.8	18	8.6	0		0	35.4	0	5.5	10	10.5	0	0	4.67	2.786	
B07D0132		C1934-03-1001		nee	5300.0	6100.0	05-03-1934		94.7	54	1.9	326.4	14.5	9.5	0	0	0	28.6	0	4.3	3.1	6.3	0	0	4.45	2.759	
