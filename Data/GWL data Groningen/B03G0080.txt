LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B03G0080	248724	605989	Rijksdriehoeksmeting	03G		0.94		03GP9001		4			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B03G0080	31-08-2005	T2005-00-4290		nee	501.0	601.0	31-08-2005	15322.5	
B03G0080	31-08-2005	T2005-00-4291		nee	2370.0	2470.0	31-08-2005	9695	
B03G0080	17-10-2003	T2003-00-0006		nee	501.0	601.0	17-10-2003	15700	
B03G0080	17-10-2003	T2003-00-0007		nee	2370.0	2470.0	17-10-2003	11400	
