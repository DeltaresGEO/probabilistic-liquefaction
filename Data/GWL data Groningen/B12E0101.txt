LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0101	240725	574540	Rijksdriehoeksmeting	12E		0.6		12EA3101		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KMNO4V-G (mg/l)	Mn (mg/l)	NH4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0101	26-08-1960	C1960-08-1041		nee	3676.0	8079.0	26-08-1960	19	39.8	2.1	226	5.7	.08	.2	18	3.71	1.95	7.15	
B12E0101	19-08-1960	C1960-08-1024		nee	8060.0	8160.0	19-08-1960	21	41.89	1.6	229	6.3	.13	1	18	3.67	1.84	7.1	
