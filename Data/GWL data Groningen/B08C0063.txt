LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B08C0063	262369	580308	Rijksdriehoeksmeting	08C		-1.19		08CP0063		10			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	TEMP-V (C)	
B08C0063		C1992-10-1147		nee	480.00003	580.0	09-10-1992	117		
B08C0063		C1992-10-1146		nee	300.0	400.0	09-10-1992	134		
B08C0063	11-08-2005	T2005-00-4434		nee	302.0	402.0	11-08-2005	32		
B08C0063	11-08-2005	T2005-00-4435		nee	482.0	582.0	11-08-2005	52		
B08C0063	04-06-2003	T2003-00-0139		nee	300.0	400.0	04-06-2003	42		
B08C0063	04-06-2003	T2003-00-0140		nee	480.0	580.0	04-06-2003	35		
B08C0063	03-06-1999	T1999-00-2582		nee	300.0	400.0	03-06-1999	67		
B08C0063	03-06-1999	T1999-00-2583		nee	480.0	580.0	03-06-1999	27		
B08C0063	10-06-1997	C1997-06-1262		nee	480.0	580.0	10-06-1997	45	20	
B08C0063	10-06-1997	C1997-06-1261		nee	300.0	400.0	10-06-1997	20	20	
