LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0156	240660	574490	Rijksdriehoeksmeting	12E		0.3		12EA3156		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0156	10-05-1963	C1963-05-1022		nee	4264.0	7355.0	10-05-1963	22	61.8	20.1	37.18	3.2	212.3	7	5.7	5	.2	.12	.1	0	0	16.6	10.9	21.2	.85	18	3.46	1.75	7.48	
B12E0156	24-04-1963	C1963-04-1043		nee	8325.0	8425.0	24-04-1963	26		63	52.78	2.9	233	9	5.7		.15	.25								18	3.82	2.32	7.4	
