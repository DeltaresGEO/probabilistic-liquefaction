LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07H0116	258212	580488	Rijksdriehoeksmeting	07H		-0.48		07HP0116		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B07H0116		C1992-10-1161		nee	767.0	867.0	13-10-1992	1268	
B07H0116	04-06-1998	C1998-06-1026		nee	770.0	870.0	04-06-1998	1200	
