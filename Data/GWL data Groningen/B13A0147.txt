LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B13A0147	264600	570470	Rijksdriehoeksmeting	13A		1.78		13AP0147		4			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B13A0147		C1975-01-1052		nee	2400.0	4400.0	28-01-1975	100	
B13A0147		C1983-01-1034		nee	2400.0	4400.0	17-01-1983	161	
B13A0147		C1984-09-1179		nee	2400.0	4400.0	24-09-1984	177	
B13A0147		C1977-02-1011		nee	2400.0	4400.0	10-02-1977	105	
