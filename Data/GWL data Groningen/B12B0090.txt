LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12B0090	239705	574600	Rijksdriehoeksmeting	12B		0.18		12BA3090		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12B0090	11-05-1964	C1964-05-1019		nee	6344.0	8949.0	11-05-1964	18	0	47.2	22	33.1	6.7	142.1	6	3.8	5.8	.26	.04	.07	0	0	18.1	0	0	31.1	21	0	.4	18	2.32	1.43	7.18	
B12B0090	24-04-1964	C1964-04-1034		nee	8518.0	8618.0	24-04-1964	18			22.8	36.66	7.4	161	7	5.4		.34	.32						0			0		18	2.64	1.57	7.19	
