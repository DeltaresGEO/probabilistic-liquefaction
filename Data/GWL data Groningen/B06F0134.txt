LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B06F0134	216176	590046	Rijksdriehoeksmeting	06F	GPS (Global Positioning System)	0.97	Landmeting	06FL0017		3			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B06F0134	14-06-2005	T2005-00-4297		nee	72.0	272.0	14-06-2005	113	
B06F0134	17-10-2003	T2003-00-0009		nee	72.0	272.0	17-10-2003	28	
B06F0134	10-06-1999	T1999-00-2466		nee	72.0	272.0	10-06-1999	26	
