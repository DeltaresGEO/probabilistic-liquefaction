LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B11F0233	213132	573617	Rijksdriehoeksmeting	11F		2.85		11FL0082		4			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B11F0233		C1990-06-1207		nee	295.0	395.0	21-06-1990	313	
B11F0233	14-08-2005	T2005-00-4461		nee	293.0	393.0	14-08-2005	221.5	
B11F0233	13-08-2003	T2003-00-0161		nee	293.0	393.0	13-08-2003	255	
B11F0233	17-04-1997	C1997-04-1111		nee	288.0	388.0	17-04-1997	115	
