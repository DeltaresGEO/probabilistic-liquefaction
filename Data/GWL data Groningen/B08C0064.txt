LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B08C0064	260984	585067	Rijksdriehoeksmeting	08C		-0.4		08CP0064		5			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B08C0064		C1992-10-1162		nee	730.0	830.0	13-10-1992	1241	
B08C0064	25-08-2000	T2000-00-2756		nee	780.0	880.0	25-08-2000	1620	
B08C0064	25-08-2000	T2000-00-2755		nee	157.0	257.0	25-08-2000	18	
B08C0064	01-09-1998	C1998-09-1003		nee	157.0	257.0	01-09-1998	19	
B08C0064	01-09-1998	C1998-09-1004		nee	780.0	880.0	01-09-1998	1580	
