LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B06H0166	211141	581108	Rijksdriehoeksmeting	06H	GPS (Global Positioning System)	-0.46		06HP7023		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	HCO3 (mg/l)	K (mg/l)	Mg (mg/l)	NH4 (mg/l)	NO3 (mg/l)	Na (mg/l)	SO4 (mg/l)	TOTH (mmol/l)	pH (-)	
B06H0166		C1996-10-1007		nee	143.0	193.0	01-10-1996	99.8	40.1	63.7	514	1.63	48.5	.26	1.96	13	0	4.49	6.65	
B06H0166	01-10-1996	C1996-10-1008		nee	593.0	643.0	01-10-1996	97.3	38.8	61.5	504	1.84	47.7	.61	1.83	12.2	0	4.39	6.71	
