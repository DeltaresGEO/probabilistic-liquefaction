LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0390	244372	572443	Rijksdriehoeksmeting	12E		2.16		12EL0044		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B12E0390	15-06-2005	T2005-00-4470		nee	123.0	223.0	15-06-2005	20	
B12E0390	24-09-2003	T2003-00-0170		nee	123.0	223.0	24-09-2003	18	
