LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12B0033	238820	574060	Rijksdriehoeksmeting	12B		2.56		12BP0033		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	AGRES (-)	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (mS/m)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12B0033	01-12-1969	C1969-12-1002		nee	5960.0	6060.0	01-12-1969	3	25	0	24	26	22.3	6.1	46	6	3	3.9	.18	.17	.1	0	0	26	0	56	17.6	.27	20	.75	.77	<6.5	
B12B0033	29-04-1954	C1954-04-1039		nee	5960.0	6060.0	29-04-1954					26	28.4	7.8	43				.21							50.4			20	.61	.68	6.3	
