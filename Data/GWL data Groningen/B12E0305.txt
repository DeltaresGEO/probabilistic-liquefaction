LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0305	246703	573612	Rijksdriehoeksmeting	12E	GPS (Global Positioning System)	2.06		12EP0305		6			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B12E0305	03-05-2005	T2005-00-4479		nee	2650.0	2750.0	03-05-2005	66	
B12E0305	03-05-2005	T2005-00-4478		nee	900.0	1000.0	03-05-2005	52	
B12E0305	06-06-2003	T2003-00-0176		nee	900.0	1000.0	06-06-2003	43	
B12E0305	06-06-2003	T2003-00-0177		nee	2650.0	2750.0	06-06-2003	68	
B12E0305	20-05-1999	T1999-00-2612		nee	900.0	1000.0	20-05-1999	64	
B12E0305	20-05-1999	T1999-00-2613		nee	2650.0	2750.0	20-05-1999	121	
