LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0032	241250	574330	Rijksdriehoeksmeting	12E		0.35		12EA3032		4			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	H2S (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-G (mg/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0032	31-10-1947	C1947-10-1015		nee	3550.0	8400.0	31-10-1947	15.6	0	62.2	23.2	34.46	2.7	0	191	11	7.1	7.6	5.4	.05	.25	.16	0	15.9	0	19.4	15.2	0	.2	18	3.14	1.79	7.48	
B12E0032	03-10-1947	C1947-10-1002		nee	7475.0	7575.0	03-10-1947	12.6	0	61.9	54	42.52	1.5	0	191	20	9.9	14.1	5.9	.29	.38	.06		35.3	0	19.6	20.6	0	8.9	18	3.14	1.79	7.56	
B12E0032	29-09-1947	C1947-09-1032		nee	3800.0	3900.0	29-09-1947	7.6	0	63.4	42.7	40.11	1.4	0	195	16	13.9	15.9	4.9	.17	.72	.16		26.6	0	21.8	19.6	0	3.3	18	3.17	1.786	7.8	
B12E0032	25-09-1947	C1947-09-1028		nee	2350.0	2450.0	25-09-1947	15.7	0	61.3	46	40.74	5.7	0	221	20	11.3	26.7	5.1	.15	.88	.16		32.7	0	13.6	20.2	0	2.4	18	3.5	1.75	7.52	
