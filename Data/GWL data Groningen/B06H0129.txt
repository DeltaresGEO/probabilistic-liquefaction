LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B06H0129	215206	579210	Rijksdriehoeksmeting	06H	GPS (Global Positioning System)	-0.27		06HP9002		6			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B06H0129	04-08-2005	T2005-00-4322		nee	123.0	223.0	04-08-2005	195	
B06H0129	04-08-2005	T2005-00-4323		nee	123.0	223.0	04-08-2005	208	
B06H0129	31-07-2003	T2003-00-0033		nee	123.0	223.0	31-07-2003	89	
B06H0129	31-07-2003	T2003-00-0034		nee	600.0	700.0	31-07-2003	190	
B06H0129	30-08-1999	T1999-00-2488		nee	123.0	223.0	30-08-1999	165	
B06H0129	30-08-1999	T1999-00-2489		nee	600.0	700.0	30-08-1999	179	
