LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12B0068	239975	573950	Rijksdriehoeksmeting	12B		0.2		12BA3068		3			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-G (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	SO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12B0068	22-08-1961	C1961-08-1017		nee	7794.0	8690.0	22-08-1961	29	0	28	41.37	4.4	204	4	5.7	.2				18	3.35	1.68	6.95	
B12B0068	21-08-1961	C1961-08-1015		nee	4185.0	8690.0	21-08-1961	28	0	26	39.8	4.8	204	5	5.4	.24	.03			18	3.35	1.7	7	
B12B0068	24-05-1961	C1961-05-1025		nee	8800.0	8900.0	24-05-1961		0	36	43.46	1.6	224		17.1	.14	.03	.02	0	18	3.42	1.71	7.1	
