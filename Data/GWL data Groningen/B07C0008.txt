LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07C0008	222840	585310	Rijksdriehoeksmeting	07C		4.0		07CA3008		1			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	EC (uS/cm)	HCO3 (mg/l)	KMNO4V-O (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO3 (mg/l)	SO4 (mg/l)	TEMP-V (C)	TOTH (mmol/l)	
B07C0008		C1918-08-1021		nee		7900.0	30-08-1918	5855	1624.39	820	16.2	17.2	8.9	0	552	18	24.464	
