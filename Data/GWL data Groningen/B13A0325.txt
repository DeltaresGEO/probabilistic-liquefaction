LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B13A0325	264280	569620	Rijksdriehoeksmeting	13A		1.1		13AP0035		6			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B13A0325		C1990-06-1225		nee	300.0	400.0	26-06-1990	19	
B13A0325		C1989-02-1007		nee	3098.0	8068.0	06-02-1989	42	
B13A0325		C1988-10-1001		nee	3098.0	8068.0	03-10-1988	79	
B13A0325		C1983-10-1130		nee	3098.0	8068.0	17-10-1983	154	
B13A0325		C1984-09-1178		nee	3098.0	8068.0	24-09-1984	139	
B13A0325		C1990-06-1226		nee	3098.0	8068.0	26-06-1990	67	
