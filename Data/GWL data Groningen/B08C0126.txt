LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B08C0126	269507	580412	Rijksdriehoeksmeting	08C	GPS (Global Positioning System)	1.77	Landmeting	08CL0007		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	
B08C0126	11-08-2005	T2005-00-4431		nee	363.0	413.0	11-08-2005	433	
B08C0126	07-09-2000	T2000-00-2763		nee	363.0	413.0	07-09-2000	408	
