LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0093	240759	574010	Rijksdriehoeksmeting	12E	Geschat, Top. Kaart 1:10.000	0.25		12EA3093		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO3-- (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KMNO4V-G (mg/l)	Mn (mg/l)	NH4 (mg/l)	NO3 (mg/l)	SO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0093	23-12-1960	C1960-12-1018		nee	3943.0	8739.0	23-12-1960	0	23	41.89	2.7	202	12.6	.11	.2		13.7	18	3.32	1.98	7.1	
B12E0093	06-12-1960	C1960-12-1006		nee	8800.0	8900.0	06-12-1960	0	30	38.23	1.5	171	10.6	.1	.28	0	25	18	2.78	1.68	7.1	
