LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0159	240550	574830	Rijksdriehoeksmeting	12E		0.2		12EA3159		2			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	O2 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0159	22-11-1963	C1963-11-1043		nee	4028.0	8120.0	22-11-1963	17	60	20.2	35.82	2.7	197	9	7.6	4.9	.1	.16	.05	0	14.3	0	0	8.6	23	0	.8	18	3.21	1.7	7.42	
B12E0159	06-11-1963	C1963-11-1006		nee	8012.9995	8112.9995	06-11-1963	22		20	39.69	2.5	234.2	9	5.5		.08	.42										18	3.85	2	7.49	
