LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12E0016	240460	573400	Rijksdriehoeksmeting	12E		0.35		12EP0040		5			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	CO2 (mg/l)	CO3-- (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (uS/cm)	Fe (mg/l)	H2S (mg/l)	HCO3 (mg/l)	KLEUR (mgPt/l)	KMNO4V-G (mg/l)	KMNO4V-O (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NH4-ORG (mg/l)	NO2 (mg/l)	Na (mg/l)	NaHCO3 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	T-O2 (C)	T-PO4 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12E0016	06-10-1948	C1948-10-1007		nee	19912.0	20012.0	06-10-1948	7.9	0	78.9	738	244.65	9.8	0	178	18	23.6	25.8	7.8	.15	1.5	.31	0	425.7	0	23.1	7.6	0	4.2	18	2.92	2.286	7.73	
B12E0016	06-10-1948	C1948-10-1008		nee	22812.0	22912.0	06-10-1948	7.2	0	143.5	2354	572.46	6.5		279	24	18.8	23.3	72.1	.03	4.8	.16	0	1332.6	0	9.3	23.2		4.4	18	4.57	6.571	7.97	
B12E0016	05-10-1948	C1948-10-1005		nee	14913.0	15013.0	05-10-1948	8.6	0	70.3	22.4	39.59	5.4	0	246	30	9.9	13.1	7.2	.25	.26	.17		20.5	0	10.1	17.6	0	2.6	18	4.03	2.054	7.83	
B12E0016	05-10-1948	C1948-10-1003		nee	9762.0	9862.0	05-10-1948	17	0	66.5	29	40.64	8.2	0	175	24	11.4	12.1	7.8	.42	.56	.16	0	19.3	0	58.5	13.2	0	3.6	18	2.85	1.893	7.38	
B12E0016	05-10-1948	C1948-10-1004		nee	12314.0	12414.0	05-10-1948	9.6	0	56.4	19.6	33.62	4.3	0	189	20	5.1	8.8	4	.16	.25	.03	0	17.5	0	18.1	12.2	0	3.3	18	2.88	1.579	7.67	
