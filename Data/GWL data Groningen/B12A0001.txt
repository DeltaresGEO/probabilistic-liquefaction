LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B12A0001	221432	573839	Rijksdriehoeksmeting	12A	GPS (Global Positioning System)	3.04		12AP0001		6			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Al (mg/l)	Ca (mg/l)	Cl- (mg/l)	EC (mS/m)	EC (uS/cm)	Fe (mg/l)	HCO3 (mg/l)	K (mg/l)	Mg (mg/l)	Mn (mg/l)	NH4 (mg/l)	NO2 (mg/l)	NO3 (mg/l)	Na (mg/l)	O-PO4 (mg/l)	SO4 (mg/l)	SiO2 (mg/l)	TEMP-V (C)	TIJDH (mmol/l)	TOTH (mmol/l)	pH (-)	
B12A0001		C1985-03-1013		nee	8921.0	9021.0	12-03-1985	<.005	85.7	23		47	2.55	317.3	1.53	7.6	.21	.181	<.033	<.044	14.1	<.031	1.25	57.5	20		2.45	7.99	
B12A0001		C1985-03-1012		nee	2921.0	3021.0	12-03-1985	<.005	99.4	21		54.1	1.33	385.04	2.06	8.4	.48	1.625	<.033	<.044	15.4	.031	1.3	74.5	20		2.825	7.17	
B12A0001	01-01-1952	C1952-01-1004		nee		6321.0	01-01-1952			31		6.95	21	392			.31						0		20	6.43	3.36		
B12A0001	01-01-1952	C1952-01-1003		nee		2321.0	01-01-1952			28		7.44	40	431			.55						0		20	6.96	3.48		
B12A0001	01-01-1952	C1952-01-1002		nee	8900.0	9000.0	01-01-1952			31		5.29	13.3	260			.3								20	4.39	2.43		
B12A0001	01-01-1952	C1952-01-1001		nee	2900.0	3000.0	01-01-1952			26	67.8		33.3	392			.95						0		20	6.32	3.16		
