LOCATIE gegevens
NITG-nr	X-coord	Y-coord	Coordinaat systeem	Kaartblad	Bepaling locatie	Maaiveldhoogte (m tov NAP)	Bepaling maaiveldhoogte	OLGA-nr	RIVM-nr	Aantal analyses	Meetnet	Indeling	
B07D0113	236900	578150	Rijksdriehoeksmeting	07D	Geschat, Top. Kaart 1:10.000	1.24		07DA3113		12			

KWALITEIT gegevens VLOEIBAAR
NITG-nr	Monster datum	Monster-nr	Monster apparatuur	Mengmonster	Bovenkant monster (cm tov MV)	Onderkant monster (cm tov MV)	Analyse datum	Cl- (mg/l)	TEMP-V (C)	
B07D0113		C1926-11-1002		nee	5874.0	6324.0	01-11-1926	63.8	0	
B07D0113		C1926-11-1024		nee	5874.0	6324.0	15-11-1926	102	0	
B07D0113		C1926-11-1049		nee	5874.0	6324.0	29-11-1926	41.7	0	
B07D0113		C1926-12-1017		nee	5874.0	6324.0	13-12-1926	42.2	0	
B07D0113		C1926-12-1036		nee	5874.0	6324.0	27-12-1926	43.2	0	
B07D0113		C1927-06-1009		nee	5874.0	6324.0	08-06-1927	44	0	
B07D0113		C1927-03-1028		nee	5874.0	6324.0	14-03-1927	46.5	0	
B07D0113		C1927-03-1059		nee	5874.0	6324.0	29-03-1927	43.6	0	
B07D0113		C1927-04-1030		nee	5874.0	6324.0	11-04-1927	44.3	0	
B07D0113		C1927-04-1060		nee	5874.0	6324.0	25-04-1927	46.3	0	
B07D0113		C1927-05-1014		nee	5874.0	6324.0	09-05-1927	51.3	0	
B07D0113		C1927-01-1015		nee	5874.0	6324.0	10-01-1927	47	0	
